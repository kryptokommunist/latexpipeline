\author{Melvin Witte}
\email{melvin.witte@student.hpi.de}
\group{BP2017P2 -- IoT \& Blockchain - Rail2X SmartServices}
%\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Konsens in verteilten Systemen -- Blockchain-Algorithmen}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle
\begin{abstract}
	Konsensfindung in verteilten Systemen ist ein altes Problem, zu welchem ein neuer Ansatz mit Blockchain-Technologien aufgetaucht ist. Es wird eine kurze Einführung in die Grundlagen verteilter Systeme gegeben. Danach werden traditionelle Konsensalgorithmen anhand des Byzantine General Problems besprochen. Blockchain-Systeme werden fast ausschließlich als Grundlage für eine verteilte, elektronische Währung genutzt. Ein Blockchain-System ist ein Hauptbuch, in dem alle Transaktionen gesammelt in Blöcken festgehalten werden. Blöcke, die bereits im Hauptbuch stehen, können nicht gelöscht werden, neue Blöcke werden an die Blockkette im Hauptbuch angehängt. Zur Aufnahme neuer Blöcke in das Hauptbuch gibt es Proof-of-Work-Algorithmen, welche die Datenbank über das Gesamtmaß der Rechenkraft schützen und dabei eine große Menge an Ressourcen benötigen. Zum Erstellen eines neuen Blocks muss ein kryptographisches Rätsel gelöst werden, welches von der gesamten bestehenden Blockkette und dem Inhalt des momentanen Blocks abhängt. Alternativ gibt es Proof-of-Stake-Algorithmen, die zufallsbasiert einem oder mehreren Nutzern die Möglichkeit geben, neue Blöcke zu erstellen. Dabei ist die Wahrscheinlichkeit, einen neuen Eintrag zu erstellen, proportional zum eigenen Anteil an der Blockchain. Das folgt dem Nothing-at-Stake-Prinzip: Anteilhalter der Blockchain, die viel zu verlieren haben, stellen eher sicher, dass das System richtig funktioniert, als es zu untergraben.
\end{abstract}

\section{Einleitung}
Eine Sammlung dezentraler Prozessen, die für den Nutzer als Gesamtsystem nutzbar sind, nennt man ein \emph{Verteiltes System}\cite{tanenbaum_distributed_2007}. Eines der grundlegenden Probleme in verteilten Systemen ist das der Konsensfindung in Systemen mit byzantinischem Fehlermodell\footnote{Unter einem byzantinischen oder auch willkürlichen Fehler versteht man das Verhalten eines fehlerhaften Prozesses, der willkürliche Informationen ausgibt. Dabei kann der Prozess nicht antworten oder die angeforderten Informationen unabsichtlich oder absichtlich falsch liefern\cite*[pp.321-326]{tanenbaum_distributed_2007}}.

Einen neuen Ansatz der Konsensfindung in verteilten Systemen bieten sogenannte \enquote{Blockchain-Systeme}. Eine Blockchain ist eine dezentrale, öffentliche Datenbank, in der Blöcke mit Transaktionen hauptsächlich über eine zu leistende Rechenaufgabe verkettet werden. Mit diesem Prinzip werden bis jetzt hauptsächlich digitale Währungen umgesetzt -- der älteste noch existierende Vertreter ist Bitcoin\footnote{\url{https://bitcoin.org/}}.

Ethereum\footnote{\url{https://www.ethereum.org/}} bietet neben der Währung Ether eine eigene Sprache\footnote{EVM (Ethereum Virtual Machine) Code, weiterführend:\url{https://github.com/ethereum/wiki/wiki/Ethereum-Development-Tutorial}} für Smart Contracts. Smart Contracts bieten die Möglichkeit, dezentrale Anwendungen auf der Blockchain in Form von Code laufen zu lassen. Dabei können Regeln für Besitz, Transaktionsformate und Zustandsübergangsfunktionen erstellt werden\cite{_wiki:_2017}.

Im folgenden wird betrachtet, wie Blockchain-Systeme das Problem der Konsensfindung lösen. Dabei werden zwei verschiedene Ansätze betrachtet -- Proof of Work und Proof of Stake.

\section{Grundlagen verteilter Systeme}
\textcite{tanenbaum_distributed_2007} bieten eine gute und umfassende Einführung in weite Teile der Thematik. Die folgende Einführung in verteilte Systeme orientiert sich daher daran, angefangen mit einer kurzen Definition:

\subsection{Definition}
\blockquote[{\cite[p. 2]{tanenbaum_distributed_2007}}]{A distributed system is a collection of independent computers that appears to its users as a single coherent system.}

Aus dieser Definition lassen sich sinngemäß folgende wichtige Aspekte ableiten:

\begin{itemize}
	\item Ein verteiltes System besteht aus unabhängigen Komponenten: \emph{Computern} oder \emph{Prozessen}.
	
	\item Diese Computer, bzw. Prozesse, kollaborieren, um Benutzern zu suggerieren, sie benutzen nur ein einziges System. Die dafür nötige Kommunikation geschieht über \emph{Kanäle}.
	
	\item Es wird keine Vermutung über die Art der Computer, bzw. Prozesse, getroffen.
	
	\item Es wird keine Vermutung über die Art der Verbindung zwischen Computern, bzw. Prozessen, getroffen.
\end{itemize}

\subsection{Ziele}
Verteilte Systeme werden im Hinblick auf mehrere Ziele benutzt.\cite[pp. 3-16]{tanenbaum_distributed_2007} Die wichtigsten Aufgaben, die verteilte Systeme mit Blick auf Blockchain-Systeme erfüllen sollen, sind das \emph{Verfügbarmachen entfernter Ressourcen}, die Erstellung einer \emph{Transparente Verteilung} der eigenen Ressourcen, sowie das Sicherstellen der \emph{Offenheit} des Systems. Ein weiteres Ziel von verteilten Systemen, die \emph{Skalierbarkeit} eines solchen Systems, wird in Blockchain-Systemen für die Sicherheit geopfert.

\begin{description}
	\item[Verfügbarmachen entfernter Ressourcen] Die Hauptaufgabe verteilter Systeme ist es, Nutzern einfachen Zugriff auf entfernte Ressourcen zu gewähren und diese kontrolliert und effizient zu teilen. Ressourcen können dabei sowohl Hardware-Ressourcen, wie z.B. Drucker, aber auch Software-Ressourcen, wie z.B. Internetseiten, sein. In Bezug auf Blockchain-Technologien wird der Zugriff auf das Hauptbuch und die neu erstellten Blöcke durch das System gewährleistet.

	\item[Transparente Verteilung] Ein weiteres wichtiges Ziel verteilter Systeme ist es, die eigene Diversität vor Nutzern zu verstecken. Nutzer sollen zu jeder Zeit mit dem System so interagieren können, dass sie nur auf ein Gesamtsystem zugreifen und nicht auf eine Ansammlung verschiedener Computer und Prozesse. In Bezug auf Blockchain-Systeme geht es hier erneut darum, dass das Hauptbuch zu jeder Zeit den gleichen Zustand hat, unabhängig davon, welchen Teilnehmer man fragt, also dass die gleichen Blöcke in der gleichen Reihenfolge im Hauptbuch angehängt sind. 

	\item[Offenheit] Mit Offenheit in verteilten Systemen ist gemeint, dass das System seine Dienste mit standardisierten Regeln, sowie die Syntax und Semantik dieser Regeln, anbietet. Im Internet ist das z.B. die Kommunikation gemäß verschiedener Protokolle. In Blockchain-Systemen bezeichnet das die öffentlichen Regeln um neue Blöcke zu erstellen und an das Hauptbuch anzuhängen.
	
	\item[Skalierbarkeit] Unter Skalierbarkeit in verteilten Systemen versteht man, dass dem System einfach neue Ressourcen und Nutzer hinzugefügt werden können, dass das System räumlich erweitert werden kann (\emph{geographische Skalierbarkeit}) und dass große Systeme trotzdem einfach verwaltbar sind (\emph{administrative Skalierbarkeit}). In Bezug auf Blockchain-Systeme ist geographische Skalierbarkeit dadurch gegeben, dass die Nutzer dieser Systeme über das Internet kommunizieren. Neue Nutzer können einfach durch das Ankündigen ihrer Adresse innerhalb des Systems dem System beitreten. Administrativer Aufwand ist nicht vorhanden, da sowohl neue Nutzer, sowie bereits bestehende Nutzer durch Nachrichten im Netzwerk 
\end{description}

\subsection{Fehlertoleranz}

\label{subsec:Fehlertoleranz}

Bevor man über Fehlertoleranz sprechen kann, muss man sich angucken, was es überhaupt heißt, Fehler zu tolerieren. Fehlertoleranz ist ein zentraler Begriff, wenn man von \emph{zuverlässigen Systemen} spricht. Diese müssen folgende Eigenschaften haben:\cite[pp. 322-332]{tanenbaum_distributed_2007}

\begin{description}
	\item[Verfügbarkeit] Das System ist sofort betriebsbereit, sollte eine Anfrage an das System kommen. Mit anderen Worten ist es die Wahrscheinlichkeit, dass zu jedem Zeitpunkt das System korrekt arbeitet.
	
	\item[Zuverlässigkeit] Das System läuft kontinuierlich ohne Ausfall. Im Gegensatz zu Verfügbarkeit beschreibt Zuverlässigkeit die Wahrscheinlichkeit, dass ein System auch über ein längeres Zeitintervall korrekt läuft.
	
	\item[Ausfallsicherheit] Wenn das System ausfällt, sollte keine Katastrophe passieren. Das ist für Systeme wichtig, die bei einem Ausfall verheerende Folgen haben können, z.B. Atomkraftwerke, bemannte Raketen in der Raumfahrt oder Medizintechnik
	
	\item[Wartbarkeit] Ein ausgefallenes System muss einfach repariert werden können. Weiterhin muss die Möglichkeit bestehen, das System zu updaten.
\end{description}

Fehlertolerante verteilte Systeme gewährleisten nun, dass die Eigenschaften zuverlässiger Systeme erfüllt sind. In Zusammenhang mit Verfügbarkeit und Zuverlässigkeit heißt das, dass die Wahrscheinlichkeit eines Systemausfalls drastisch reduziert wird. Das kann u.A. durch redundante Systemkomponenten erreicht werden. Wenn eine der Komponenten ausfällt, kann eine andere die Aufgaben übernehmen, oder läuft sogar schon parallel mit.

Generell gibt es zwei verschiedene Fehlermodelle: Das \emph{fail-stop-Fehlermodell} geht davon aus, dass fehlerhafte Prozesse weder auf Anfragen reagieren, noch von sich aus kommunizieren. Das \emph{byzantinische Fehlermodell} geht davon aus, dass fehlerhafte Prozesse sich willkürlich verhalten. Fehlerhafte Prozesse können zusätzlich zu einem fail-stop-Verhalten falsche Ein- oder Ausgaben geben -- unbeabsichtigt oder auch in böswilliger Absicht. Mit anderen Worten: Man kann keine Vermutung darüber treffen, wie sich ein ausgefallener Prozess verhält. Ist ein System sicher gegenüber byzantinischen Fehlern, so spricht man von \emph{Byzantine Fault Tolerance} (BFT). Im Blockchain-Kontext handelt es sich um ein öffentliches System im Internet. Da jeder dem System beitreten kann, kann man byzantinische Fehler nicht ausschließen.

Man unterscheidet bei verteilten Systemen zwischen \emph{synchronen} und \emph{asynchronen Systemen}. Synchrone Systeme haben feste Zeitfenster, in denen Operationen ablaufen. Daher hat jeder Prozess ein vorher festgelegtes Zeitlimit, in dem eine Antwort auf eine Nachricht ankommen muss. Sollte die Antwort des Prozesses nicht innerhalb dieses Zeitfensters ankommen, kann man davon ausgehen, dass der Prozess fehlerhaft ist. In asynchronen Systemen kann man keine Aussage darüber treffen, ob und wann ein Prozess Anfragen bearbeitet und wann er antwortet. Auch die Kanäle, über die die Prozesse kommunizieren, können unbestimmt lange Verzögerung haben. Insbesondere kann man durch Zeitlimits keine Annahmen über die Korrektheit des Prozesses treffen. Im Normalfall hat man ein asynchrones System, da das Synchronisieren von Systemen einen großen Aufwand erfordert.

\subsection{Sicherheit}
In diesem Abschnitt ist mit Sicherheit nicht wie in \ref{subsec:Fehlertoleranz} die Ausfallsicherheit gemeint, sondern Sicherheit gegenüber böswilligen Angriffen auf das System. Wenn man von sicheren verteilten Systemen spricht, sind dabei hauptsächlich drei Aspekte wichtig:\cite[pp. 378 - 380]{tanenbaum_distributed_2007}

\begin{description}
	\item[Zuverlässigkeit und Ausfallsicherheit] (siehe \ref{subsec:Fehlertoleranz})
	
	\item[Vertraulichkeit] Informationen des Systems sind nur für entsprechend autorisierte Nutzer zugänglich.
	
	\item[Integrität] System-Ressourcen können nur durch entsprechend autorisierte Nutzer geändert werden.
\end{description}

\noindent Um diese Eigenschaften sicherzustellen gibt es drei wichtige Sicherheitsmechanismen, die auf unterschiedliche Weisen implementiert werden können:

\begin{description}
\item[Verschlüsselung] kann z.B. durch vorherigen Schlüsselaustausch und gemeinsamen geheimen Schlüssel\footnote{Zum Beispiel Diffie-Hellman Schlüsselaustausch: \url{https://tools.ietf.org/html/rfc2631}} implementiert werden. Kann gleichzeitig auch als \emph{Authentifizierung} benutzt werden.

\item[Authentifizierung] kann z.B. durch einen gemeinsamen geheimen Schlüssel oder Asymmetrische Kryptographie\cite{merkle_hiding_1978} erreicht werden.

\item[Autorisierung] kann z.B. durch fälschungssichere Zertifikate\cite[pp. 434-437]{tanenbaum_distributed_2007} implementiert werden.
\end{description}

\noindent Dabei betrachtet man vier verschiedene Arten von Bedrohungen:

\begin{description}
	\item[Abfangen] Ein unautorisierter Nutzer erhält Zugriff auf die Dienste oder Daten des Systems.
	\item[Unterbrechung] Dienste oder Daten sind nicht verfügbar, werden unbrauchbar gemacht oder zerstört, z.B. bei DDoS-Attacken
	\item[Modifizierung] Daten eines Dienstes, z.B. die Datenbankeinträge einer Datenbank, werden geändert, sodass dieser nicht mehr den ursprünglichen Spezifikationen entspricht.
	\item[Fabrizierung] Zusätzliche Daten oder Aktivitäten, z.B. ein neuer Nutzer einer Datenbank, werden erstellt.
\end{description}

\section{Konsensfindung}
Konsensalgorithmen in verteilten System werden benutzt, um verschiedene Aufgaben zu lösen, z.B. zur Wahl eines Koordinators, zum Annehmen oder Ablehnen einer Transaktion oder zur Aufgabenverteilung. Das Ziel dabei ist es, in einer Gruppe von Prozessen einen Konsens bezüglich einer Entscheidung in endlich vielen Schritten zu finden\cite[pp. 331-335]{tanenbaum_distributed_2007}. Während das in asynchronen Systemen mit mindestens einem fehlerhaften Prozess nie garantiert werden kann\cite{fischer_impossibility_1985}, kann in synchronen Systemen Konsens sichergestellt werden, solange mehr als zwei Drittel aller Prozesse korrekt laufen.\cite{lamport_byzantine_1982}.

\subsection{Byzantine Generals Problem}
Im folgenden wird \textcite{lamport_byzantine_1982} vorgestellt. Als Analogie wird eine Gruppe byzantinischer Generäle genannt, die bei der Belagerung einer Stadt entscheiden müssen, ob sie angreifen oder sich zurückziehen. Dabei können sie nur über Boten miteinander kommunizieren.\footnote{Auch wenn dies eine schöne, historische Analogie ist, trägt sie meines Erachtens wenig zum Verständnis bei. Daher werde ich im Folgenden die Generäle als Prozesse betrachten und die Boten als Kanäle}

\subsubsection{Synchrone Systeme ohne fehlende Kanäle}
Als System ohne fehlende Kanäle bezeichne ich ein System, in dem jeder Prozess mit jedem anderen Prozess über einen Kanal verbunden ist. Jeder Prozess kann direkt mit jedem anderen kommunizieren. Folgende Annahmen existieren in einem solchen System:
\begin{enumerate}[label=(A\arabic*)]
	\item \label{A1} Jede gesendete Nachricht wird korrekt geliefert.
	
	\item \label{A2} Der Empfänger kennt den Sender.
	
	\item \label{A3} Das Ausbleiben einer Nachricht kann entdeckt werden.
\end{enumerate}

\noindent Es gibt zwei Ansprüche an die Konsensfindung:

\begin{enumerate}[label=(\Alph*)]
	\item \label{A} Alle korrekten Prozesse entscheiden sich für den gleichen Wert (wahr oder falsch).
	
	\item \label{B} Eine geringe Anzahl (byzantinisch) fehlerhafter Prozesse kann die korrekten Prozesse nicht von einem falschen Wert überzeugen.
\end{enumerate}

\noindent Jeder Prozess $i$ kommuniziert nun die Information $v(i)$ an alle anderen Prozesse. Sollte ein Prozess $i$ von einem anderen Prozess $j$ keine Information erhalten, sieht er $v(j)$ als Standardwert $false$ an. Um nun \ref{A} zu erreichen, muss jeder Prozess alle Werte $v(1),...,v(n)$ bei $n$ Prozessen kombinieren und alle Prozesse müssen die gleiche Methode zur Kombination der Werte benutzen. Um \ref{B} zu erreichen, muss diese Methode fehlertolerant sein.

Aus diesen Ansprüchen ergeben sich zwei weiterführende Überlegungen:

\begin{enumerate}[label=(\arabic*)]
	\item \label{1} Um \ref{A} zu erreichen, muss jeder Prozess die gleiche Information für $v(1),...,v(n)$ erhalten. Anders formuliert: Zwei korrekte Prozesse benutzen denselben Wert für jedes $v(i)$.
	
	\item \label{2} Um \ref{B} zu erreichen, muss, wenn der Prozess $i$ korrekt ist, jeder Prozess den von diesem Prozess gesendeten Wert $v(i)$ benutzen.
\end{enumerate}

Nun kann man das ganze Problem auf eine andere Weise betrachten: Ein \emph{Koordinator-Prozess} versendet einen Wert $v$. Die anderen Prozesse müssen nun entscheiden, ob sie diesen Wert als Konsens annehmen oder ablehnen. Auf dieses Problem angewandt ergeben sich aus \ref{1} und \ref{2} folgende zwei sogenannte \emph{Interactive Consistency Bedingugen}:

\begin{enumerate}[label=(IC\arabic*)]
	\item Alle korrekten Prozesse entscheiden gleich: Entweder lehnen sie den Vorschlag ab oder nehmen ihn an.
	
	\item Wenn der Koordinator-Prozess richtig läuft, dann akzeptieren alle anderen korrekten Prozesse den Vorschlag.
\end{enumerate}

Um eine Methode für die Konsensfindung zu erstellen, definiert man eine Funktion $majority(v_1,...,v_n) = v$, die die Mehrheit aller Werte $v_i$ bestimmt. Diese Mehrheit kann a) die Mehrheit der Werte $v_i = v$ sein, falls eine Mehrheit existiert. Sonst wird ein Standardwert -- $false$ -- festgelegt. Oder b) kann $v$ der \emph{Median} aller Werte $v_i$ sein.

Nun kann man einen Algorithmus $OM(m), m\ge0$ erstellen, der in diesem System zu einem Konsens führt:

\paragraph{$OM(0)$}
\begin{enumerate}
	\item Der Koordinator-Prozess schickt jedem anderen Prozess seinen Wert.
	
	\item Jeder Prozess benutzt den Wert des Koordinators, oder $false$, falls er keinen erhält.
\end{enumerate}

\paragraph{$OM(m), m > 0$}
\begin{enumerate}
	\item Der Koordinator schickt allen $n-1$ Prozessen seinen Wert.

	\item \label{OM2}Für jedes $i$ sei $v_i$ der Wert, den der Prozess $i$ vom Koordinator erhält, oder $false$, falls er keinen Wert erhält. Der Prozess $i$ wird nun zum neuen Koordinator im Schritt $OM(m-1)$ mit allen anderen $n-2$ Prozessen.
	
	\item Für jedes $i, j \neq i$ sei $v_j$ der Wert, den der Prozess $i$ von Prozess $j$ in Schritt \ref{OM2} erhält, oder $false$, falls er keinen Wert erhält. Prozess $i$ benutzt den Wert $majority(v_1,...,v_{n-1})$ und wird zum neuen Koordinator in der nächsten Iteration. Das wird bis $OM(0)$ fortgeführt.\footnote{Da jeder Aufruf $m-1$ neue Rekursionen des Algorithmus' aufruft, ist die Nachrichtenkomplexität des Algorithmus' exponentiell abhängig von m. Da der Artikel nur ein Beweis des Konzepts ist, legt der Algorithmus keinen Wert auf Effizienz.}
\end{enumerate}

Für jedes $m$ löst der Algorithmus $OM(m)$ das \emph{Byzantine Generals problem}, falls es mehr als $3m$ Prozesse und maximal $m$ fehlerhafte Prozesse gibt\footnote{Der Beweis und eine beispielhafte Anwendung dieses Protokolls können in \textcite{lamport_byzantine_1982} gefunden werden.}.

\subsubsection{Synchrones System ohne fehlende Kanäle mit unterschriebenen Nachrichten}
Zusätzlich zu den bisher getroffenen Annahmen \ref{A1} bis \ref{A3} kommt nun eine neue Annahme hinzu:

\begin{enumerate}[label=(A\arabic*)]
	\setcounter{enumi}{3}
	\item \label{A4} Die Signatur eines Prozesses kann nicht gefälscht werden und jede Änderung des Inhalts unterschriebener Nachrichten kann erkannt werden. Jeder kann die Authentizität der Signatur eines Prozesses feststellen.
\end{enumerate}

Dabei wird keine Annahme über die Signatur von fehlerhaften Prozessen gemacht. Ein fehlerhafter Prozess kann dabei auch mit der Unterschrift eines anderen fehlerhaften Prozesses unterschreiben.

Um hier nun eine Methode zur Konsensfindung zu erstellen, definiert man eine Methode $choice(V)$, die ein Element $v$ aus $V$ auswählt. Wenn die Menge $V$ nur ein Element $v$ enthält, dann ist $choice(V) = v$. Wenn die Menge $V$ leer ist, dann ist $choice(V) = false$. Sonst kann man $choice(V)$ analog zu $majority$ zum Beispiel durch den Median-Wert von $V$ bestimmen.

Nun kann man einen Algorithmus $SM(m)$ erstellen. Für diesen Algorithmus sei $x:i$ der Wert $x$ signiert von Prozess $i$. $v:j:i$ beschreibt daher denn Wert $v$, der erst von Prozess $j$ und dann von Prozess $i$ signiert wurde.
Dieser Algorithmus führt in diesem System zu einem Konsens:

\paragraph{$SM(m)$}
\begin{enumerate}
	\item[0.] Anfangs $V_i = \emptyset$
	
	\item Der Koordinator signiert und sendet seinen Wert an jeden anderen Prozess.
	
	\item Für jedes $i$:
	
	\begin{enumerate}[label=(\Alph*)]
		\item Wenn der Prozess $i$ eine Nachricht der Form $v:0$ vom Koordinator erhält, und er noch keine andere Nachricht erhalten hat, dann
		\begin{enumerate}[label=(\roman*)]
			\item setzt er $V_i = \{v\}$
			\item sendet er die Nachricht $v:0:i$ an alle anderen Prozesse
		\end{enumerate}
	
		\item Wenn der Prozess i eine Nachricht der Form $v:0:j_1:\cdots:k_k$ erhält und $v$ noch nicht in der Menge $V_i$ ist, dann
		\begin{enumerate}[label=(\roman*)]
			\item fügt er $v$ zu $V_i$ hinzu
			
			\item falls $k < m$, sendet er die Nachricht $v:0:j_1:\cdots:j_k:i$ an jeden Prozess außer $j_1,...,j_k$
		\end{enumerate}	
	\end{enumerate}

	\item Für jedes $i$: Wenn der Prozess $i$ keine Nachrichten mehr erhalten wird\footnote{Der Prozess weiß, ab welchem Zeitpunkt er keine Nachrichten mehr erhalten wird, da jede Nachricht nach \ref{A1} korrekt geliefert wird und nach \ref{A3} das Ausbleiben einer Nachricht erkannt werden kann.}, dann befolgt er $choice(V_i)$.
\end{enumerate}

\noindent Für jedes $m$ löst der Algorithmus $SM(m)$ das Byzantine Generals Problem, falls es maximal $m$ fehlerhafte Prozesse gibt.\footnote{Der Beweis kann in \textcite{lamport_byzantine_1982} nachgelesen werden.}

\textcite{lamport_byzantine_1982} beschreibt weitere Algorithmen in Netzwerken mit fehlenden Kanälen. Bei Interesse können diese dort nachgelesen werden. Zusammenfassend kann man sagen: Wenn Kanäle fehlen, entsteht ein ungerichteter Graph. In diesem Graphen muss ein verbundener Subgraph aller korrekten Prozessen existieren. Ansonsten ist es trotz Signaturen nicht möglich, einen Konsens zu finden.

\subsection{Proof of Work}
\emph{Proof of Work} (PoW) beschreibt einen Mechanismus, der den Zugriff auf eine Ressource oder einen Dienst beschränkt, ihn aber nicht verhindert. Das Prinzip entstand ursprünglich als Kontrollmechanismus, um das Versenden von Spam-Mails teurer zu machen. Im folgenden wird \textcite{dwork_pricing_1992} vorgestellt.

Dazu muss bei Versenden einer Mail eine moderat teure Funktion berechnet werden, die \emph{Pricing-Funktion} genannt wird. Durch zusätzliche Information kann diese Pricing-Funktion einfacher berechnet werden. Diese Information nennt man \emph{Shortcut}.

Funktionen werden dabei allgemein in drei Schwierigkeitsklassen aufgeteilt: einfach, moderat und schwer. Einfache Funktionen sind trivial zu berechnen und dementsprechend billig. Zu moderaten Funktionen herrscht eine signifikante Steigerung der Schwierigkeit, sie können jedoch in annehmbarer Zeit berechnet werden. Schwere Funktionen hingegen sind nur mit unvernünftigem Aufwand zu berechnen.\footnote{Unvernünftiger Aufwand heißt, dass zur Berechnung der Funktionen ein extrem hoher Zeitaufwand benötigt wird z.B. bei der Umkehr von kryptographischen Einwegfunktionen teilweise mehrere Tage bis hin zu Jahren.}

$f$ ist nun eine Pricing-Funktion, falls

\begin{enumerate}
	\item $f$ moderat einfach zu berechnen ist.
	
	\item $f$ bei wiederholtem Berechnen verschiedener Eingaben das Berechnen nicht einfacher wird. Man sagt dass $f$ dann nicht anfällig für Amortisation ist.
	
	\item es bei gegebenem $x$ und $y$ einfach ist $y=f(x)$ zu bestimmen.
\end{enumerate}

Außerdem gibt es eine \emph{Difference-Funktion}, die die Lücke bezüglich der Schwierigkeit zwischen einfachen und moderaten Funktionen mittels eines linearen Parameters anpasst.\footnote{Als Beispiel dient später die Variante, mit der Bitcoin die Difference-Funktion implementiert}

Weiterhin hat eine Funktionsfamilie $F$ die Shortcut-Eigenschaft, falls für $k\ge1$ ein effizienter Algorithmus existiert, der das Paar $(s,c)$ generiert, sodass $s$ gleichverteilt in $S$ ist. Zudem muss, gegeben $s$ aber nicht $c$, $f_s$ eine Funktion in $F$ sein. Da $f$ jedoch nicht anfällig für Amortisierung sein darf, ist es schwer, solche Shortcuts zu finden.

\subsubsection{Bitcoin}
In Bitcoin wird das PoW-Prinzip zur Konsensfindung benutzt. Die Motivation hinter Bitcoin liegt darin, ein Peer-to-Peer Electronic Cash-System aufzubauen, um vertrauenswürdige dritte Parteien im Electronic Cash-Verfahren obsolet zu machen. Dadurch werden die Transaktionskosten und auch die benötigte Zeit drastisch gesenkt, die eine Transaktion bis zur Erfüllung braucht.

Das System hinter Bitcoin -- die \emph{Blockchain} -- ist vergleichbar mit einem Hauptbuch, welches \emph{Blöcken}, die aus \emph{Transaktionen} durch einen digitalen Zeitstempel verkettet. Nachfolgend werden die einzelnen Begriffe nach \textcite{nakamoto_bitcoin:_2008} erläutert.

\paragraph{Transaktionen}
Eine Transaktion in Bitcoin beschreibt das Verhalten, wenn eine Bitcoin den Besitzer wechselt. Allgemein passiert das, indem der momentane Besitzer der Münze den Hash-Code der vorigen Transaktion zusammen mit seinem öffentlichen Schlüssel, sowie den öffentlichen Schlüssel des nächsten Besitzers ans Ende der Bitcoin anhängt. Der Empfänger kann dann die Besitzkette der Münze darüber prüfen. In \autoref{bitcoin_transaction} erkennt man, wie der momentane Besitzer der Bitcoin mithilfe der letzten Transaktion und den öffentlichen Schlüsseln die Bitcoin an einen neuen Besitzer übergeben kann.

\begin{figure}
	\centering
	\includegraphics{figures/bitcoin_transaction}
	\caption{Transaktionskette einer Bitcoin\cite{nakamoto_bitcoin:_2008}}
	\label{bitcoin_transaction}
\end{figure}.

\paragraph{Blöcke}
Transaktionen werden in Blöcken gesammelt, die mithilfe des PoW-Konzepts in einer logischen Abfolge verkettet werden. Daher kommt der Name \enquote{Blockchain}. Diese logische Verkettung erfolgt dadurch, dass der Hash-Code des vorigen Blocks in den nächsten Block aufgenommen wird. Um den PoW zu erbringen, gilt es nun, eine sogenannte \emph{Nonce} zu finden, durch die der Hash-Code des zu validierenden Blocks mit einer bestimmten Zahl an 0-bits anfängt. Die erforderliche Anzahl an 0-bits wird alle 2016 Blöcke dynamisch festgelegt und orientiert sich an den gefundenen Blöcken. Bitcoin versucht das Finden neuer Blöcke konstant auf einen Block alle 10 Minuten zu halten. Der Aufwand, um neue Blöcke zu finden, ist exponentiell zur Anzahl der 0-bits. Diese Anzahl der 0-bits ist die zuvor erwähnte Difference-Funktion, das Finden der Nonce an sich ist die Pricing-Funktion. Der schematische Ablauf ist in \autoref{bitcoin_blocks} dargestellt.

\begin{figure}
	\centering
	\includegraphics{figures/bitcoin_blocks}
	\caption{Verkettung von Blöcken\cite{nakamoto_bitcoin:_2008}}
	\label{bitcoin_blocks} 
\end{figure}

Der Prozess, um einen neuen Block zu finden, lässt sich wie folgt zusammenfassen:

\begin{enumerate}
	\item Neu entstandene Transaktionen werden in das gesamte Netzwerk übertragen.
	
	\item Sogenannte \emph{Miner} sammeln Transaktionen in einem Block. Miner können sich selbst aussuchen, welche Transaktionen sie aufnehmen. Daher gibt es bei jeder Transaktion noch frei wählbare Transaktionsgebühren, die der Miner bei Aufnahme der Transaktion in den Block erhält.
	
	\item Jeder Miner arbeitet daran, den PoW zu finden. Findet er den PoW als erster, so erhält er eine Belohnung in Form von Bitcoins. Im Moment wird ein Miner mit 12,5 Bitcoins für das Finden eines neuen Blocks belohnt. Dieser Wert halbiert sich alle 210.000 Blöcke.\footnote{Das Maximum verfügbarer Bitcoins ist dadurch auf 21 Millionen Bitcoins begrenzt.}
	
	\item Nachdem ein Miner den PoW gefunden hat, überträgt er den Block in das gesamte Netzwerk.
	
	\item Die anderen Prozesse akzeptieren den Block nur dann, wenn alle Transaktionen valide sind.
	
	\item Die anderen Teilnehmer akzeptieren den Block, indem sie die Arbeit am nächsten Block anfangen.
\end{enumerate}

\noindent Dabei besteht weiterhin ein zentrales Problem von Electronic Cash-Systemen: Der Empfänger kann alleine anhand einer Transaktion nicht überprüfen, ob die Bitcoin doppelt ausgegeben wurde. In diesem Fall spricht man vom \emph{Double Spending Problem}. Dieses Problem wird dadurch gelöst, dass Blöcke, in welchen Transaktionen gesammelt werden, einen digitalen Zeitstempel besitzen. Ein Nutzer kann daher anhand des Verlaufs der Blockchain  überprüfen, ob eine Bitcoin in einer früheren Transaktion bereits ausgegeben wurde.

Als richtige Kette wird dabei die längste Kette angesehen, da die meiste Arbeit in diese Kette gesteckt wurde. Um einen einzelnen Block in dieser Kette zu verändern, müsste man den PoW für diesen Block und alle nachfolgenden Blöcke wiederholen und zusätzlich die richtige Kette überholen. Das kann man nur erreichen, wenn man über die Mehrheit der Rechenkraft verfügt. Bei der momentanen Größe des Systems wäre das extrem teuer, da bereits vor einem Jahr pro Stunde mehrere 10.000\$ pro Stunde zum Finden des PoW benutzt worden sind.\cite{aste_fair_2016}.

Dadurch, dass es so teuer ist, im Nachhinein Blöcke zu verändern, kann man die Blockchain hinter Bitcoin als dezentrale, öffentliche Datenbank von Transaktionen ansehen, die im Nachhinein praktisch nicht verändert werden kann.

\subsubsection{Ethereum}
Der größte Konkurrent von Bitcoin als Kryptowährung ist Ethereum\cite{_wiki:_2017} mit ihrer Währung \emph{Ether}. Zusätzlich zur Währungsfunktion bietet Ethereum die Möglichkeit, sogenannte \emph{Smart Contract} auf der Blockchain aufzusetzen. Smart Contracts sind Code, der beim Eintreten verschiedener Konditionen ausgeführt wird -- im Grunde genommen ein digitaler Vertrag.\footnote{Die Ethereum Foundation, \url{https://ethereum.org/}, hat eine eigene, Turing-vollständige Sprache für Smart Contracts geschrieben -- Ethereum Virtaul Machine Code, oder kurz EVM Code.}

Über Smart Contracts können verschiedene andere Konzepte auf der Blockchain implementiert werden:

\begin{description}
	\item[Smart Property] Ansprüche auf IoT-Geräte, z.B. Zugriff auf geteilte Autos nach Bezahlung über Ether
	
	\item[Tokens] Aufsetzen eigener Kryptowährung\footnote{Diese \enquote{Währung} kann auch z.B. Gegenstände aus Computerspielen darstellen} auf der Ethereum-Blockchain.\footnote{\url{https://ehtereum.org/token/}}
	
	\item[Decentralized Autonomous Organisations (DAO)] Autonome Organisationen, die intern über die Verwendung des Organisationsvermögens abstimmen\footnote{\url{https://www.ethereum.org/dao/}}
\end{description}

\noindent Das Prinzip hinter Ethereum ist das gleiche wie bei Bitcoin. Der Unterschied ist, dass das von Ethereum entwickelte PoW-Verfahren \emph{Ethash}\footnote{\url{https://github.com/ethereum/wiki/wiki/Ethash}} deutlich schneller zu einem Ergebnis kommt. Wird bei Bitcoin etwa ein Block alle 10 Minuten gefunden, so wird in Ethereum ein Block alle ca. 15 Sekunden gefunden.

\paragraph{Ethash}
Der Ablauf von Ethash lässt sich grob in vier Phasen aufteilen:

\begin{enumerate}
	\item Für jeden Block existiert ein \emph{Seed}, der etwa alle 30.000 Blöcke angepasst wird.
	 
	\item Aus diesem Seed kann ein 16 MB großer, \emph{pseudo-zufälliger Cache} errechnet werden.
	
	\item Aus diesem Cache wiederum kann ein 1 GB großes \emph{Datenset} errechnet werden.
	
	\item Verschiedene Teile des Datensets werden in der Funktion der Nonce zusammengehasht, bis der Hash-Code des gesamten Blocks unter einem bestimmten Wert liegt.
\end{enumerate}

\noindent Der Vorteil von Ethash ist, dass nur die Miner das komplette Datenset brauchen. Für Clients, die nicht minen, reicht das extrem kleine Cache zum Validieren des PoW. Zudem wird deutlich weniger Energie in die Erstellung eines einzelnen Blocks gesteckt. Mehr Transaktionen können mit weniger Energie ablaufen.

PoW-Algorithmen sind hauptsächlich sicher, da physikalisch wenig Ressourcen zur Verfügung stehen, um den PoW zu erbringen. Dabei gibt es zwei Ressourcen\cite{bitfury_group_proof_2015}:

\begin{enumerate}
	\item Spezielle Hardware zum Minen, sogenannte \emph{ASICs}\footnote{Hardware, die auf Brute-Force-Angriffe auf Hashing spezialisiert ist, siehe \url{https://en.bitcoin.it/wiki/ASIC}}
	
	\item Strom, um die Hardware zu versorgen
\end{enumerate}

Um nun eine höhere Chance auf die Belohnungen für das Finden von Blöcken zu haben, leisten sich Miner ein Wettrüsten bezüglich der verfügbaren Ressourcen. Das ist ökologisch gesehen nicht nachhaltig, weshalb es schon länger Überlegungen zu einer alternativen Gruppe von Konsensalgorithmen gibt: Proof of Stake(PoS)-Algorithmen.

\subsection{Proof of Stake}
Bei PoW wird \enquote{zufällig} ein Miner ausgewählt, der den gefordeten PoW als erstes findet. Im PoS wird nun diese erzeugte Zufälligkeit durch eine andere Form der Zufälligkeit ersetzt: Ein \emph{Stakeholder} der Währung, also jemand der die Währung besitzt, hat eie zu seinem Besitz proportional hohe Wahrscheinlichkeit, den nächsten Block erstellen zu dürfen. Das nennt man im Allgemeinen \emph{Block Minting}. Die Idee dahinter ist das \emph{Nothing-at-Stake-Prinzip}: Die Teilnehmer mit dem höchsten Stake haben das meiste Interesse daran, dass die Blockchain korrekt läuft. Schließlich haben diese am meisten zu verlieren, sollte durch einen Angriff die Reputation und damit der Wert einer Blockchain sinken. Im klassischen PoS wird davon ausgegangen, dass alle Benutzer am Konsensverfahren teilnehmen.

Allgemein kann man drei Bedingungen oder generelle Richtlinien aufstellen, um die korrekte Funktionsweise von Blockchains zu sichern:\cite{bitfury_group_proof_2015}

\begin{enumerate}
	\item Ein Benutzer, der einen Block entdeckt, sollte ermutigt werden, diesen Block unverzüglich mit dem Netzwerk zu teilen.\footnote{Das ist wichtig, um \emph{Eventual Consistency} zu erreichen. Eventual Consistency in verteilten Systemen sagt allgemein aus, dass alle Teilnehmer des Systems den gleichen Zustand eines Datensatzes kennen, solange eine hinreichend lange Zeit ohne Schreibvorgängen und Fehlern vorausgesetzt werden kann.\cite[pp. 289-291]{tanenbaum_distributed_2007}}
	
	\item Ein Benutzer sollte davor abgeschreckt werden, neue Blöcke in der Mitte einer Kette zu erstellen. Genauer: Wenn ein Block $B'$ auf den Block $B$ verweist, so sollte der Benutzer keinen Grund haben, einen Block auf $B$ zu erstellen.\footnote{Das sollte passieren, um vorsätzliches Erstellen verschieneder Zweige zu verhindern.}
	
	\item Konsensregeln sollten so konstruiert sein, dass verschiedene Blockchain-Zweige aufgelöst werden. Ein Blockchain-Zweig sollte daher immer in vernünftiger Zeit alle anderen überholen.
\end{enumerate}

\noindent PoS-Algorithmen hängen meistens von mehreren Faktoren ab: Dem vorigen Block $B_{prev}$, dem Benutzer mit der Adresse $A$, dem momentane Zeitstempel $t$, dem Kontostand von $A$, $bal(A)$ und der Zielschwierigkeit $D\in[1,M]$. Ein Algorithmus sieht dabei meistens ähnlich wie folgende Bedingung aus:

$hash(hash(B_{prev}, A, t)) \le bal(A)M/D$

\subsubsection{Delegated PoS}
Bei Delegated-PoS-Verfahren (DPoS-Verfahren) darf eine bestimmte Menge an vorher festgelegten Benutzern am Konsensverfahren teilnehmen, sogenannte \emph{Delegates} oder \emph{Validators}, die bei Erfüllen ihrer Pflichten gerinfügig belohnt und bei schädlichem Verhalten hart bestraft werden. Dabei gibt es zwei verschiedene Prozesse, an denen ein Dlegate teilnimmt. Der erste Prozess ist das Erstellen eines Blocks, der zweite das Validieren der generierten Blöcke durch digitales Signieren. Blöcke können dabei von einzelnen Nutzern erstellt werden. Zum Validieren müssen jedoch typischerweise mehrere Delegates den Block signieren. Die Liste der berechtigten Delegates ändert sich periodisch nach bestimmten Regeln. Dabei gibt es im Allgemeinen drei verschiedene Arten, neue Delegates zu bestimmen und alte Delegates aus ihren Pflichten zu entlassen:

\begin{enumerate}
	\item Wahl durch Stake im System
	
	\item Mehrheitswahl durch andere Benutzer im System mit Wahlkraft entsprechend ihrem Stake
	
	\item Macht entsprechend einer Sicherheitseinlage, auf die sie während ihrer Funktion als Delegate keinen Zugriff haben, und die bei Fehlverhalten verloren geht
\end{enumerate}

Weiterhin gibt es das Prinzip der \emph{weak subjectivity}, welches aussagt, dass ein neuer Nutzer nicht alleine von dem momentanen Zustand der PoS-Blockchain auf den richtigen Zweig schließen kann.\footnote{\url{https://blog.ethereum.org/2014/11/25/proof-stake-learned-love-weak-subjectivity/}} Einige weitere Angriffe werden in \textcite{bitfury_group_proof_2015} vorgestellt.

\subsubsection{Casper}
Casper\cite{buterin_casper_2017} ist ein Hybrid-System von PoW und PoS. Zum Erstellen der Blöcke wird mangels eines sicheren PoS-Algorithmus weiter der Ethash-Algorithmus benutzt. Jeder 100. Block ist ein \emph{Checkpoint}. An diesem Checkpoint werden die letzten 100 Blöcke validiert. Delegate kann jeder werden, der einen Betrag als Sicherheitseinlage verschließt. Ein Block wird dabei nur dann als korrekt angesehen, sollte er mindestens zwei Drittel Zustimmung unter den Delegates haben.

\section{Weiterführende Themen}
Es gibt verschiedene Formen der Angriffe auf PoW- und PoS-Systeme, sowie Abwehrmechanismen gegen diese.\cite{bitfury_group_proof_2015}

Auch Orakel in Zusammenhang mit Blockchain\footnote{\url{https://gnosis.pm/}} sind eine nähere Betrachtung wert.

\section{Zusammenfassung}
Es wurde ein kurzer Einblick in verteilte Systeme gegeben und traditionelle Konsensalgorithmen anhand des Byzantine General Problems behnadelt. Danach ist auf den neuen Ansatz von verteilten Blockchain-Anwendungen eingegangen worden und darauf, welche Entwicklung es in Bezug auf PoW und PoS gibt.

PoW-Blockchain-Anwendungen sind im Allgemeinen durch die pure Masse an Rechenleistung, die hinter ihnen steckt, sehr sicher. Gleichzeitig sind ebendiese Rechenleistung ein massives Problem für die Umwelt. PoS-Blockchain-Anwendungen hingegen sind anfällig für eine Vielzahl an Angriffen, bieten jedoch eine zufällige Auswahl zur Erstellung des nächsten Blocks und die Möglichkeit, deutlich härtere Bestrafungen einzuführen.