\author{Marcus Ding}
\email{marcus.ding@student.hpi.de}
\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Praktische Störung von \emph{IEEE 802.11p} Signalen}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle
\begin{abstract}
\emph{IEEE 802.11p} ist eine Erweiterung des Funkstandards \emph{IEEE 802.11} für den Einsatz in Fahrzeugumgebungen. Daher ist die Latenz von besonderer Wichtigkeit. In einer Versuchsreihe mit modifizierten, handelsüblichen WLAN-Karten zeigte sich, dass \emph{IEEE 802.11p} im Vergleich mit vergleichbaren Funkstandards auch bei Störung eine geringere Latenz hat. Zudem ist die Leistung auf größere Entfernungen ausreichend.
\end{abstract}

\section{Einleitung}

Der Standard \emph{IEEE 802.11p} wurde für die Kommunikation zwischen Autos entwickelt. Die Deutsche Bahn ist daran interessiert den Standard für die Schiene zu adaptieren (\emph{Rail2X}). Die Leistungsfähigkeit der Technologie und Robustheit gegen Störung im Vergleich mit anderen verwandten Funkstandards ist daher kritisch um die Vorteilhaftigkeit zu bewerten. Dazu führt Abschnitt 2 die genutzte Hardware ein. In Abschnitt 2 folgt eine Kurzzusammenfassung des \emph{IEEE 802.11p}-Standards. Anschließend präsentiert Abschnitt 3 die Messergebnisse und Abschnitt 4 gibt einen Ausblick auf nächste Schritte.

\section{Testsystem}

Für das Testen von \emph{IEEE 802.11p} gibt es zur Zeit keine fertigen Produktlösungen für Endnutzer. Daher dienten handelsübliche \emph{WLAN}-Karten in Verbindung mit industriellen auf physisch höhere Belastung ausgelegten Rechnersystemen als Testbed. Das Testbed bestand aus drei solchen Rechnern. Durch Modifikation von Treiber und Software beherrschten die Rechnersysteme \emph{IEEE 802.11p}.

\subsection{Hardware}

Als Hardware-Basis für die Versuchsmessungen dienten drei \emph{HPE GL20 IoT Gateway}s. Sie sind passiv gekühlt und für industrielle Anwendungen konzipiert. Die technischen Daten lauten wie folgt:

\begin{itemize}
\item Intel I5-4300U CPU
\begin{itemize}
\item1.90 - 2,90 GHz CPU-Takt
\item 2 Kerne
\item 15 Watt maximale Leistungsaufnahme
\end{itemize}
\item 8GB Arbeitsspeicher
\item 64GB SSD
\end{itemize}.

Die Rechner wurden jeweils mit \emph{Qualcomm Atheros AR9462} \emph{WLAN}-Karten nachgerüstet. Diese sind für den Einsatz in Laptops produziert und können durch eine Modifikation des Treibers auch auf den \emph{IEEE 802.11p}-Frequenzen funken. Die Karten beherrschen ab Werk $2,4$ sowie $5$ GHz Frequenzen und die \emph{IEEE 802.11n/g/b/d/e/j/i}-Standards. \cite{atheros} 

\begin{figure}
\centering
\includegraphics[height=0.3\textwidth]{figures/hpe.jpg}
\caption{Das verwendete \emph{HPE GL20 IoT Gateway}.}
\label{fig:hpe}
\end{figure}


\subsection{Software}

Auf den Rechnern wurde \emph{Ubuntu 16.04.3 LTS} installiert. Darüber hinaus wurde ein aktuellerer Kernel installiert, um \emph{IEE 802.11p}-Funktionalität nutzen zu können. Beim aktualisierten Kernel handelte es sich um Version \emph{GNU/Linux 4.11.0-14-generic}. Zudem wurde das Tool \emph{iw} auf Version 4.14 aktualisiert und der \emph{ath9k}-\emph{WLAN}-Kartentreiber modifiziert. Zuletzt wurde die \emph{wireless-regdb} modifiziert, um ein Betreiben der \emph{WLAN}-Karte auf den für \emph{IEE 802.11p} vorgesehenen $5,9$ GHz-Frequenzen zu ermöglichen.

\section{Überblick \emph{IEE 802.11p}}

\emph{IEEE 802.11p} ist eine Erweiterung des \emph{IEEE 802.11}-Standard. Ziel ist es \emph{IEE 802.11} für den Einsatz in Fahrzeugen zu adaptieren. Aus dieser Perspektive heraus wurden viele Phasen des basalen \emph{IEEE 802.11}-Protokolls auf der \emph{MAC}-Schicht beseitigt oder verkürzt. \emph{IEEE 802.11p} erlaubt es Stationen außerhalb des Kontextes eines \emph{Basic Service Set} (\emph{BSS}) zu operieren. Die Bezeichnung hierfür lautet \emph{OCB} (outside the context of a basic service set). Dadurch kann die Latenz der Assoziierungsphase eingespart werden. Weiter gibt es nicht die Notwendigkeit mehrere Kanäle zu scannen, da \emph{OCB}-Kommunikation auf einem Kanal stattfindet.

Beim Austausch von Frames im \emph{OCB}-Modus findet auf der \emph{MAC}-Schicht keine Authentifizierung statt. Es ist dennoch möglich diese auf der Anwendungsebene zu implementieren. Auf physischer Ebene befasst sich der Standard hauptsächlich mit der Spektrumallokation. Die vorgesehenen Frequenzen für Fahrzeuganwendungen liegen im $5 GHz$ Spektrum. Es gibt einen Kontrollkanal und die restliche Kanäle sind Anwendungen zugewiesen (Siehe Tabelle \ref{table:its}). Um den Auswikungen des Doppler-Efffekts entgegenzuwirken, wurde die Kanalbandbreite statt auf die üblichen $20$ auf $10 MHz$ festgelegt. \cite[Vgl.]{msadaa2010comparative}\footnote{Original in Englisch}


\section{Messungen}

Gemessen wurde Latenz und UDP-Datendurchsatz in verschiedenen Testumgebungen. Hierfür wurden die Tools \emph{ping} sowie \emph{iperf} eingesetzt.

\begin{description}
    \item[ping] Dieses Tool dient der Messung der Paketumlaufzeit\footnote{Englisch: Round Trip Time (RTT)}. Dazu versendet das Programm ein im \emph{Internet Control Message Protocol} definiertes \emph{ECHO\_REQUEST}-Datagram um eine \emph{ECHO\_RESPONSE} auszulösen. Das \emph{ECHO\_REQUEST}-Datagram enthält neben dem IP-Header (20 Bytes) zusätzlich noch einen ICMP-Header (8 Bytes) und eine beliebige Datenmenge. \cite{ping8Lin35:online}
        \item[iperf] Das Programm dient der Bandbreitenmessung in Netzwerken. Verschiedene Parameter bezügliche Puffer, Timings und Protokolle lassen sich einstellen. Es misst Bandbreiten für \emph{TCP}, \emph{UDP}, \emph{SCTP} in \emph{IPv4}- und \emph{IPv6}-Netzwerken. \footnote{siehe \url{https://iperf.fr}}
\end{description}

Die Rechner erhielten für alle Versuche die gleichen statischen \emph{IP}-Adressen: \emph{$10.0.0.1$}, \emph{$10.0.0.2$} und \emph{$10.0.0.3$}. Entsprechend beziehen sich die folgend genutzten Bezeichnungen Rechner 1, Rechner 2, Rechner 3 auf die an der letzen Stelle korrespondierenden \emph{IP}-Adressen.

\subsection{Messungen im Inneren}

Im ersten Versuchsaufbau wurden die Auswirkung von Distanz im Gebäudeinneren untersucht. Die Messungen zeigen, dass die Signalqualität verglichen mit \emph{WLAN}-Produkten für Haushalte deutlich robuster ist. Es ist zu vermuten, dass dies mit der geringeren Nutzung und folglich Funkfeldbelastung des $5,9$ GHz-Frequenzbands zusammenhängt.

\subsubsection{Aufbau}

Die Messungen wurden auf der 3. Etage im Inneren des ABC-Gebäudes des Hasso-Plattner-Instituts durchgeführt. Hierzu wurde Rechner 2 am Ende des Ganges platziert. Als mobiler Messpunkt diente ein Bürostuhl auf welchem Rechner 1 befestigt wurde (Siehe Abb. \ref{fig:stuhl}). Es wurden Messungen auf 3, 20, 50 und 100 Metern Distanz durchgeführt.

\begin{figure}[!h]
\centering
\includegraphics[height=0.47\textwidth]{figures/chair2x.JPG}
\caption{Die mobile Station des Versuchsaufbaus. Im Hintergrund: Die fest positionierte Station.}
\label{fig:stuhl}
\end{figure}

\vspace{-0.9cm}

\paragraph{Messmethodik}

Die Latenz wurde mit \emph{ping} gemessen. Es wurde mit folgenden Befehlen auf Rechner 1 der Durchschnitt für verschiedene Paketgrößen anhand von fünf Stichproben ermittelt:
\begin{lstlisting}[language=bash]
 $ ping 10.0.0.2 -c 5
 $ ping 10.0.0.2 -c 5  -s 512
 $ ping 10.0.0.2 -c 5  -s 2048
\end{lstlisting}
. Der Datendurchsatz wurde mit \emph{iperf} gemessen. Es wurde der UDP-Durchsatz mit folgendem Befehl auf Rechner 1 ermittelt:
\begin{lstlisting}[language=bash]
  $ iperf -c 10.0.0.2 -u -b 1000M
\end{lstlisting}
.  Auf Rechner 2 wurde zuvor der \emph{iperf}-Server mit gestartet:
\begin{lstlisting}[language=bash]
  $ iperf -s -u
\end{lstlisting}
.

\subsubsection{Ergebnis}

Das Messergebnis zeigt, dass die Funkverbindung im Gebäudeinneren robust bleibt. Erst bei 100 Metern Distanz zwischen den Rechnern gibt es Verschlechterungen der Messwerte. Der Datendurchsatz in Abbildung \ref{fig:durchsatz1} bricht um circa 38 Prozent ein. Wohingegen sich die Latenz für 100 Meter Distanz bei allen Paketgrößen in Annäherung verdoppelt (Abb. \ref{fig:latenz1}). Die höhere Latenz für die größeren \emph{ping}-Pakete lässt sich dadurch erklären, dass die großen Pakete die \emph{MTU} von 1500 Bytes überschreiten und in mehrere Pakete aufgeteilt werden. Entsprechend steigen die Trendlinien lediglich in der Höhe versetzt vergleichbar an.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{figures/durchsatz1.png}
\caption{Messergebnisse auf verschiedenen Entfernungen ((\emph{iperf}, größer ist besser).}
\label{fig:durchsatz1}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{figures/latenz1.png}
\caption{Messergebnisse auf verschiedenen Entfernungen (\emph{ping}, geringer ist besser).}
\label{fig:latenz1}
\end{figure}

\subsection{Messung mit Störung}
\subsubsection{Aufbau}

Die Rechner 1 - 3 wurden in einem symmetrischen Dreieck angeordnet. Dabei wurden verschiedene \emph{IEEE 802.11}-Modi konfiguriert und Latenz- sowie Datendurchsatzmessungen vorgenommen. Rechner 1 führte hierbei die Messungen durch. Rechner 2 diente als Messpunkt um Daten von Rechner 1 zu empfangen und zu antworten. Rechner 3 diente stets als Störer. Die Messreihe im \emph{OCB}-Modus diente der Messung von \emph{IEEE 802.11p}. Als Vergleich dienten ein Ad-Hoc-Netzwerk (\emph{IBSS}), ein im Haushalts-\emph{WLAN} gebräuchliches Netzwerk im \emph{AP}-Modus und selbiges mit einem externen Störer im Ad-Hoc-Modus. Die jeweiligen Konstellationen sind in Abbildung \ref{fig:aufbau} dargestellt. Die Rechner im \emph{AP}-Modus wurden stets mit aktivem \emph{WPA2} konfiguriert. Die Frequenzen für \emph{IBSS} sowie \emph{AP} betrug $2442 MHz$. Die Frequenz für \emph{OCB} war $5890 MHz$.

\begin{figure}[h]
\centering
\begin{subfigure}{0.4\textwidth}
\centering\captionsetup{width=.8\linewidth}
\includegraphics[width=\textwidth]{figures/messung_ocb.png}
\caption{Versuchsanordnung der \textbf{Messreihe OCB} im \emph{OCB}-Modus.}
\end{subfigure}
\begin{subfigure}{0.4\textwidth}
\centering\captionsetup{width=.8\linewidth}
\includegraphics[width=\textwidth]{figures/messung_ibss.png}
\caption{Versuchsanordnung der \textbf{Messreihe IBSS} im \emph{IBSS}-Modus.}
\end{subfigure}
\begin{subfigure}{0.4\textwidth}
\centering\captionsetup{width=.8\linewidth}
\includegraphics[width=\textwidth]{figures/messung_ap.png}
\caption{Versuchsanordnung der \textbf{Messreihe AP} im \emph{AP}-Modus. Beide Clients sind mit dem Master verbunden.}
\end{subfigure}
\begin{subfigure}{0.4\textwidth}
\centering\captionsetup{width=.8\linewidth}
\includegraphics[width=\textwidth]{figures/messung_ap_ibss.png}
\caption{Versuchsanordnung der \textbf{Messreihe AP+IBSS} im \emph{AP}-Modus mit einem Störer im \emph{IBSS}-Modus.}
\end{subfigure}
\caption{Die vier Versuchsanordnungen der Messreihe mit Störung. Der Abstand der Rechner zueinander betrug etwa 20 Zentimeter.}
\label{fig:aufbau}
\end{figure}

\paragraph{Störer}

Auf Rechner 3 wurde ein \emph{Python}-Skript ausgeführt, welches über ein \emph{Socket}  \emph{Raw Packets} auf \emph{Sicherungsschicht} (\emph{OSI-Schicht} 2) verschickt. Dadurch werden Problematiken, wie eine etwaige Plausibilitätsüberprüfung des Paketes durch das Betriebssystem vermieden. So konnte in einem vorherigen Versuch unter Nutzung eines \emph{Socket}s vom Typ \emph{AF\_INET} im \emph{OCB}-Modus nur gestört werden, wenn eine \emph{IP}-Adresse eines existierenden Gerätes angegeben wurde. Mit dem \emph{AF\_PACKET}-Parameter lassen sich mit dem \emph{Socket} arbiträre Pakete versenden. Der \emph{SOCK\_RAW}-Parameter erlaubt, auch den Header zu manipulieren. \cite{packet7L98:online} Dies ist ein interessantes Szenario, da dieses Verhalten mit einem unintelligenten Jammer oder einem defekten Gerät vergleichbar ist. Mit dem Parameter $N$ lässt sich die Größe in Bytes des zu versendenden Frames festlegen. Der Frame hat für den Parameterwert $N$ die Form $(00110001)^N$. Dies entspicht den hintereinanderfolgenden Einsen als \emph{UTF-8}-Codierung. Der gesamte \emph{Python}-Code ist in Abbildung \ref{fig:messungcode} zu finden.

\paragraph{Messmethodik}

Die Latenz wurde mit \emph{ping} gemessen. Es wurde mit folgendem Befehl auf Rechner 1 der Durchschnitt von 2000 Stichproben ermittelt:
\begin{lstlisting}[language=bash]
  $ sudo ping 10.0.0.2 -i 0 -c 2000
\end{lstlisting}
.
\noindent Der Datendurchsatz wurde mit \emph{iperf} gemessen. Es wurde der UDP-Durchsatz mit folgendem Befehl auf Rechner 1 ermittelt:
\begin{lstlisting}[language=bash]
  $ iperf -c 10.0.0.2 -u -b 1000M
\end{lstlisting}
. 
\noindent Auf Rechner 2 wurde zuvor der \emph{iperf}-Server mit gestartet:
\begin{lstlisting}[language=bash]
  $ iperf -s -u
\end{lstlisting}
.
\noindent Jeweils zuvor wurde auf Rechner 3 das Skript zur Signalstörung (Abb. \ref{fig:messungcode}) mit angepasstem Parameter $N \in \{100,200,300,400,500,757\}$ ausgeführt:
\begin{lstlisting}[language=bash]
  $ sudo python stoerer.py
\end{lstlisting}
.

\begin{figure}
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=0.9,
bgcolor=LightGray,
fontsize=\footnotesize,
linenos
]{python}
import socket
import time

N = 757
seconds_runtime = 30
data = "1" * N

s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
s.bind(('wlp1s0', socket.SOCK_RAW))

print("Starting sending...")

end_time = time.time() + seconds_runtime
packets_count = 0

while (time.time() < end_time):
    s.send(data)
    packets_count = packets_count + 1

data_rate = (len(data.encode('utf-8')) * packets_count * 8) / \
seconds_runtime / 1000.0

print("Datarate is " + str(data_rate) + " kBit/s")
\end{minted}
\caption{\emph{Python}-Code zur Erzeugung des Störsignals. Der Parameter $N$ bestimmt die Größe des versendeten \emph{Raw Packets}.}
\label{fig:messungcode}
\end{figure}

\subsubsection{Ergebnis}

Die Messungen zeigen die erwartbare Verschlechterung der Werte mit zunehmender Störung, mit Ausnahme der Messreihe AP. Sowohl bei Latenz als auch Durchsatz verschlechtern sich ihre Werte nur geringfügig und nicht abhängig vom Störwert (Siehe Abb. \ref{fig:durchsatz2} und \ref{fig:latenz2}). Dieses Verhalten ist durch den Umstand zu erklären, dass im \emph{AP}-Modus der Master-\emph{Access Point} den Kanalzugriff koordiniert. Dies resultiert in diesem Fall darin, dass der Störer nicht effektiv stören kann. \cite[S. 131]{dargie2010fundamentals} So steigt die Latenz unabhängig vom Grad der Störung nur knapp 60 Prozent an. Der Datendurchsatz sinkt auch unabhängig vom Grad der Störung um circa 60 Prozent. Exakte Angaben sind Tabelle \ref{table:mess1} und \ref{table:mess2} zu entnehmen.

Verglichen mit den restlichen Messreihen -- ausgenommen AP -- zeigen die Trendlinien, dass die Verschlechterung mit anwachsender Störung bei OCB am geringsten ist. Die Datenrate ist gestört durchgehend mit IBSS vergleichbar, die Latenz von IBSS ist bei Störung jedoch mindestens doppelt so groß und hat bei maximaler Störung gar vierfache Größe.

Die Daten zeigen, dass \emph{IEEE 802.11p} seine Berechtigung hat. Auch wenn die ungestörte Datenrate im \emph{OCB}-Modus die geringste von allen ist, sind die Werte gestört im Datendurchsatz mindestens vergleichbar und in der Latenz überlegen. Dies gilt jedoch nicht, wenn man die Messreihe AP betrachtet. Da hier zur Initiierung eine längere Zeit zur Authentifizierung benötigt wird, ist diese jedoch für die Echtzeitanwendungen, für welche \emph{IEEE 802.11p} entwickelt wurde, nicht geeignet. Ist der Störer nicht Teil des Netzerkes (Messreihe AP+IBSS) ist lediglich der Datendurchsatz geringfügig höher. Zudem konnte mit den Störungen bei keinem Funkstandard eine Überschreitung der kritischen Latenzzeiten, die vom europäischen Standard \emph{ITS}\footnote{Intelligent Transport Systems} für den Einsatz in Autos vorgesehen sind, erzwungen werden (Siehe Tabelle \ref{table:its}).

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{figures/durchsatz2.png}
\caption{Messergebnisse mit Störer (\emph{iperf}, größer ist besser).}
\label{fig:durchsatz2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{figures/latenz2.png}
\caption{Messergebnisse mit Störer (\emph{ping}, geringer ist besser)}
\label{fig:latenz2}
\end{figure}

\begin{table}[!h]
\begin{center}
  \begin{tabular}{ | p{2.3cm} | p{2.3cm} | p{1.6cm} | p{6.2cm} |}
    \hline
    \textbf{Application category} & \textbf{Latency tolerance} & \textbf{Range} & \textbf{Example (delay requirements)}\\ \hline
    \textbf{Road safety} & Low latency & Local range & \begin{tabular}{@{}c@{}}Pre-crash sensing/warning (50 ms)  \\ Collision risk warning (100 ms)\end{tabular} \\ \hline
    \textbf{Traffic efficiency} & Some latency is acceptable & Medium range & \begin{tabular}{@{}c@{}}Traffic information - \\ Recommended itinerary (500 ms)\end{tabular}  \\ \hline
    \textbf{Value-added services} & Long latency is accepted & Medium range &  \begin{tabular}{@{}c@{}}Map download update -\\ Point of interest notification (500 ms)\end{tabular}  \\
    \hline
  \end{tabular}
\end{center}
\caption{\emph{ITS} Awendungskategorien: Beispiele und Anforderungen. \cite{msadaa2010comparative}}
\label{table:its}
\end{table}

\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Störrate & OCB & IBSS  & AP & AP+IBSS \\\hline
0        & 1,64             & 1,50              & 1,44            & 1,46                 \\\hline
100      & 2,63             & 5,72              & 2,42            & 3,81                 \\\hline
200      & 3,77             & 9,36              & 2,42            & 4,97                 \\\hline
300      & 4,78             & 13,59             & 2,38            & 7,92                 \\\hline
400      & 5,92             & 19,15             & 2,48            & 8,06                 \\\hline
500      & 6,72             & 21,79             & 2,40            & 11,42                \\\hline
757      & 9,59             & 47,36             & 2,52            & 13,15            \\\hline   
\end{tabular}
\caption{Messergebnisse mit Störer (\emph{ping}). Latenz in ms.}
\label{table:mess1}
\end{table}

\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Störrate & OCB & IBSS & AP & AP+IBSS  \\ \hline
0        & 11,6             & 22                & 27,5            & 26,4                 \\ \hline
100      & 8,45             & 8,42              & 18,9            & 14,2                 \\ \hline
200      & 7,73             & 6,46              & 18,6            & 11,5                 \\ \hline
300      & 7,07             & 5,49              & 18,5            & 10,01                \\ \hline
400      & 7,14             & 5,13              & 17,4            & 8,4                  \\ \hline
500      & 6,56             & 4,92              & 18              & 8,6                  \\ \hline
757      & 5,89             & 4,22              & 17,5            & 6,32                 \\ \hline
\end{tabular}
\caption{Messergebnisse mit Störer (\emph{iperf}). \emph{UDP}-Datendurchsatz in MBit/s.}
\label{table:mess2}
\end{table}

\clearpage

\section{Zusammenfassung und Ausblick}

Insgesamt demonstrieren beide Versuche, dass der in \emph{IEEE 802.11p} definierte \emph{OCB}-Modus leistungsstark ist. \emph{IEEE 802.11p} bringt zuvorderst Verbesserungen bei der Latenz mit sich. Keine der Störungen hat zu einer Latenz über den \emph{ITS}-Grenzwerten für den Autoeinsatz geführt. Die Reichweite ist groß, jedoch hat der Platz im Gebäude nicht ausgereicht um die Limitierungen zu testen. Eine zukünftige Messung im Freien wäre diesbezüglich aussagekräftiger. Zudem lassen sich aus dem statischen Versuchsaufbau keine Aussagen zu Effekten von Bewegungsgeschwindigkeit auf die Verbindungsqualität treffen. Hier wären weitere Messungen, etwa aus einem fahrenden Auto heraus,  notwendig. Eine Messung der gestörten Latenzen mit realistischen Paketgrößen wäre ebenfalls interessant. Womöglich würde die Latenz dadurch kritische Schwellwerte übersteigen.