\author{Marcus Ding}
\email{marcus.ding@student.hpi.de}
\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Blockchain Grundlagen -- \enquote{How To Time-stamp a Digital Document}}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle
\begin{abstract}
Stuart Haber und Scott Stornetta veröffentlichen 1991 das Paper \enquote{How to time-stamp a digital document} -- ein Grundstein heutiger Blockchain-Technologien. Das Paper skizziert zwei Ideen zur Verifizierung von Änderungs- und Erstellungsdatum für beliebige digitale Dokumente: Dies kann zum einen durch \emph{Linking} -- eine Verkettung von Transaktionen durch Einbettung eines Hashes der vorherigen Transaktionen in die jeweilig aktuelle -- erfolgen. Zum anderen kann dies durch \emph{Distributed Trust} -- eine Echtheitsbestätigung basierend auf Zertikaten von zufällig gewählten Nutzern aus einem Pool -- erfolgen. Die Nutzung von asymmetrischer Kryptographie und Hashing machen diese Verfahren effizient und sicher gegenüber Manipulationsversuchen.
\end{abstract}

\section{Einleitung}

Der Begriff \emph{Blockchain} ist einer der Begriffe, welche  oftmals in einem Atemzug mit \emph{Big Data}, \emph{Smart Contracts}, \emph{Internet of Things}, \emph{Industrie 4.0}  und vielen anderen Schlagwörtern genannt wird und in letzter Zeit immer präsenter wurde (Abb. \ref{fig:ngram}). 

Die Grundidee -- ein robustes, verteiltes Transaktionssystem ohne zentrale Vertraueninstzanz betreiben zu können -- hat etwas anarchisches an sich und beflügelt daher neben Firmen auch allerlei Libertäre und Cypherpunks auf dieser Erde. Mit der Währung Bitcoin, welche Blockchain-Technologien implementiert und aktuell in 24 Stunden $3,004,540,000$ Dollar an Transaktionen verarbeitet\footnote{Daten von \url{https://coinmarketcap.com/currencies/bitcoin/}, aufgerufen am 06.11.2017}, gibt es ein rasant wachsendes und immer populärer werdendes (Abb. \ref{fig:ngram2}) Beispielprojekt, das Potenzial und Gefahren der Technologie gleichermaßen aufzeigt. Insbesondere verdeutlicht es, wie wichtig es ist, dass alle Transaktionen sicher vor Manipulationsversuchen sind. Auch das Bedürfnis, dass jeder Nutzer die Echtheit der Transaktionen verifizieren möchte wird hier klar. Dies alles muss ohne zentrale Instanz funktionieren. Exakt dieses Probleme adressiert das Paper \enquote{How to time-stamp a digital document} \cite{timestamp}, als eines der ersten zu seiner Zeit.

DerAbschnitt zweite Abschnitt beginnt mit der Definition des unscharfen Begriffs \emph{Blockchain} und basaler kryptographischer und Hashinggrundlagen. Darauf aufbauend folgt in Abschnitt 3 eine Zusammenfassung beziehungsweise Übersetzung des dieser Ausarbeitung zugrunde liegenden Papers \enquote{How to time-stamp a digital document} \cite{timestamp}, welches Konzepte für einen verlässlichen Zeitstempelservice für digitale Dokumente vorstellt. Die besondere Relevanz dieser Arbeit wird dadurch unterstrichen, dass sie im Paper \enquote{Bitcoin: A peer-to-peer electronic cash system} \cite{nakamoto2008bitcoin} , welches die Grundlage von Bitcoin darstellt, in den Quellen genannt wird. Weiter sind insgesamt drei der acht Quellen des Papers vom gleichen Autorenpaar geschrieben. \cite{nakamoto2008bitcoin} In Abschnitt 4 findet sich eine abschließende Zusammenfassung und ein Ausblick auf weitergehende Themen.

\begin{figure}
\includegraphics[width=\textwidth]{figures/ngram.png}
\caption{Historischer Kontext: Abgebildet ist die Entwicklung der Google Suchfrequenzen. Rot: \enquote{Blockchain}, Blau: \enquote{IoT} \cite{ngram}. Beide Begriffe scheinen zu korrellieren und haben erst kürzlich einen Popularitätsschub erfahren.}
\label{fig:ngram}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{figures/ngram2.png}
\caption{Historischer Kontext: Abgebildet ist die Entwicklung der Google Suchfrequenz für den Begriff \enquote{Bitcoin} \cite{ngram2}. Auch wenn kein durchgehender Anstieg zu erkennen ist, gibt es doch einen klaren Aufwärtstrend.}
\label{fig:ngram2}
\end{figure}

\section{Definitionen}

Eine exakte Definition des Begriffes Blockchain ist schwierig, da er in vielen Ausprägungen gebräuchlich ist und Definitionen oft vage bleiben und widersprüchlich sind.

\subsection{Blockchain}

Dennoch helfen bereits nicht-wissenschaftliche Definition weiter, wenngleich ohne Kenntnis eines konkreten Beispiels die Definition weiterhin unscharf bleibt. Etwa die Wikipedia-Definition:

\begin{quotation}
Eine Blockchain (auch Block Chain, englisch für Blockkette) ist \textbf{eine kontinuierlich erweiterbare Liste von Datensätzen}, genannt \enquote{Blöcke}, \textbf{welche} mittels kryptographischer Verfahren \textbf{miteinander verkettet sind}. Jeder Block enthält dabei typischerweise einen kryptographisch sicheren Hash des vorhergehenden Blocks, einen Zeitstempel und Transaktionsdaten.

Der Begriff Blockchain wird synonym für ein Konzept genutzt, mit dem ein Buchführungssystem dezentral geführt werden kann und dennoch ein Konsens über den richtigen Zustand der Buchführung erzielt wird, auch wenn viele Teilnehmer an der Buchführung beteiligt sind. \textbf{Dabei gibt es Konzepte, die das Vertrauen in eine zentrale Instanz erfordern, aber auch solche, die vollständig ohne das Vertrauen in einen Mittelsmann auskommen}. Worüber in dem Buchführungssystem Buch geführt wird, ist für den Begriff der Blockchain unerheblich. Es können Werte einer Währung, Immobiliengrundbücher, Wikipedia-Einträge, Kunstgegenstände oder Verträge sein.

\hfill --- Wikipedia \cite{blockchain_wiki}, ergänzt durch Hervorhebungen
\end{quotation}

Ein weiterer sehr interessanter Blickwinkel ist die Betrachtung der Blockchain als eine spezielle Art von Datenbank:

\begin{quotation}
Das Wort Blockchain kommt aus dem Englischen und bedeutet wörtlich übersetzt schlicht Block-Kette. \textbf{Die Blockchain ist zuvorderst eine Datenbank}, also ein Stück Software, in dem Daten gespeichert werden. Den Anfang macht der „Schöpfungsblock“, alle weiteren Blöcke werden erst überprüft und dann chronologisch hinten angehängt.

\hfill --- Retarus.com \cite{blockchain_ret}, ergänzt durch Hervorhebungen
\end{quotation}

In für wissenschaftlich bestimmte Fachkreise bestimmten Publikationen finden sich engere Definitionen:

\begin{quotation}
Der Begriff  Blockchain beschreibt eine spezielle technische Realisierung der Integritätssicherung, bei der Einträge in Blöcke zusammen- gefasst und durch kryptographische Hash-Funktionen zu einer praktisch unveränderlichen Folge verkettet werden. [...]

Die als Blockchain bezeichnete \emph{authentifizierte Datenstruktur} lässt sich als Weiterentwicklung der verketteten Liste betrachten. Abbildung [\ref{fig:blockchain}] veranschaulicht die entscheidenden Ergänzungen. [...] Eine korrespondierende \emph{authentifizierte Datenstruktur} erhält man, indem Zeiger durch Hash-Werte der vorhergehenden Elemente ersetzt werden. [...] Um die nachträgliche Modifikation großer Teile der Datenstruktur für einen Angreifer impraktikabel aufwändig zu machen, kann als Konvention vereinbart werden, dass gültige Elemente die Lösung für eine algorithmisch schwierige, aber effizient überprüfbare Aufgabe enthalten müssen, deren genaue Aufgabenstellung (d. h. die Instanz des Problems) von allen anderen Daten in diesem Element abhängt. [...] Diese Datenstruktur kann als Hash-verkettete Liste mit Arbeitsnachweis bezeichnet werden und wird als Idealtyp für eine Blockchain verstanden. Listenelemente sind dabei Blöcke, die als Nutzdaten eine Liste von Einträgen (z. B. Transaktionen bei Bitcoin) enthalten.

\hfill --- Technische Grundlagen und datenschutzrechtliche Fragen der Blockchain-Technologie \cite{bohme2017technische}
\end{quotation}

\begin{figure}
\includegraphics[width=\textwidth]{figures/blockchain.png}
\caption{Abgebildet ist die inkrementale Annäherung an das Blockchain-Konzept \cite{bohme2017technische}}
\label{fig:blockchain}
\end{figure}

Diese letzte Definition ist besonders hilfreich, da sie technischer und recht konkret ist. Mit Betrachtung dieser Definition sollte das tiefergehende Konzept hinter dem Wort Blockchain im Abschnitt 3, welcher das \enquote{How to time-stamp a digital document}-Paper \cite{timestamp} zum Inhalt hat, verständlicher werden. Mit Ausnahme des Arbeitsnachweises wird dort ein dieser Definition entsprechendes Blockchainmodell diskutiert.

\subsection{\enquote{Public key}-Verschlüsselung und Signaturen}

Vor den 70er-Jahren gab es lediglich symmetrische Verschlüsselungsverfahren bei welchen Sender und Empfänger denselben Schlüssel besitzen müssen. \cite{diffie1988first}[S. 560] Dies bringt problematische Erfordernisse, wie Sicherung und geheime Übermittlung des Schlüssels mit sich. Dem gegenüber steht die Idee des \enquote{Public key}-Kryptosystems, welches wie folgt definiert wird:

\begin{quotation}
Jeder Nutzer veröffentlicht in einer öffentlichen Datei eine Verschlüsselungsprozedur $E$. Der Nutzer hält die Details seiner korrespondierenden Entschlüsselungsprozedur $D$ geheim. Diese Prozeduren haben folgende Eigenschaften:
\begin{enumerate}
\item Das Entschlüsseln einer verschlüsselten Nachricht $M$ ergibt $M$. Formal, $$D(E(M)) = M.$$
\item Sowohl $E$ als auch $D$ sind einfach zu berechnen.
\item Durch Veröffentlichung von $E$ erleichtert der Nutzer nicht die Berechnung von $D$. Praktisch kann er nur Nachrichten, welche mit $E$ verschlüsselt sind entschlüsseln oder $D$ errechnen.
\item Falls eine Nachricht $M$ erst entschlüsselt und anschließend wieder verschlüsselt wird, resultiert $M$. Formal, $$E(D(M)) = M.$$
\end{enumerate}

\hfill --- RSA Paper, 1978 \cite{rivest1978method}[Original in Englisch]
\end{quotation}

Zusammengefasst bedeutet dies, dass mit einem \enquote{Public key}-Kryptosystem ein öffentlicher Schlüssel und ein privater Schlüssel existieren. Der öffentliche Schlüssel gibt keinen Hinweis auf den privaten; solange dieser private Schlüssel geheim bleibt, nützt der öffentliche Schlüssel einem Angreifer nicht. Der private Schlüssel kann zudem genutzt werden, um von beliebigen Nachrichten bzw. Daten $M$ eine Signatur  $S = D(M)$ zu erstellen. Nun kann jeder Nutzer prüfen, ob die Nachricht wirklich vom behaupteten Sender stammt, indem er mit dem korrespondierenden öffentlichen Schlüssel prüft, ob $E(S) = M$. Ist das der Fall, kann die Signatur nur vom entsprechenden Besitzer des privaten Schlüssels $D$ erstellt worden sein, da dieser geheim ist.

\subsection{Hashing}

In ihrem Paper \enquote{How to time-stamp a digital document} \cite{timestamp} machen Haber und Stornetta gebrauch von der Annahme einer perfekten Hashingfunktion, um sich mühselige Rechenarbeit zu ersparen:

\begin{quotation}
\noindent In Vereinfachung nutzen wir eine Familie kryptografisch sicherer, kollisionsfreier Hash-Funktionen. Diese Familie von Funktionen $h: \{0,1\}^* \rightarrow \{0,1\}^l$ komprimiert Bitstrings beliebiger Länge zu Strings der Länge $l$, mit folgenden Eigenschaften:
\begin{enumerate}
\item Die Funktionen $h$ sind einfach zu berechnen und es ist einfach zufällig ein Mitglied der Familie zu wählen.
\item Gegeben eine der Funktionen $h$ ist es berechnungstechnisch unmöglich ein Paar verschiedener Strings $x,x'$ zu finden, welche $h(x)=h(x')$ erfüllen. (Ein solches Paar wird Kollision von $h$ genannt.)
\end{enumerate}

\hfill --- \enquote{How to time-stamp a digital document} \enquote{How to time-stamp a digital document} \cite{timestamp}[Original in Englisch]
\end{quotation}

In der Realität existieren diese zwar nicht, aber eine Kollision muss lediglich unwahrscheinlich genug sein, sodass ein Angreifer diese nicht mit zumutbaren Aufwand berechnen kann.

\section{\enquote{How to time-stamp a digital document} von Stuart Haber \& Scott Stornetta}

Die beiden Autoren Stuart Haber und Scott Stornetta arbeiteten für den aus AT\&T hervorgegangene R\&D-Zweig Bellcore \cite{bellcore} als sie 1991 im \enquote{Journal of Cryptology} ihr Paper \enquote{How to time-stamp a digital document} \cite{timestamp} veröffentlichen, welches einen Grundstein für heutige Blockchain-Technologien darstellt. Ihr Paper skizziert zwei Ideen zur Verifizierung von Änderungs- und Erstellungsdatum für beliebige digitale Dokumente, welche nachfolgend dargestellt werden. Zuvor beschreiben die Autoren einen naiven Ansatz, um die Vorteile ihres neuen Verfahrens zu verdeutlichen.

Hauptmotiv für die Entwicklung eines Zeitstempeldienstes  digitaler Dokumente ist für die Autoren, dass es bei bestimmten Dokumenten wichtig sein kann, dass das Datum einer kritischen Prüfung standhält. Etwa bei geistigem Eigentum, welches vor Gericht angezweifelt wird. Die Autoren beschreiben, wie durch eine Verkettung von Verfahren, wie hintereinanderfolgende Notizen in einem Laborjournal und regelmäßige Zeichnung durch einen Notar in einer Firma eine ausreichend glaubwürdige Vertrauenskette aufgebaut werden kann. Und zwar so, dass eine Fälschung so schwierig würde, dass diese bemerkt werden würde. Dies wollen die Autoren in ihrem Paper mathemathisch beweisbar und rechentechnisch praktisch umsetzbar für digitale Dokumente erreichen. \cite{timestamp}[Vgl.]

\subsection{Der naive Ansatz}

Den naiven Ansatz fassen die Autoren unter dem Begriff eines digitalen Banksafes\footnote{\enquote{digital safety-deposit box} \cite{timestamp}} zusammen. Die Idee hierbei ist, dass Nutzer ihr Dokument einem Zeitstempelservice (ZSS) schicken. Der ZSS vermerkt das Datum zu welchem das Dokument eingeschickt wurde und speichert eine Kopie ab. Möchte der Nutzer nun gegenüber einem Dritten das Alter der Dokuments beweisen, so kann dieser das Dokument dem ZSS schicken, welcher es mit der gespeicherten Kopie abgleicht und das Vorlagedatum ausgibt. Dieser Ansatz hat gleich mehrere gravierende Nachteile:

\begin{itemize}
\item Die Geheimhaltung des Dokumenteninhalts ist nicht gewährleistet -- während der Übermittlung und durch die Kopie beim ZSS entstehen Angriffsmöglichkeiten.
\item Bandbreite und Speicherplatz könnten limitiert sein, was die Nutzung des ZSS bei großen Dokumenten erschwert.
\item Das Dokument kann während der Übertragung korrupt werden, verlorengehen oder später beim ZSS abhanden kommen. In diesen Fällen ist das Zertifikat für den Nutzer wertlos.
\item Grundsätzlich hindert bei dem besprochenen Ansatz nichts den ZSS daran mit einem Nutzer zu kolludieren, um gefälschte Zeitstempel zu verbreiten.
\end{itemize}

Dieser Ansatz ließe sich noch weiter verbesseren, indem statt der Dokumente Hashwerte selbiger versandt
 werden und der ZSS Hashwert und Datum signieren und ein entsprechendes Zertifikat an den Nutzer senden würde. \cite{timestamp}[Vgl.]

\subsection{Linking}

Wir beginnen mit der Beobachtung, dass die Sequenz der Nutzer und der Hashwerte ihrer Dokumente nicht vorhersagbar sind. Speichern wir also Bits aus den vorherigen Anfragen in das auszustellende Zertifikat ein, so können wir uns sicher sein, dass das Zertifikat nach den vorigen Anfragen erstellt wurde. Eine Vordatierung ist ausgeschlossen. Selbiges sichert das Zertifikat auch gegen eine Manipulation in die andere Richtung ab. Eine Rückdatierung wird ausgeschlossen, da Bits der zu manipulierenden Anfrage in die später ausgestellten Zertifikate gespeichert werden müssen. Diese sind jedoch nach Definition bereits ausgestellt worden.

Eine Zeitstempelanfrage besteht aus einem $l$-Bit-String und einer Nutzer-ID. Wir verwenden $\sigma(\cdot)$ um die Signaturprozedur des ZSS zu beschreiben. Der ZSS stellt signierte, sequenziell numerierte Zeitstempelzertifikate aus. Als Antwort auf die $n$-te Anfrage $(y_n, ID_n)$ in der Sequenz durch unseren Nutzer, führt der ZSS zwei Operationen aus:

\begin{enumerate}
\item Der ZSS sendet dem Nutzer das signierte Zertifikat $s = \sigma(C_n)$, wobei das Zertifikat
$$C_n = (n, t_n, ID_n, y_n; L_n)$$
sich aus der Sequenznummer $n$, der Zeit $t_n$, der Nutzer-ID $ID_n$ und dem Hashwert $y_n$ aus der Anfrage, sowie der Linkinginformation des zuvor ausgestellten Zertifikats $L_n = (t_{n-1}, ID_{n-1}, y_{n-1}, H(L_{n-1}))$ zusammensetzt.
\item Wird die nächste Anfrage bearbeitet, sendet der ZSS dem Nutzer die Nutzer-ID $ID_{n+1}$ der nächsten Anfrage zu.
\end{enumerate}

Hat der Nutzer $s$ und $ID_{n+1}$ vom ZSS erhalten, prüft er, dass $s$ eine gültige Signatur eines korrekten Zertifikats ist (Zeit und Hashwert sind korrekt etc.).

Wird das Zeitstempelzertifikat des Dokumentes $x$ später angezweifelt, kann der Zweifler damit beginnen, das Zertifikat $(s, ID_{n+1})$ zu überprüfen. Hierbei muss vorallem geprüft werden, ob $s$ eine korrekte Signatur eines Zertifikats ist, welches den Hash von $x$ und das behauptete Datum enthält. Um sicherzustellen, dass keine Kollusion zwischen dem Nutzer und dem ZSS vorliegt, kann der Zweifler den Nutzer $ID_{n+2}$ um dessen Zeitstempelzertifikat $(s', ID_{n+2})$ bitten, welches eine Signatur
$$s' = \sigma(n+1, t_{n+1}, ID_{n+1}, y_{n+1}, L_{n+1})$$
eines Zertifikats enthält, das in seiner Linkinginformation $L_{n+1}$ eine Kopie seines Hashwertes $y_n$ enthält. Diese Linkinginformation wird weiter dadurch abgesichert, dass sie den Hashwert der Linkinginformation des angezweifelten Dokuments $H(L_n)$ enthält. So kann der Zweifler beliebig lang die Signatursequenz vorwärts, aber auch rückwärts überprüfen. \cite{timestamp}[Vgl.]

\subsection{Distributed Trust}

Für diesen Ansatz gehen wir davon aus, dass ein sicheres Signaturverfahren existiert, welches es allen Nutzern erlaubt Nachrichten zu signieren. Zudem wird von der Existenz eines Pseudozufallszahlengenerators $G$ ausgegangen, welcher allen Nutzern zur Verfügung steht. Ein \textit{Pseudozufallszahlengenerator} ist ein Algorithmus, der kurze Startwerte in Ausgabesequenzen überführt, welche von keinem Algorithmus von zufälligen Sequenzen unterschieden werden können -- also nicht vorhersagbar sind.

Wieder beginnen wir mit dem Hashwert $y$ des Dokumentes welches unser Nutzer mit Nutzer-ID $ID$ zeitstempeln lassen möchte. Wir nutzen $y$ als Startwert unseres Pseudozufallszahlengenerators, dessen Ausgabe als $k$-Tupel von Nutzer-IDs aufgefasst werden kann:
$$G(y) = (ID_1, ID_2, ... , ID_k).$$

Der Nutzer schickt nun seine Anfrage $(y, ID)$ an alle $k$ Nutzer des Tupels. Als Antwort erhält er von Nutzer $ID_j$ eine signierte Nachricht $s_j=\sigma_j(t, ID, y)$, welche die Zeit $t$ enthält. Der Zeitstempel besteht aus $[(y,ID), s_1, ... , s_k]$. Die $k$ Signaturen $s_j$ können leicht vom Nutzer selbst oder einem Zweifler überprüft werden. Das Vertrauen entsteht durch die zufällige Auswahl der Nutzer, ein Angreifer müsste eine große Menge an Nutzern unter seiner Kontrolle haben, um realistische Chancen für eine wahrheitswidrige Zertifikaterstellung zu haben. Dies kommt auch im gewählten Namen \emph{Distributed Trust} zum Ausdruck: Das Vertrauen wird auf mehrere Nutzer verteilt. \cite{timestamp}[Vgl.]

\section{Zusammenfassung und Ausblick}

Der Begriff Blockchain ist unscharf, mit Kenntnis der Konzepte welche \enquote{How to time-stamp a digital document} von Stuart Haber \& Scott Stornetta beschreibt, kann man sich durch diese Konkretisierung der Probleme und Lösungsmöglichkeiten dennoch an ihn annähern. Die Vertrauenskette des \enquote{Linking}-Konzepts beruht darauf, dass alle Zertifkate der Sequenz durch Linkinginformationen verkettet werden, sodass ein nachträgliches oder vorzeitiges Einschieben eines neues Kettenglieds unmöglich wird. Der \enquote{Distributed Trust}-Ansatz schöpft sein Vertrauen aus der zufälligen Auswahl aus einem Nutzerpool, welche jeweilig das Dokumentendatum mit einer Zeitsignatur verbriefen. Der Zufall soll sicherstellen, dass kein Angreifer mit Hilfe von unter seiner Kontrolle stehenden Nutzern falsche Zeitsignaturen ausstellen kann.

Hier ist eine Verbindung beider Verfahren der nächste denkbare Schritt, um noch höhere Sicherheit zu gewährleisten. Außerdem nennen die Autoren Haber \& Stornetta in ihrer Arbeit noch weitere Möglichkeiten zur praktischen Implementierung welche hier keine Erwähnung fanden.

Der Schritt die obig dargestellten Verfahren zu erweitern, sodass diese nicht nur signierte Zeitzertifikate sind, sondern statt der Zeit auch andere Informationen, wie ausführbaren Code, Währungstransaktionsdetails, Kontostände und sonstiges enthalten, liegt nicht fern und ist heute vielfältig realisiert.
