\author{Tim Cech}
\email{Tim.Cech@student.hpi.uni-potsdam.de}
\group{BP2017P1 -- \enquote{Wolke sucht Herzschlag}}
%\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Grundlagen der Interpretation des 12-Ableitungen Elektrokardiogramms}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle
\begin{abstract}
 Das 12-Ableitungen-Elektrokardiogramm ist eine der wichtigsten Oberflächentechniken elektrische Phänomene im Herzen bildhaft darzustellen. Jede der 12 Messpunkte (Ableitung) repräsentiert eine Achse zum Herzen, indem auftretende elektrische Ströme gemessen werden. Die heutzutage standardmäßig verwendeten 12 Ableitungen sind nach den Arbeiten von Einthoven, Goldberg und Wilson normiert und haben sich in der klinischen Praxis als besonders geeignet erwiesen, Phänomene im Herzen zu beobachten. Für die korrekte Interpretation eines 12-Ableitungen Elektrokardiogramms sind Kenntnisse über den Aufbau des PQRST-Komplexes, der grundsätzlich die Kontraktionen und Entspannungen im Herzrhythmus im Elektrokardiogramm abbildet, sowie physiologischer Grundkenntnisse über die Funktionsweise des Herzens erforderlich. Anhand eines Vergleiches zwischen einem gesunden Herzrhythmus (Sinusrhythmus) und eines krankhaften kann exemplarisch veranschaulicht werden, wo Ansatzpunkte für die Entdeckung von pathologischen (potentiell krankhaften) Phänomenen im Elektrokardiogramm vorhanden sind.	
\end{abstract}


\section{Einleitung}

Das Herz ist für die Steuerung des Blutkreislaufes essentiell. Viele chemische Prozesse im Körper brauchen unter anderem Sauerstoff (z.B. die essentielle Bereitstellung von dem Energieträger ATP für den Wechselstoff) für ihren korrekten Ablauf. Dieser Sauerstoff wird von den roten Blutkörperchen transportiert und den Zellen so zur Verfügung gestellt. Nach dem Verbrauch des Sauerstoffs werden die roten Blutkörperchen mit Kohlenstoffdioxid angereichert, welches sie dann über das Herz wieder zur Lunge transportieren, wo es veratmet wird. Danach werden die roten Blutkörperchen wieder mit Sauerstoff aus der Atemluft angereichert, ehe das Herz dieses sauerstoffreiche Blut zurück in den Körper pumpt. 

Das Herz kann seine Pumpfunktion im menschlichen Körper durch Kontraktions- und Entspannungsphasen erfüllen. Die Kontraktionen werden durch minimale elektrischen Ströme im Herzen meist nur im Millivoltbereich verursacht. Abweichungen von den normalen Strömen können ein Indiz dafür sein, dass eine Herzkrankheit vorliegt. Dazu müssen aber diese elektrischen Ströme in einer Art und Weise erfasst werden, dass eine Messung im klinischen und notärztlichen Rahmen jederzeit vorgenommen werden kann und trotzdem möglichst prägnante Aussagen über die Herzströme ermöglicht.  

Eine solche Möglichkeit bietet das Elektrokardiogramm (daher die bildhafte Darstellung von elektrischen Strömen, die das Herz betreffen (kardia - altgriechisch für: verwandt mit dem Herz)). Für Elektrokardiogramm ist die Abkürzung EKG gebräuchlich. Unter dieser Abkürzung wird meistens auch das später vorgestellte  übliche 12-Ableitungen Elektrokardiogramm verstanden.\cite[S.~14-15.]{Isabel1} Das EKG ermöglicht es das Herz ohne aufwändige und zum Teil gefährliche innere Untersuchung, daher unter Öffnung des Brustkorbes, zu untersuchen. Für seine Erfassung werden verschiedene Elektroden auf den Körper platziert und so die elektrischen Ströme im Herzen gemessen. Das EKG zählt damit zu den Oberflächen-Untersuchungsmethoden. Dabei werden die verschiedenen Phasen der Herzkontraktion durch die Elektroden gemessen und in das EKG abgebildet. Diese Phasen sind dann durch typischen Wellen (P und T-Welle) und zwei oder drei Zacken abgebildet (qRS-Komplex).

Der Beginn der Entwicklung des modernen EKG wird mit 1844 angegeben.\cite{frankfurt} In diesem Jahr konnte Carlo Matteucci in Experimenten an einem Taubenherz nachweisen, dass die Erregungsbildung und der Schlag des Herzens auf elektrischen Phänomenen beruht. Dies legte die Grundlage für verschiedene Versuche, diese vorhandenen elektrischen Ströme systematisch zu messen und zu interpretieren. Das erste EKG im weiteren Sinne wurde jedoch erst 1882 von Augustus Desiré Waller abgenommen, als er Experimente an seinem Hund durchführte. Er tauchte die Pfoten seines Hundes in Silberchloridlösung und konnte dann an diesen Ableitungen (daher eine spezifische von den tatsächlichen Herzströmen hergeleitete Messung \cite{duden2} ) abnehmen.\cite{duisburg}  Das Oberflächen-EKG am Menschen entstand in seiner heutigen Form ab 1903 mit der Forschung Willem Einthovens. Dieser schlug zuerst nur eine einzige bipolare Ableitung entlang des rechten und linken Armes vor, welche später zur modernen Ableitung I werden sollte. Später ergänzte aber Einthoven diese erste bipolare Ableitung durch zwei weitere Ableitungen II und III, um mehr Sichtweisen auf die Arbeit des Herzens zu bekommen und so genauere Aussagen darüber stellen zu können. Diese ersten drei Ableitungen wurden später von Nehb, Wilson und Goldberg ergänzt, wobei im modernen EKG die Ableitungen nach Nehb nur noch selten Verwendung finden.\cite{frankfurt}

\section{Physiologische Grundlagen zur Erfassung des EKG} \label{sec:structure}
Das Herz regelt den Blutaustausch zwischen dem großen und kleinen Blutkreislauf des Körpers. Der kleine Blutkreislauf wird auch Lungenkreislauf genannt. Dieser umfasst den Transport des sauerstoffarmen Bluts zur Lunge, wo dieses Kohlenstoffdioxid abgibt und danach Sauerstoff aufnimmt. Anschließend fließt das nun sauerstoffreiche Blut zurück zum Herzen, womit sich der kleine Blutkreislauf schließt. Das Herz pumpt nun das sauerstoffreiche Blut in den großen Blutkreislauf. In diesem wird das Blut überall im Körper zu Zellen transportiert, welche Sauerstoff für ihre biochemischen Reaktionen benötigen, ehe es mit Abfallprodukten dieser Reaktionen, vor allem Kohlenstoffdioxid, beladen wieder zum Herz zurückfließt. Hiermit schließt sich auch der große Blutkreislauf und das Blut geht als Nächstes wieder in den kleinen Blutkreislauf ein.

\subsection{Grundsätzlicher Ablauf des Herzrhythmus} 

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Herzrhythmus}
	\caption{Schematische Darstellung eines Herzens mit besonderer Annotierung der für den Herzrhyhtmus relevanten Abläufe \cite{Herz1}}
	\label{fig:Herzrhythmus}
\end{figure}

Das Herz ist ein Hohlorgan, das größtenteils aus Muskelgewebe besteht. Es wird durch die Herzscheidewand in eine linke und rechte Herzkammer und linken und rechten Vorhof unterteilt (vergleiche \autoref{fig:Herzrhythmus}). Es ist kontinuierlich mit dem Blutaustausch zwischen dem großen und kleinen Blutkreislauf beschäftigt. Dazu wird in jedem Herzschlag Blut aus einem Kreislauf in die entsprechende Kammer gepumpt und dann bei der nächsten Kontraktion in den jeweils anderen Kreislauf gepumpt. In  \autoref{fig:Herzrhythmus} ist eine schematische Abbildung des Herzens dargestellt. Vor der Kontraktion läuft sauerstoffarmes Blut aus dem Körper in den rechten Vorhof, während sauerstoffreiches Blut aus dem Lungenkreislauf in den linken Vorhof fließt. Zu diesem Zeitpunkt sind die Klappen zwischen Vorhof und Kammer noch geschlossen. Sie öffnen sich während der Entspannungsphase des Herzens, wodurch sich das Herz weitet und sich auch die Klappen öffnen, womit das Blut in die jeweilige Herzkammer fließen kann. Dann beginnt das Herz sich zusammenzuziehen (Kontraktion), wodurch das Blut wie bei einer Pumpe aus der Kammer in den Blutkreislauf herausgeschleudert wird. Danach beginnt das Herz, sich wieder zu entspannen, wodurch ein neuer Herzschlag beginnen kann. Dieser kontinuierliche Prozess der Entspannung, der Füllung und des Pumpens wird Herzrhythmus genannt. Eine einzelne Iteration der Kontraktion und Entspannung wird Herzschlag genannt. Dabei ist es wichtig, dass die Kontraktion weder zu schnell noch zu langsam ist. Wäre sie zu schnell, so könnte das Blut nicht hinreichend angesaugt werden und könnte nicht aus der Herzkammer gepumpt werden, womit der Herzrhythmus gestört wäre. Ist die Kontraktion hingegen zu langsam, so wäre sie nicht fähig, genug Kraft aufzubringen, das Blut aus dem Herzen herauszupumpen, was ähnliche Folgen haben würde. Die Kontraktion und Entspannung werden über elektrische Impulse gesteuert, die im nächsten Abschnitt beleuchtet werden. (Die Darstellungen in diesem Absatz folgen \cite{Bayer})

\subsection{Grundlagen der Erregungsbildung und -leitung des Herzens}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Herzimpuls}
	\caption{Schematische Darstellung eines Herzens mit besonderer Annotierung der für die Erregungsbildung relevanten Abläufe \cite[S.~12.]{Isabel1}}
	\label{fig:Herzimpuls}
\end{figure}

In \autoref{fig:Herzimpuls} ist eine andere Darstellung des Herzens zu sehen, wobei hier insbesondere Fokus auf die Leitung der elektrischen Impulse durch das Gewebe des Herzens gelegt wurde. Der elektrische Impuls für die Herzkontraktion wird im sogenannten Sinusknoten im rechten Vorhof gebildet. Danach breitet sich der Impuls in den Vorhöfen aus. Dabei werden diese depolarisiert. Die Herzkammern sind im gesunden Herzen vom Impuls durch Gewebe isoliert, außer beim Atrioventrikularknoten (kurz AV-Knoten). Wie in der \autoref{fig:Herzimpuls} befindet sich der AV-Knoten an der Schnittstelle zwischen Herzvorhöfen und Herzkammern.\cite{doccheck2} Da dieser auf diese Weise einen natürlichen Flaschenhals bildet, kommt es zu einer kurzen Verzögerung, ehe sich der Impuls über den AV-Knoten auf das His-Bündel ausbreitet, von wo er dann die Herzkammern depolarisieren kann. Wie in \autoref{fig:Herzimpuls} zu sehen ist, schließt das His-Bündel an den AV-Knoten an. Das His-Bündel durchstößt im gesunden Herzen als einziges das Herz-Skelett (daher das isolierende Gewebe) und bildet so die einzige "Brücke" zwischen den Vorhöfen und Herzkammern. \cite{doccheck1}  Währenddessen klingt der Impuls in den Vorhöfen ab, wodurch es zur Repolarisierung der Vorhöfe kommt. In den Herzkammern breitet sich das Signal, nun immer langsamer werdend, über die Tawara-Schenkel auf das Purkinje-Fasernetz aus, womit die gesamte Kammer erregt wird und es zur Kontraktion kommt. Die Tawara-Schenkel und das Purkinje-Fasernetz sind feine Gewebestrukturen, die das Signal im Herzen verteilen.\cite{spektrum}\cite{medlex} Danach bildet sich der Impuls zurück, wodurch es zur Repolarisation der Herzkammern kommt, womit dann die Iteration beendet ist und nach einer kurzen Pause ein neuer Impuls im Sinusknoten gebildet wird. Im gesunden Körper sind bei einem erwachsenen Menschen 50 bis 100 Schläge pro Minute normal, wobei die Länge eines Schlages um bis zu ca. 0,12 Sekunden variieren kann.\cite[S.~12-13.]{Isabel1}

\section{Die Erfassung des EKG}
Eine bestimmte Messung der Herzströme wird als Ableitung bezeichnet. Es werden bipolare und unipolare Ableitungen unterschieden, wobei bipolare Ableitungen einen echten Plus- und Minuspol haben, während bei unipolaren Ableitungen ein Pol gegen einen starken Widerstand verschaltet wird, womit dieser als isoelektrisch (daher gleichbleibend sich nicht auf die Messung auswirkend) angenommen wird. Ein modernes EKG wird normalerweise mit 12 Ableitungen erfasst, wovon sechs Ableitungen Extremitätsableitungen und die anderen sechs Ableitungen Brustwandableitungen sind. Weiterhin existieren weitere selten verwendete oder veraltete Ableitungen.

\subsection{Die Extremitätsableitungen}
Die sechs Extremitätsableitungen unterteilen sich wiederum in die drei bipolaren Ableitungen nach Einthoven (Ableitung I, II, III) und die drei unipolaren Ableitungen nach Goldberg (aVR, aVL, aVF).\cite[S.~14-15.]{Isabel1} Allen Extrimitätsableitungen ist gemein, dass sie am linken, rechten Arm oder/und linken Bein abgeleitet werden. Dies rührt daher, dass das Herz eher in der linken Körperhälfte gelegen ist, daher Messungen, die die linke Körperseite betonen, signifikantere Ergebnisse liefern. An das rechte Bein wird manchmal noch eine Erdungselektrode angelegt, welche aber irrelevant für die eigentliche Messung ist und deswegen auch keine standardisierte Position hat.\cite{thieme}

\subsubsection{Die bipolaren Ableitungen nach Einthoven}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Einthovenableitungen}
	\caption{Anschluss der Ableitungen nach Einthoven \cite{Ableitung}}
	\label{fig:Einthoven}
\end{figure}

In \autoref{fig:Einthoven} ist schematisch dargestellt, wie die Ableitungen I-III abgenommen werden. Dabei ist insbesondere relevant, welche Elektroden zur Abnahme der jeweiligen Ableitung verschaltet werden. Ableitung I misst den Herzstrom in Richtung vom rechten Arm zum linken Arm, Ableitung II vom rechten Arm zum linken Bein und Ableitung III vom linken Arm zum linken Bein. Zur korrekten Abnahme ist es erforderlich, dass die Elektroden korrekt, wie in \autoref{fig:Einthoven} dargestellt,  verschaltet sind, da ansonsten nur eine invertierte oder völlig nicht interpretierbare Ableitung herauskommt.\cite{Ableitung}\cite[S.~14-15.]{Isabel1} 

\subsubsection{Die unipolaren Ableitungen nach Goldberg}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Goldbergableitungen}
	\caption{Anschluss der Ableitungen nach Goldberg \cite{Ableitung2}}
	\label{fig:Goldberg}
\end{figure}

In \autoref{fig:Goldberg} ist die Art und Weise wie die drei Ableitungen nach Goldberg abgenommen werden dargestellt. Dabei wird das Potential einer Elektrode gegenüber der beiden anderen gemessen. Dazu werden die beiden anderen Elektroden, die genau die gleichen wie bei den Ableitungen nach Einthoven sind, für diese Ableitung mit einem starken Widerstand von ca. 5000 Ohm verschaltet.\cite[S.~14-15.]{Isabel1} Dadurch wird das messbare Potential der verbliebenen Elektrode auch verstärkt, weswegen die Ableitungen mit augmented Voltage Right (Kurz aVR. Im Deutschen ungefähr: Verstärkte Spannung rechts), augmented Voltage Left (Kurz aVL. Im Deutschen ungefähr: Verstärkte Spannung links) und augmented Voltage Feet (Kurz aVF. Im Deutschen ungefähr: Verstärkte Spannung am Fuß) bezeichnet werden.\cite{Ableitung2} Durch den Widerstand sind die messbaren Potentiale der beiden Elektroden nahezu null, womit der Minuspol in diesen Ableitungen nicht gemessen wird. Da nur das gemessene Potential einer Elektrode ausschlaggebend ist, werden die Ableitungen nach Goldberg als unipolar bezeichnet. 

Da zwei Elektroden am Minuspol geschaltet sind, gegenüber der dritten Elektrode am Pluspol, wird ungefähr die Seitenhalbierende der zuvor gemessenen Achsen bei den Ableitungen nach Einthoven gemessen. Daher werden die sechs Extremitätsableitungen üblicherweise auch im sogenannten Einthoven-Dreieck zusammengefasst, welches die Messrichtungen zusammenfassend darstellt.\cite[S.~14-15.]{Isabel1}

\subsection{Die Brustwandableitungen}

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{figures/Wilson}
	\caption{Messbereiche und Positionierung der Ableitungen nach Wilson \cite{Ableitung3}}
	\label{fig:Wilson}
\end{figure}

Neben den schon betrachteten Extremitätsableitungen werden noch weitere Ableitungen in direkter Nähe zum Herzen angebracht, um vor allem konkrete Phänomene lokalisieren zu können. Üblicherweise in Verwendung sind nur die sechs unipolaren Brustwandableitungen nach Wilson V1-V6. Diese werden, von der linken Brustvorderwand beginnend, weiter nach links, bis zur Seite geklebt und können so messen, ob Phänomene eher in der Vorderwand oder in der Seite auftreten.\cite[S.~17.]{Isabel1} Die Elektroden werden vor allem in der linken Körperhälfte geklebt, da sie so dem auch eher links gelegenen Herzen näher sind. Selten werden die auf den Rücken geklebten unipolaren Ableitungen V7-V9 verwendet, die noch weiter links von V6 geklebt sind. Daher messen sie vor allem die Ströme in der Herzhinterwand. Diese werden jedoch aus technischen Gründen im normalen EKG nicht verwendet.\cite[S.~17.]{Isabel1} Dies gilt auch für die auf die rechte Brustvorderwand geklebten unipolaren Ableitungen V1R-V3R, da diese normalerweise zu weit vom Herzen entfernt sind, um Phänomene signifikant aufzeigen zu können.\cite[S.~14-15.]{Isabel1} Die bipolaren Brustwandableitungen nach Nehb gelten als veraltet und finden heutzutage kaum noch Einsatz, da sich die sechs unipolaren Ableitungen nach Wilson sich in der klinischen Praxis als besser geeignet für die EKG-Erfassung erwiesen.\cite[S.~14-15.]{Isabel1} 


\section{Der Aufbau der Kurve einer Ableitung}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/EKG-Welle}
	\caption{Schritte der Herzkontraktion und ihre Entsprechung im EKG \cite{EKG-Welle}}
	\label{fig:EKG-Welle}
\end{figure}

Die Korrelation zwischen dem Erregungsverlauf im Herzen und den typischerweise erfassten Kurvenverläufe sind in \autoref{fig:EKG-Welle} dargestellt.

\begin{itemize}
 \item Die im dritten Kapitel erwähnte Erregungsbildung im Sinusknoten, kann nicht im EKG beobachtet werden. Das Phänomen zeigt zu kleine elektrische Ausschläge, um mit dieser Methode beobachtet werden zu können. Zwar können aus anderen Beobachtungen Rückschlüsse auf die Arbeit des Sinusknotens gezogen werden, allerdings kann sie nicht direkt beobachtet werden.\cite[S.~12-13.]{Isabel1} Dafür sind andere Untersuchungsmethoden zu verwenden, auf die hier nicht näher eingegangen werden soll.

 \item Als erstes kann die Depolarisierung der Vorhöfe beobachten werden. Diese beginnt langsam am Sinusknoten und umfasst dann immer mehr Bereiche, ehe sie am AV-Knoten gehemmt bzw. verzögert wird. Diese ungefähr gleichmäßige Ausbreitung beobachten wir als zumeist positive Wellenform, die sogenannte P-Welle. Die P-Welle dauert normalerweise 0,05-0,1 Sekunden. (vergleiche auch \autoref{fig:EKG-Welle} oberste Reihe erstes Bild)\cite[S.~18-19.]{Isabel1} 

 \item Danach kommt es durch die Hemmung des AV-Knoten und His-Bündels als Flaschenhals zu einer kurzen Pause in der Erregungsleitung. Diese ist als isoelektrische Linie (daher wieder auf dem Grundniveau) bis zum QRS-Komplex zu beobachten und wird PQ-Strecke genannt.(vergleiche auch \autoref{fig:EKG-Welle} oberste Reihe zweites Bild)\cite{frankfurt} 

 \item Als nächstes breitet sich die Erregung in den Herzkammern aus, was diese depolarisiert. Dabei ist zunächst eine kleine, in manchen Ableitungen kaum sichtbare, negative Zacke zu beobachten, die sogenannte Q-Zacke. Diese repräsentiert die initiale Kammererregung. (vergleiche auch \autoref{fig:EKG-Welle} mittlere Reihe erstes Bild) Im gesunden Herzen ist die Höhe der Q-Zacke höchstens ein viertel der Höhe der folgenden R-Zacke und dauert maximal 0,03 Sekunden. Dies sind die sogenannten Pardee-Kriterien, eine Q-Zacke welches sie erfüllt, wird Pardee-Q genannt. Eine deutliche Q-Zacke in einer der Ableitungen V1-V4, ist pathologisch und darf daher im gesunden Herzen nicht beobachtet werden.\cite[S.~22.]{Isabel1} 

 \item Danach folgen jeweils eine schlanke positive R-Zacke und eine negative S-Zacke.(vergleiche auch \autoref{fig:EKG-Welle} mittlere Reihe zweites und drittes Bild) In V1-V6 ist im gesunden Herzen die sogenannte R-Progression zu beobachten. Die R-Progression beschreibt, dass in V1-V6 die R-Zacke sukzessive größer wird, während die S-Zacke kleiner wird. Die R-Zacke wird entweder zwischen V2 und V3 oder V3 und V4 größer als die S-Zacke. Diesen Übergang nennt man Umschlagzone. In V6 soll maximal nur noch eine kleine S-Zacke zu beobachten sein. Ist dort noch immer eine deutliche S-Zacke zu sehen oder nimmt diese über V1-V6 kaum ab, spricht man von einer S-Persistenz.\cite[S.~22-23.]{Isabel1} Da die Q-Zacke normalerweise bedeutend kleiner ist als die R und S-Zacke, wird meist zusammenfassend qRS-Komplex geschrieben, um die Größenverhältnisse anzudeuten. Ein qRS-Komplex im gesunden Herzen dauert normalerweise 0,06 bis zu 0,1 Sekunden.\cite[S.~22-23.]{Isabel1} 

 \item Nach der vollständigen Erregungsausbreitung kommt es zur intraventrikulären Erregungsrückbildung. Diese ist auch als nahezu isoelektrische Linie im Beginn zu beobachten und bildet damit die sogenannte ST-Strecke. (vergleiche auch \autoref{fig:EKG-Welle} unterste Reihe erstes Bild)

 \item Gegen Ende dieser Erregungsrückbildung kommt es ähnlich zur P-Welle, zur sogenannten T-Welle. (vergleiche auch \autoref{fig:EKG-Welle} unterste Reihe zweites Bild) Diese ist zumeist positiv, darf aber in V1 regelmäßig auch negativ sein. Normalerweise hat die T-Welle eine Amplitude, die eine Größe zwischen 1/6 und 2/3 der Größe der R-Zacke hat. In V1-V3 kann die T-Welle dieses Maximum auch überschreiten ohne notwendig pathologisch zu sein.\cite[S.~23-24.]{Isabel1} 
 
 \item Sowohl für die P, als auch die T-Welle gilt: Ist der dazugehörige qRS-Komplex größtenteils negativ, so darf auch die Welle negativ sein, ohne ein pathologisches Anzeichen zu sein. Die intraventrikuläre Erregungsrückbildung wird auch Repolarisierung der Herzkammern genannt.\cite[S.~18-19.]{Isabel1}\cite[S.~23-24.]{Isabel1}
 
 \item Während der Ausbreitung der Erregung in den Herzkammern, kommt es in den Vorhöfen zur Repolarisierung bzw. Erregungsrückbildung. Die Depolarisierung in den Herzkammern ist jedoch deutlich stärker, daher kann dieses Phänomen nicht mit einem EKG beobachtet werden, da der QRS-Komplex diesen Vorgang vollständig überdeckt.\cite[S.~12-13.]{Isabel1}
\end{itemize}

\section{Die beispielhafte Interpretation eines EKG}

Die bisher beschriebenen Phänomene, Techniken und Regeln können nun dafür benutzt werden, ein EKG korrekt zu erfassen und zu interpretieren. Dabei ist die Unterscheidung zwischen drei verschiedenen EKG-Arten vornehmlich wichtig: Dem Sinusrhythmus, einem potentiell pathologischen Rhythmus und einem nicht bestimmbaren EKG. Es ist schwer, aus einem EKG einen unbedingte pathologischen, daher krankhaften, Fehler im oder am Herzen zu schlussfolgern, da viele Phänomene konstitutionell sein können, daher zwar typisch krankhaft sind, jedoch ohne, dass eine Krankheit beim Patienten vorliegt. Je nach Phänomen ist eine konstitutionelle Ursache wahrscheinlicher oder die absolute Ausnahme und daher erst als allerletzte Erklärung anzunehmen. Es sollte jedoch immer bedacht werden, dass es besser ist, eine krankhafte Ursache anzunehmen und sich zu irren, als eine konstitutionelle, da letztere Variante die Gesundheit des Patienten gefährden könnte.

Ein EKG wird auf standardisiertem Papier erfasst, dabei bedeutet der Abstand eines kleines Kästchen auf der x-Achse das Vergehen von 0,02 Sekunden. Das EKG-Papier wird kontinuierlich durch einen Drucker gezogen und entsprechend der Taktung bedruckt. Der Ausschlag des EKG von einem kleinen Kästchen in y-Richtung entspricht einer gemessenen Spannung von 0,2 Millivolt. Dementsprechend entspricht ein großes Kästchen einer vergangenen Dauer von 0,1 Sekunden und eine gemessenen Stromstärke von 1 Millivolt und der Papiervorschub beträgt 50 Millimeter pro Sekunde.\cite{muenster}

\subsection{Das EKG eines Sinusrhythmus}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Sinusrhythmus}
	\caption{Ein beispielhaftes 12-Ableitungen EKG eines Sinusrhythmus mit regelmäßigen P und T-Wellen und einem unauffälligem QRS-Komplex \cite[S.~150-151.]{Isabel1}}
	\label{fig:Sinusrhythmus}
\end{figure}

Der Sinusrhythmus ist der normale Rhythmus des Herzens.\cite[S.~38.]{Isabel1} Der hier in \autoref{fig:Sinusrhythmus} dargestellte Rhythmus ist ein normaler Rhythmus, daher er hat keine normalen Veränderungen wie beispielsweise Sinusbradyarrythmie. Eine Sinusbradyarrythmie ist eine Variante des Sinusrhythmus, bei dem die Herzfrequenz unregelmäßig und eher langsam ist.\cite[S.~150-151.]{Isabel1} In V1-V6 sehen wir eine normale R-Progression, schon in V4 ist keine deutliche S-Zacke mehr erkennbar, womit auch keine S-Persistenz vorliegt. Die Umschlagszone der R-Progression ist zwischen V2 und V3 angesiedelt. Weiterhin erfüllt jedes Q die Pardee-Kriterien (vergleiche dazu vorigen Oberpunkt vierter Stichpunkt), auch ist keine Q-Zacke in V1-V4 sichtbar. Die negative P und T-Welle in der aVR-Ableitung ist auch regelkonform, da der dazugehörige qRS-Komplex vorwiegend negativ ist. Weiterhin ist auch die Amplitude der T-Welle weder zu hoch noch zu niedrig in Relation zur Spitze der R-Zacke. Weiterhin gilt für die T-Welle in V1 die gleiche Regel, wie für die Wellen in aVR. Es wird auch nicht kritisch, dass nur eine der beiden Wellen negativ ist, da sie bei einem zugehörigen vorwiegend negativem qRS-Komplex nur negativ sein können, aber auch positiv bei entsprechender Höhe normal sind.

\subsection{Ein krankhaftes EKG}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/Vorderwandinfarkt}
	\caption{Ein beispielhaftes 12-Ableitungen EKG eines Vorderwandinfarktes mit unter anderem ST-Streckenhebungen \cite[S.~200-201.]{Isabel1}}
	\label{fig:Vorderwandinfarkt}
\end{figure}

In \autoref{fig:Vorderwandinfarkt} ist das EKG eines akuten Vorderwandinfarktes sichtbar.\cite[S.~200-201.]{Isabel1} Auffällig in der Ableitung I ist die immer mehr abfallende Welle des EKG allgemein. In V2-V5 ist eine deutliche ST-Streckenhebung zu sehen. Das bedeutet, dass die ST-Strecke beginnt, bevor die S-Zacke auf ihr ursprüngliches Niveau zurückgesunken ist. Auch ist eine mangelnde R-Progression zu beobachten. Zwischen V1-V4 baut sich das R fast überhaupt nicht auf und auch danach bleibt es auffällig klein. Die Umschlagszone ist auch zwischen V4 und V5 verschoben, wo sie nicht auftreten sollte. Q Zacken sind kaum zu sehen. Die R und S-Zacken sind in den Extremitätenableitungen unauffällig. Die negativen P und T-Wellen sind in diesen Ableitungen auch wieder regelkonform.

Da die Auffälligkeiten vor allem in den Ableitungen V1-V4 auftreten, die vor allem über die Vorderwand des Herzens verklebt werden, kann ein Problem in dieser Zone vermutet werden. Für eine exakte Diagnose wären allerdings noch mehr Aspekte in Betracht zu ziehen. Die Analyse, wie hier dargestellt, wäre nur ausreichend, um weitere Analysen und Untersuchungen zu veranlassen.

\subsection{Ein nicht-interpretierbares EKG}

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{figures/Gestoertes}
	\caption{Ein durch Muskelzittern verzerrtes EKG \cite[S.~252-253.]{Isabel1}}
	\label{fig:Gestoertes-EKG}
\end{figure}

Ein EKG kann durch viele verschiedene Faktoren nicht mehr nutzbar werden. So können nah gelegene Stromquellen die gemessenen Stromflüsse überlagern oder verändern. Der Patient sollte keine Elektrogeräte am Körper tragen und andere Quellen sollten möglichst fern gehalten werden, da ansonsten die Ableitungen Merkmale zeigen, die nichts mit den Stromflüssen im Herzen zu tun haben. Auch sollte darauf geachtet werden, dass die Elektroden richtig angeschlossen sind und die Ableitungen richtig verschaltet sind. Ansonsten könnte beispielsweise eine mangelhafte R-Progression beobachtet werden, wo keine ist. Zuletzt sollte darauf geachtet werden, dass der Patient nicht, wie in \autoref{fig:Gestoertes-EKG} dargestellt, zittert. Dies kann regelmäßig auf Grund von Angstzuständen oder zu niedrigen Temperaturen im Untersuchungsraum auftreten.\cite[S.~145-146.]{Isabel1}

Solche gestörten EKG-Elemente werden Artefakte genannt. Sie sind grundsätzlich nicht dafür geeignet, Aussagen zu treffen. Selbst scheinbare eindeutige Anzeichen, wie in der obigen Abbildung, sind nicht für Diagnosen geeignet. Scheinbar können wir oben den qRS-Komplex trotz der Muskelartefakte noch erkennen, aber auch dieser kann durch das Zittern der Muskeln verändert werden und ist daher für eine Analyse ungeeignet.

\section{Zusammenfassung und Ausblick}

Das moderne EKG entwickelte sich seit dem 19.Jahrhundert. Es misst die Erregungsleitung im Herzen, wobei die Erregungsbildung im Sinusknoten und die Repolarisation der Vorhöfe mit dieser Methode nicht beobachtet werden können. Das normale EKG hat 12 Ableitungen. Zur korrekten Klassifizierung eines EKG sind Kenntnisse über den qRS-Komplex, die P-Welle und die T-Welle essentiell. Die Q-Zacke muss dabei im gesunden Herzen die sogenannten Pardee-Kriterien erfüllen. Die Ableitungen V1-V6 müssen eine regelkonforme R-Progression aufzeigen und es darf in V6 kein deutlich sichtbares S mehr verbleiben. Die P und T-Wellen sind zumeist positiv, außer wenn ihr zugehöriger qRS-Komplex vorwiegend negativ ist, dann können sie, müssen aber nicht, negativ sein. Abweichungen von diesem Schema sprechen oft für ein krankhaftes Phänomen am oder im Herzen, wobei es viele Ausnahmen gibt.

Zur besseren Klassifikation des EKG können noch weitere Aspekte untersucht werden. Zunächst sind hier die Lagetypbestimmung und der Herztakt zu kennen. Auch die bekannten Abweichungen vom Sinusrhythmus, die trotzdem auf ein völlig gesundes Herz schließen lassen, sollten weiter ausgeleuchtet werden. Abschließend sollte bei vielen Analyseergebnissen noch einmal die Unterscheidung getroffen werden, ob das beobachtete Phänomen immer pathologisch ist oder nur teilweise pathologisch. Auch bleibt für die weitere Arbeit offen, inwieweit solche feinen Unterschiede überhaupt bei der Klassifizierung betrachtet werden sollten oder nicht im Zweifel dann die Entscheidung des Arztes eingeholt werden sollte.

Für die algorithmische Arbeit wird es nötig sein die kontinuierlichen EKG-Daten in einen analogen Datensatz umzuwandeln, der das eigentliche EKG gut repräsentiert. Eine dafür sehr übliche Methode ist das Sampeln\cite{physionet}, daher wird ein gemessener Wert in kurzen Zeitabständen erfasst, um so eine gute Repräsentation der Daten zu erhalten. Dabei wäre es wünschenswert, möglichst alle 12 Ableitungen zu sampeln, da teilweise nur über mehrere Ableitungen hinweg Phänomene untersucht werden können (beispielsweise die R-Progression). Soll auf den Daten anschließend ein Klassifizierer trainiert werden, um normale Schläge, pathologische Schläge und Artefakte automatisch unterscheiden zu können, so wäre es wünschenswert eine möglichst große Bandbreite an Trainingsdaten zu haben. Es gibt viele, teilweise seltene, Phänomene, die im EKG beobachtet werden können, allerdings ist ihre Klassifizierung ohne das Bewusstsein, über die Existenz dieser Krankhaften, selbst für einen ausgebildeten Kardiologen (daher Fachmediziner für das Herz) schwer. (vergleiche \cite{Isabel1}) Unter diesen Voraussetzungen ist es jedoch durchaus vorstellbar, dass zukünftige Arbeit, einen Klassifizierer schafft, der sehr zuverlässig die Art der Schläge klassifizieren kann. Der Sinusrhythmus hat eine sehr klare Form und die Ausnahmen davon sind bekannt und können gut umschrieben werden (z.B. etwas unregelmäßiger langsamer Herzschlag). Daher können Abweichungen von ihm schnell entdeckt und eine entsprechende Klassifizierung vorgenommen werden. Artefakte zeichnen sich als letzte Kategorie durch große Schwankungen in den Werten aus, die völlig untypisch für das EKG sind, weswegen auch diese Klassifizierung möglich sein sollte.