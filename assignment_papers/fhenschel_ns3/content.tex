\author{Florian Henschel}
\email{florian.henschel@student.hpi.uni-potsdam.de}
\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{NS -- 3}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/ns3logo}
	\caption{Das offielle Logo von ns--3 \footfullcite{ns3logo}}
\end{figure}

\begin{abstract}
Die Effizienz der Nutzung von Computern kann durch die Vernetzung dieser gesteigert werden. Um dies effizient zu gestalten, muss das Netzwerk aufgrund der heutigen Technologievielfalt sehr genau geplant werden. Dies wird zum Beispiel durch Netzwerksimulatoren, wie ns--3, ermöglicht. Der auf diskreten Events basierende Ablauf einer ns--3 Simulation ermöglicht eine detaillierte Planung des zu prüfenden Szenarios, aus welchem sich sehr detaillierte Infortmationen gewinnen lassen. Dies kann auf verschiedenen Wegen geschehen: ns--3 unterstützt Logging auf 7 Leveln, das Aufnehmen von realistischen .pcap Dateien und eine komplexes eigenes Tracingsystem. Simulationen werden in C++ programmiert, mittels AWL und TCL kann der Ablauf und die Auswertung der Szenarien komplexer gestaltet werden und mittels Python kann das gesamte ns--3 System verändert werden. Diese vergleichsweise Low-Level Einflussmöglichkeiten stellen jedoch auch einen der Nachteile von ns--3 dar, dem es zum Beispiel gegenüber OMNET++ an einer umfangreichen GUI mangelt. Die somit verhältnismäßig komplizierte Nutzung von ns--3 wird jedoch durch die vielen Helferfunktionen wieder einsteigerfreundlicher gestaltet.
\end{abstract}


\section{Einleitung} \label{einleitung}
Eine Interaktion des Menschen mit dem Computer zu dessen frühen Zeiten war nur an der physischen Position des Computers möglich und der Austausch von Informationen zwischen verschiedenen Rechnern musste zum Beispiel durch die manuelle Übertragung der Daten über Lochkarten durchgeführt werden. Die direkte Verbindung von Recheneinheiten ermöglicht die Vermeidung derartiger Flaschenhälse der Datenverarbeitung, da beispielsweise eine elektrische Übertragung der Daten wesentlich schneller abläuft. Als daher die ersten Versuche unternommen wurden, Computer miteinander zu vernetzen, war die Menge an Technologien zur Verbindung und Kommunikation der Computer noch sehr klein. Außerdem umfassten die darauf basierenden Netzwerke aufgrund der geringeren Verbreitung von Rechnern im Vergleich zu Heute wesentlich weniger Knoten, die es zu verbinden galt. Dies ermöglichte es, dass für ein Netzwerk eine der wenigen, für den gegebenen Fall passenden, Technologien ausgewählt werden konnte und das Netzwerk direkt in physischer Form mit dieser konstruiert werden konnte.

Durch die weitere Forschung an (und Entwicklung von) neuen Netzwerktechnologien wuchs der Pool an passenden Systemen pro Anwendungsfall und auch die Komplexität der einzelnen Protokolle und Schnittstellen. Weiterhin stieg die Verbreitung der Rechner auch stark, sodass Netzwerke heute aus wesentlich mehr Knoten bestehen als früher. Trotz dieser Veränderungen muss der Aufbau von Netzwerken aber auch heute noch möglich sein. Probleme, die erst ab einer bestimmten Größe eines wachsenden Netzwerks auftreten oder Vorteile, die sich aus einer anderen Verbindungstechnologie ergeben würden, lassen sich jedoch vor allem aus finanziellen und zeitlichen Gründen nicht mehr so einfach erkennen. 

An dieser Stelle spielen moderne Netzwerksimulatoren eine wichtige Rolle. Sie ermöglichen eine relativ zeiteffiziente und kosteneffiziente Planung von Netzwerken mittels einer Simulation des finalen Netzwerks. Dabei lassen sich verschiedene Parameter, wie die Anzahl an Knoten des Netzwerks oder die verwendete Technologie schnell austauschen oder auch die Reaktion des Netzes auf Ausfälle von Komponenten überprüfen. Dazu muss die Simulation jedoch auch möglichst viele Informationen bereitstellen, um eine transparente Einsicht in das System zu ermöglichen.

Eine diese Simulationsumgebungen ist ns--3, deren Möglichkeiten im Folgenden weiter beleuchtet werden sollen.

\section{Allgemeine Informationen}
ns--3 ist seit 2008 der offizielle Nachfolger von ns--2, welcher durch das ns--3 Consortium finanziell und organisatorisch unterstützt wird. Der Netzwerksimulator ist unter der \enquote{GNU GPLv2 license} lizenziert und das Ziel der Entwickler ist das Erreichen einer zentralen Rolle in der Bildung und Forschung.\footfullcite{noauthor_ns_2017} Dafür ist laut der Webseite eine Mitarbeit der Nutzer am System und das Hinzufügen neuer Features durchaus erwünscht, dabei gilt es jedoch, die teilweise ungewöhnlichen Standards des Codes zu beachten, welche im Tutorial an einigen Stellen erwähnt werden.

Funktional handelt es sich um eine Simulation, welche auf diskreten Ereignissen basiert, welche im System mit einem festen Zeitpunkt in einer Warteschlange vermerkt werden, welche dann bei der Durchführung der Simulation abgearbeitet wird. Dabei können Ereignisse, wie der Start einer Application (siehe \autoref{application} Application), schon vor dem Simulationsstart geplant werden oder, wie das periodische Senden eines Beacon frames\footfullcite{beaconFrame} eines W--Lan Routers, zur Laufzeit von Systemkomponenten hinzugefügt werden.

\section{Terminologie} \label{terminologie}
Eine Simulation in ns--3 orientiert sich relativ nah an der Realität und versucht daher die Aspekte, die ein vernetztes System auszeichnen, abstrakt darzustellen. Da zum Beispiel die Komponente der Netzwerkanschlusses eines Rechners je nach verwendeter Technologie sehr verschieden ausfällt (UMTS Antenne, Ethernet-Port, ...), besitzen die abstrakten Komponenten des Konzepts allgemeine Bezeichnungen, deren Kenntnis für die Arbeit mit ns--3 nötig ist.
 
Die folgenden Begriffen finden sich alle zumindest in Form einer abstrakten Klasse im ns--3 System wieder und daher wird im Folgenden auch auf einige Konfigurationsmöglichkeiten der einzelnen Komponenten hingewiesen, welche in Form von Attributen in den Klassen realisiert werden.\footfullcite[23-24]{nsnam:Tutorial}

\subsection{Node} 
Im Prinzip ist er die Repräsentation eines Computers in der Simulation. Da im Zentrum von ns--3 der Aufbau von Netzwerken steht, welche sich abstrakt als Graph darstellen lassen, orientierte man sich bei dieser Bezeichnung an der Graphentheorie. Begriffe wie \enquote{Host} wurden vermieden, da diese eher in Verbindung mit dem Internet genutzt werden, ns--3 jedoch nicht nur zur Simulation des Internets benutzt wird.
	
Ein Node an sich besitzt meist keinerlei Funktionalität und dient eher als Gerüst für eine funktionale Einheit im Netzwerk. Zur Nutzung des Nodes werden daher auf ihm z.B. NetDevices installiert, Applications gestartet oder der Node wird in einem simulierten 3-dimensionalen Raum bewegt, was zum Beispiel bei der Simulation eines W--Lan Netzwerkes relevant wird.
	
\subsection{NetDevice} 
Dies ist im Prinzip der Netzwerkanschluss des simulierten Rechners, welcher zur Technologie des genutzten Channels passen muss. In der Realität wäre dies zum Beispiel eine UMTS Antenne im Handy oder die Ethernetkarte im PC. Das NetDevice umfasst in ns--3 jedoch auch gleichzeitig noch die in der Realität vorhandene Treibersoftware des Gerätes, da in der Simulation von Betriebssystemkonzepten abstrahiert wird. Wie in einem realen Rechner ist es möglich, mehrere NetDevices in einem Node zu installieren.
	
\subsection{Channel} 
Hinter diesem Begriff verbirgt sich eine der umfangreichsten Komponenten von ns--3. Ein Channel ist im Wesentlichen die Verbindung zwischen 2 oder mehr NetDevices und somit dann in der fertigen Simulation die Verbindung der Computer/Nodes. Jedoch stellt der Channel je nach verwendeter Netzwerktechnologie unterschiedliche Dinge dar: 
	
Bei einer einfachen P2P Verbindung von 2 Nodes über ein Lankabel ist der Channel nur das Kabel, dessen Übertragungsrate in der Simulation konfiguriert werden kann. Bei einem W--Lan Netzwerk repräsentiert der Channel jedoch den gesamten simulierten 3-dimensionalen Raum zwischen den beteiligten Nodes und in ihm können Umweltobjekte modelliert werden, welche Einfluss auf das Signal nehmen könnten.
	
\subsection{Application} \label{application}
Dies sind die Programme, welche die meisten Events in der Simulation auslösen und dem Netzwerk einen funktionalen Sinn geben. Dazu können Programme geschrieben werden, welche von der Basisklasse \enquote{Application} erben. Sie werden dann auf einem Node installiert und der Start der Application wird in der Event--queue der Simulation eingetragen. Weiterhin kann das Programm auch zu einer bestimmten Zeit vom System beendet werden.
	
\section{Helfer -- Funktionen}
Die Erstellung einer Netzwerksimulation erfordert das korrekte Kombinieren der in \autoref{terminologie} erwähnten Komponenten des ns--3 Systems und die Initialisierung der simulierten Netzwerkprotokolle. Dies wird in größeren Netzwerken sehr aufwendig und umfangreich, aufgrund der Anzahl an Komponenten. Um das effiziente Erstellen von Simulationen skalierbar zu halten, wurde eine Vielzahl von \enquote{Helfer-Funktionen} in das System integriert. Diese ermöglichen zum Beispiel den unkomplizierten Aufbau von homogenen Teilnetzen:

\begin{lstlisting}
PointToPointHelper p2P;
p2P.SetDeviceAttribute ("DataRate", StringValue ("42Mbps"));
p2P.SetChannelAttribute ("Delay", StringValue ("7ms"));

NetDeviceContainer p2pDevices;
p2pDevices = pointToPoint.Install (p2pNodes);
\end{lstlisting}

Ein Helfer zur Erstellung eines P2P Netzwerkes wird deklariert und die wichtigsten Attribute der Verbindung (Delay und Übertragungsrate) werden gesetzt. Im nächsten Schritt wird diese Konfiguration auf eine skalierbare Menge von Nodes installiert (Container sind im Wesentlichen Arrays mit den Komponenten), sodass ein vollverbundenes P2P Netzwerk entsteht, und als Rückgabewert erhält man einen Container mit allen erstellten NetDevices. Ohne den Helfer müsste jedes Paar von Nodes einzeln verbunden werden, mit extra erstellten und konfigurierten Channels und NetDevices.

Andere Helfer installieren zum Beispiel den \enquote{Internetstack}, welcher zur Kommunikation via IP nötig ist, auf einer Menge von Nodes oder befüllen automatisch alle Routingtabellen eines Netzwerkes mit korrekten Einträgen.\footfullcite[29ff]{nsnam:Tutorial}

\section{Interaktion mit dem System}
Um den Ansprüchen für eine Nutzung in Forschung und Bildung gerecht zu werden, ist eine Vielfalt an Interaktionsmöglichkeiten nötig. Dabei geht es sowohl um die Eingabe von Parametern in das System, als auch um die Gewinnung von den richtigen Informationen. Diese sollten jedoch einfach zu verarbeiten sein und auch die Lage der Informationsquellen sollte steuerbar sein.

\subsection{Veränderung der Simulation}
Ein Ziel der Entwickler von ns--3 war es, die Simulation nach dem Kompilieren der C++ Quelldateien weiterhin dynamisch veränderbar zu halten. Aus diesem Grund beinhaltet das Konzept des Systems ein einheitliches Parsing von Kommandozeilen Argumenten.

Das Durchführen einer Simulation läuft relativ einfach ab: Das Beispielskript \enquote{test} des Szenarios wird im \lstinline{scratch} Ordner des Hauptverzeichnisses abgelegt und mittels Ausführung der Datei \lstinline{waf} kompiliert. Anschließend kann die Simulation mit
\begin{lstlisting}
$ ./waf --run scratch/test
\end{lstlisting}
gestartet werden. Wenn das Szenario \lstinline{test} nun ein P2P Netzwerk beinhaltet, dessen Datenrate nicht konfiguriert und somit auf die Standardrate gesetzt wurde, so ist es mittels Argumente an die Simulation möglich, diese Standardrate zu ändern und den Ablauf zu beeinflussen:
\begin{lstlisting}
$ ./waf --run "scratch/test --ns3::PointToPointNetDevice::DataRate=42Mbps"
\end{lstlisting} 

Neben diesen bereits implementierten Befehlen können auch eigene Variablen leicht von außen zugänglich gemacht werden. Die Codezeile
\begin{lstlisting}
cmd.AddValue("nNodes", "Number of Nodes in the network", nNodes);
\end{lstlisting}
ermöglicht es, eine vorher festgelegte Variable nNodes zur Ausführungszeit zu setzen und bei Bedarf eine Erklärung der Variablenfunktion auf der Kommandozeile auszugeben.
\begin{lstlisting}
$ ./waf --run "scratch/test --nNodes=42"
\end{lstlisting}

\subsection{Informationsgewinnung}
Für die Informationsgewinnung stellt ns--3 verschiedenste Möglichkeiten bereit, welche sich sowohl in der Komplexität ihrer Bedienbarkeit, als auch in der Qualität ihrer Informationen stark unterscheiden. Dabei bildet Logging die eher systemweite, grobe Komponente und das Tracen mittels \enquote{Source} und \enquote{Sink} das filigrane Werkzeug.

\subsubsection{Logging}
Logging ist im Bereich der Softwareentwicklung ein relativ weit verbreitetes Verfahren der Informationsgewinnung und auch in ns--3 funktioniert es ähnlich wie in vielen anderen Systemen. Beim Logging werden bestimmte, im Programmcode festgelegte, \enquote{Logeinträge} (z.B. \enquote{Variable x hat den Wert: \textit{Ausgabe von x}}) entweder in der Konsole ausgegeben oder bei Bedarf in eine Datei geschrieben. Dazu sind die Lognachrichten einem der 7 Log-Level zugeordnet:

\begin{itemize}
	\item \verb|LOG_ERROR|
	\item \verb|LOG_WARN|
	\item \verb|LOG_DEBUG|
	\item \verb|LOG_INFO|
	\item \verb|LOG_FUNCTION|
	\item \verb|LOG_LOGIC|
	\item \verb|LOG_ALL|
\end{itemize}

(Zusätzlich ist es auch möglich, das Logging zu deaktivieren.) Außerdem sollte zu Beginn des Simulationsskripts mittels 
\begin{lstlisting}
NS_LOG_COMPONENT_DEFINE ("loggingComponent");
\end{lstlisting}
eine Konstante festgelegt werden, über die bei der Ausführung der Simulation das Logginglevel einzelner Teile festgelegt werden kann. Mittels \lstinline{export NS_LOG=UdpEchoClientApplication=level_all} oder dem für die verwendete Shell passenden Befehl zum Setzen von Umgebungsvariablen kann dann zum Beispiel das Log-Level einer Application in der Simulation vor deren Ausführung bestimmt werden.\footcite[36]{nsnam:Tutorial}

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/consoleExecute}
	\caption{Output einer einfachen Simulation auf dem \lstinline{LOG_INFO} Level}
	\label{console}
\end{figure}

In \autoref{console} wird beispielhaft die Konsolenausgabe einer Simulation mit einem Echo-Request und der entsprechenden Echo-Response mit dem Infologlevel gezeigt. Diese umfasst nur die wesentlichen Abläufe, ohne z.B. auf Hintergründe der Netzwerkkommunikation einzugehen. 

\subsubsection{ASCII Tracing}
Das ASCII Tracing bezeichnet im Prinzip die Ausgabe von bestimmten Ereignissen der Simulation. Angenommen die Variable \lstinline{p2P} speichert einen Helfer, mit dem zuvor ein P2P Netzwerk erstellt wurde. Mit den Codezeilen

\begin{lstlisting}
AsciiTraceHelper ascii;
p2P.EnableAsciiAll (ascii.CreateFileStream ("asciitrace.tr"));
\end{lstlisting}

ist es möglich, die Events, die auf allen NetDevices des P2P Netzwerkes stattfanden, auszugeben. Da die entsprechende Ausgabe meist sehr umfangreich ist, wird hier nur beispielhaft ein Eintrag eines ASCII Traces dargestellt:

\begin{lstlisting}
+ 
2.01782 
/NodeList/1/DeviceList/1/$ns3::CsmaNetDevice/TxQueue/Enqueue ns3::EthernetHeader 
( length/type=0x806, source=00:00:00:00:00:03, destination=ff:ff:ff:ff:ff:ff) 
ns3::ArpHeader (request source mac: 00-06-00:00:00:00:00:03 source ipv4: 10.1.2.1 
dest ipv4: 10.1.2.4) Payload (size=18) ns3::EthernetTrailer (fcs=0)
\end{lstlisting}

\textit{(Im Normalfall werden die Ereignisse ohne Zeilenumbrüche geloggt, doch diese erleichtern hier das Verständnis für die Struktur.)}

Die wesentlichen Bestandteile des Events sind der Status (+ für enqueue des Events), der Zeitpunkt (die Zahl in der 2. Zeile in Sekunden), die Herkunft des Events (der Pfad gibt den Ort des NetDevice in der Simulation an) und einige Zusatzinformationen zum Event (in diesem Fall der Inhalt des ARP Request Paketes).

\subsubsection{.pcap capturing}
Zur Überwachung der Netzwerkinterfaces in einem realen Netzwerk können Programme wie Wireshark\footcite{wiresharkWebsite} genutzt werden. Diese zeichnen alle Pakete auf, die über ein spezifiziertes Interface übertragen werden und speichern diese Aufzeichnungen in .pcap Dateien. Auch ns--3 unterstützt das Aufzeichnen in .pcap Dateien und diese können dementsprechend in Wireshark ausgewertet werden.

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/wiresharkPcap}
	\caption{Auswertung einer .pcap Datei aus einer Simulation in Wireshark}
	\label{wireshark}
\end{figure}

In \autoref{wireshark} ist ein Ausschnitt einer .pcap Datei aus einem Simulierten W-Lan Netzwerk zu sehen. Die Aufzeichnung beginnt mit der Anmeldung einiger Rechner im W-Lan, darauf folgen die für diesen Netzwerktypen speziellen Beacon frames\footfullcite{beaconFrame} und am Ende ist der Beginn einer Kommunikation mit ARP-Request erkennbar. 

\subsubsection{kontrolliertes Tracing}
Die komplexeste Methode der Informationsgewinnung, aber auch die am besten steuerbare, ist die Verwendung des ns--3 eigenen Tracing Systems. Eine detaillierte Erklärung des Programmcodes wäre an dieser Stelle zu viel, aber das Grundprinzip funktioniert nach folgendem Schema:

Komponenten des ns--3 Systems besitzen eine Vielzahl von Attributen. Soll es nun möglich sein, den Zustand eines dieser Attribute zu überwachen, so wird es im System als \enquote{Tracing--Source} registriert. Dies hat zu Folge, dass bei jeder Veränderung dieses Attributes eine außerdem im System registrierte Menge an \enquote{Tracing--Sinks} benachrichtigt wird und sowohl den alten, als auch den neuen Wert des Attributes erhält. Diese \enquote{Tracing--Sinks} sind Funktionspointer, deren Ziel eine Funktion ist, welche die entsprechenden Attributwerte als Parameter entgegennehmen kann und dann weiterverarbeitet.

\section{Schwächen von ns--3}
Den beschriebenen umfangreichen Funktionalitäten von ns--3 stehen jedoch auch einige Schwächen des Systems gegenüber. Die ns--3 Entwickler sind sich dieser scheinbar bewusst und beseitigen diese auch: Beispielsweise fehlte in ns--2 noch ausreichende Unterstützung von \enquote{Wireless Sensor Networks}\footfullcite{musznicki_survey_2012}, wohingegen diese in ns--3 ausreichend gewährleistet ist.\footfullcite{noauthor_reason_nodate}

Andere bleiben jedoch bestehen: Einer der größten Kritikpunkte an ns--3 ist der Mangel einer dedizierten Entwicklungsumgebung oder dedizierter grafischer Ausgaben der Ergebnisse der Simulationen. Der Markt der Netzwerksimulatoren ist groß und viele sind auf unterschiedliche Anwendungsbereiche zugeschnitten. Unter den Simulatoren, die auf ähnliche Dinge wie ns--3 ausgelegt sind, gibt es zum Beispiel OMNET++.

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/omnetEclipse}
	\caption{Eine Entwicklungsumgebung für OMNET++\footfullcite{omnetEclipse}}
	\label{omnetEclipseImage}
\end{figure}

Dieser Netzwerksimulator besitzt eine Entwicklungsumgebung, welche auf der IDE \enquote{Eclipse} basiert und viele weitere grafische Elemente besitzt (\autoref{omnetEclipseImage}). Außerdem bietet er eine wesentlich anschaulichere Auswertung der Ergebnisse mittels strukturierter GUI an (\autoref{omnetGREImage}). 

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{figures/omnetGRE}
	\caption{Eine Simulation in OMNET++ zur Laufzeit\footfullcite{omnetGRE}}
	\label{omnetGREImage}
\end{figure}

\section{weitere Funktionalitäten}
Das reine Simulieren von Netzwerken reicht mitunter für manche Anwendungsfälle jedoch nicht aus oder hebt die Komplexität der Aufgabe stark an. So müsste beispielsweise eine an die Simulationsumgebung angepasste Version einer Software erstellt werden, wenn diese im Netzwerk getestet werden soll. Um dieses und andere Probleme zu umgehen, ermöglicht das ns--3 System die Verbindung von Realität und Simulation, sodass zum Beispiel nur die Verbindungsinfrastruktur simuliert wird, die Rechner jedoch teilweise real sind.

\section{Fazit}
Der Netzwerksimulator ns--3 wird seinen Ansprüchen gerecht, eine zentrale Rolle in der Forschung und der Bildung einnehmen zu können. Durch das passende Level der Abstraktion von der Realität und der Möglichkeit, eigene Protokolle zu implementieren und in den Simulationen zu verwenden ist es dem Nutzer möglich, eine Vielzahl verschiedener Szenarien zu simulieren. Weiterhin kann durch das breite Spektrum an Tracingoptionen genau festgelegt werden, an welcher Stelle welche Informationen in einer beliebigen Form an den Entwickler zurückgegeben werden sollen. An diesem Punkt werden jedoch auch die Nachteile des Systems deutlich: im Vergleich zu beispielsweise OMNET++ verfügt ns--3 nicht über eine dedizierte Entwicklungsumgebung oder eine GUI zur Auswertung einer Simulation. ns--3 ist also eher als komplexes Werkzeug für umfangreiche Simulationen mit einigen wenigen Einsteigertools zu betrachten.