\author{Melvin Witte}
\email{melvin.witte@student.hpi.de}
\group{BP2017P2 -- IoT \& Blockchain - Rail2X SmartServices}
%\group{BP2017P2 -- \enquote{IoT \& Blockchain - Rail2X SmartServices}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Zuverlässige Systeme und Fehlertoleranz}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle
\begin{abstract}
	In verteilten Systemen können Fehler auftreten. Im Nachfolgenden wird kurz erläutert, was ein Fehler ist, welche Metriken und Eigenschaften man zur Verfügung hat, um ein System bezüglich der Zuverlässigkeit zu beurteilen, welche Arten von Fehlern es gibt und welche Möglichkeiten man zur Verfügung hat, mit diesen Fehlern umzugehen -- Fehlermaskierung durch Redundanz in Hardware- und Softwarekomponenten, sowie in den Datenstrukturen, oder Fehlerbehandlung durch Erkennen, Eingrenzen und Korrigieren des Zustandsfehlers. Das Problem des distributed commit wird mithilfe von fehlertoleranten Algorithmen vorgestellt, um ein Gefühl dafür zu kriegen, was für fehlertolerante Algorithmen erforderlich ist.
\end{abstract}

\section{Einleitung}
Wenn ein verteiltes System verspricht, dass es seinen Dienst korrekt anbietet, dann hat es zum einen Anforderungen, die es erfüllen muss und Mechanismen, die sicherstellen, dass es diese Anforderungen auch erfüllen kann. Dazu werden Eigenschaften wie \enquote{Zuverlässigkeit} und \enquote{Fehlertoleranz} des Systems zu Hilfe gezogen.\cite[S. 321ff.]{tanenbaum_distributed_2007} Im folgenden wird näher darauf eingegangen, was diese Begriffe überhaupt bedeuten und welche Mittel man zur Verfügung hat, um diese Eigenschaften zu erfüllen.

\section{Zuverlässigkeits- und Fehlerdefinition}
Bevor ich Zuverlässigkeit und Fehler behandle, kläre ich einige Begriffe, die ich später für die Definitionen benutze.

\subsection{Begriffsklärung}
Ein \emph{System} ist erstmal nur ein Objekt, dass mit anderen Objekten interagiert. Die anderen Systeme sind die \emph{Umgebung}, die gemeinsame Grenze ist die \emph{Systemgrenze}. Die \emph{Funktionalität} des Systems ist durch die \emph{funktionalen Anforderungen} definiert und beschreibt, was das System tun soll. Das \emph{Verhalten} des Systems beschreibt, was das System tut, um die Funktionalität zu erreichen und wird durch eine Abfolge von Zuständen beschrieben.
Der \emph{Dienst}, den ein System, in diesem Fall \emph{Anbieter} genannt, liefert,  ist nun das Verhalten, welches vom Nutzer wahrgenommen wird. Ein \emph{Nutzer} ist ein anderes System, welches den gelieferten Dienst erhält. Diese Interaktion findet in der \emph{Dienst-Schnittstelle} statt, der Zustand des Systems, der nach außen hin wahrnehmbar ist, heißt \emph{externer Zustand}, der restliche Zustand heißt \emph{interner Zustand}. Ein Dienst ist dann \emph{korrekt}, wenn er die Funktionalität, entsprechend der funktionalen Anforderungen, richtig implementiert. Im Normalfall implementiert ein System mehrerer Funktionalitäten und liefert mehrere Dienste\cite[S. 3f.]{avizienis_dependability_2004}

\subsection{Fehlerdefinition}
In verteilten Systemen unterscheidet man grundsätzlich zwischen drei Fehlerarten: Bei einem \enquote{Ausfall} (englisch \enquote{failure}) liefert das System seine Dienste nicht mehr korrekt. Bei einem \enquote{partiellen Ausfall} wird jedoch noch ein Teil der Dienste korrekt geliefert. Der Ausfall kann bezüglich seiner \enquote{Schwere} eingeordnet werden (siehe \ref{subsection:systemausfall}). Der fehlerhafte Zustand, der zu einem Ausfall führt, wird \enquote{Zustandsfehler} (englisch \enquote{error}) genannt. Die \enquote{Fehlerursache} (engl. \enquote{fault}) ist der Grund, der zum Zustandsfehler führt. Dabei unterscheidet man auch zwischen \enquote{aktiven Fehlerursachen} (englisch \enquote{active faults}), die schon einen Zustandsfehler erzeugt haben und \enquote{schlafenden Fehlerursachen}, die noch keinen Fehlerzustand erzeugt haben.\cite[S. 4]{avizienis_dependability_2004} In Bezug auf diese verschiedenen Fehler kann man nun verschiedene Eigenschaften definieren, die ein \enquote{zuverlässiges System} erfüllen sollte.

\subsection{Zuverlässiges System}
Allgemein spricht man von einem \enquote{zuverlässigen System}, wenn ein Nutzer dem System vertrauen kann, dass es seine Dienste korrekt anbietet, oder spezieller, dass man Systemausfälle soweit vermeidet, dass Häufigkeit und Schwere der Ausfälle für den Nutzer akzeptabel sind. Dazu gibt es verschiedene Eigenschaften, die ein verteiltes System zu einem gewissen Ausmaß erfüllen kann:\cite[S. 5]{avizienis_dependability_2004}

\begin{description}
	\item[Verfügbarkeit] beschreibt die Bereitschaft des Systems, seinen Dienst korrekt anzubieten. Als Metrik für Verfügbarkeit kann man die Wahrscheinlichkeit, dass ein System zu einem zufällig gewählten Zeitpunkt dem Nutzer seine Dienste korrekt anbieten kann, benutzen, oder, anders formuliert, die Wahrscheinlichkeit, dass zu diesem Moment kein Ausfall vorliegt..
	
	\item[Vertrauenswürdigkeit] beschreibt die kontinuierliche und korrekte Bereitstellung seines Dienstes. Als Metrik kann man das Zeitintervall, in welchem ein System ohne Ausfall läuft, benutzen, z.B. gemessen durch die durchschnittliche Zeit bis zum nächsten Ausfall (englisch \enquote{mean time to failure, MTTF}) benutzen.
	
	\item[Katastrophensicherheit] besagt, dass bei einem Ausfall keine Katastrophe für den Nutzer oder die Umgebung passiert.
	
	\item[Vertrautheit] besagt, dass durch einen unautorisierten Zugriff auf das System keine Daten offengelegt werden.
	
	\item[Integrität] besagt, dass keine falschen Änderungen am System auftreten.
	
	\item[Wartbarkeit] besagt, dass das System einfach geändert, sowie im Falle eines Ausfalls einfach repariert werden kann.
\end{description}

Die \emph{Anforderungen an die Zuverlässigkeit} enthalten Ansprüche an diese Eigenschaften bezüglich  . Zusammen mit den geplanten \emph{Kosten} stellen diese die \emph{nicht-funktionalen Anforderungen} dar.\cite[S. 5, 20]{avizienis_dependability_2004}

\subsubsection{Sicherstellen von Zuverlässigkeit}
Man unterscheidet zwischen vier hauptsächlichen Methoden, um Zuverlässigkeit sicherzustellen, zwei Methoden, um einen zuverlässigen Dienst zu liefern (\enquote{Fehlerursachenvermeidung} und \enquote{Fehlerursachentoleranz}) und zwei, um das Vertrauen darin zu schaffen, dass die geforderten Anforderungen bezüglich Funktionalität und Zuverlässigkeit auch erfüllt werden (\enquote{Fehlerursachenbeseitigung} und \enquote{Fehlerursachenvorhersage}).\cite[S. 5]{avizienis_dependability_2004} In \autoref{dependability} sieht man eine Übersicht über die eingeführten Begriffe auf Englisch, da ein Großteil der Fachliteratur auf Englisch ist.

\begin{figure}
	\centering
	\includegraphics[max width=0.75\linewidth]{figures/dependability}
	\caption{Zuverlässigkeitsbaum\cite[S. 6]{avizienis_dependability_2004}}
	\label{dependability}
\end{figure}

\subsection{CAP-Theorem}
Das CAP-Theorem setzt einige der gewünschten Eigenschaften speziell für verteilte Datensysteme in Korrelation. Es besagt, dass ein solches System nur zwei der drei Eigenschaften \emph{Verfügbarkeit} (englisch \emph{Availability (A)}, \emph{Konsistenz} (englisch \emph{Consistency (C)} und \emph{Toleranz für Netzwerkpartitionen (P)} perfekt erfüllen kann, wobei Konsistenz die Integrität der Daten sicherstellt. Da man in verteilten Systemen eigentlich immer verschiedene Netzwerkpartitionen hat, heißt das in der Praxis, dass man zwischen C und A abwägen muss. Als Beispiel für dieses Problem kann man relationale und nicht-relationale Datenbanken gegenüberstellen. Während erstere mehr Wert auf Konsistenz legen, fokussieren sich nicht-relationale Datenbanken auf Verfügbarkeit.\cite{brewer_cap_2012}

\subsection{Fehlerursachen}
Die Fehlerursachen kann man nach acht Kriterien in verschiedene Klassen einordnen (siehe \autoref{faults}). Der erste Punkt, \emph{Phase des Entwicklungszyklus'}, bezieht sich darauf, ob die Fehlerursache in der Entwicklungsphase aufgetreten ist (dazu gehört neben der anfänglichen Entwicklung auch die Wartung des Systens) oder während der normalen Benutzung des Systems. Fehlerursachen, die während der Entwicklungsphase entstehen können, sind z.B. fehlerhafte Hardware, oder fehlerhaftes Softwaredesign, während Fehler bei der Benutzung des Systems z.B. Viren, fehlerhafte Eingaben, aber auch Übertragungsfehler und Abnutzung der Hardware sein können.
Der zweite Punkt bewertet, ob die Fehlerursache \emph{intern} aufgetreten ist, also ob z.B. Systemkomponenten (sowohl Software- als auch Hardwarekomponenten) fehlerhaft sind, oder der Fehler \emph{extern} aufgetreten ist, also ob z.B. fehlerhafte Kommunikation als Eingabe kommt oder eine Störung der Kommunikation eingetreten ist.
Das dritte Kriterium bezieht sich darauf, ob der Fehler \emph{von einem Menschen verursacht} worden ist, also z.B. Fehler in der Hardware oder Software übersehen worden sind, oder ein böswilliger Angriff auf das System erfolgt ist, oder ob er \emph{natürlichen Ursprungs} ist, also ob z.B. die Hardware durch Abnutzung Fehler aufweist, oder Kommunikationswege durch einen Sturm o.Ä. unterbrochen sind.
Als viertes Kriterium unterscheidet man zwischen \emph{Hardware-} und \emph{Software-Fehlern}.
Das fünfte Kriterium bezieht sich darauf, ob das Einführen des Fehlers zur Folge einer \emph{böswillige Absicht} geschieht, oder nicht, also ob z.B. durch einen Virus oder ein versuchtes Eindringen ohne entsprechende Zugriffsberechtigung versucht wird, aktiv die Zuverlässigkeit zu verletzen, oder dem entgegen ein Softwarefehler aufgrund von Zeitmangel in der ersten Veröffentlichung enthalten bleibt, um diesen später zu beheben.
Das sechste Kriterium bezieht sich darauf, ob die Handlung eines Menschen, die zum Fehler geführt hat, \emph{bewusst} geschieht, wie bei allen Fehlerursachen mit böswilliger Absicht, oder \emph{unbewusst}, also z.B. durch einen Softwarefehler, den man nicht entdeckt.
Das siebte Kriterium bezieht sich auf die Leistungsfähigkeit des Benutzers, genauer, ob er die Fehlerursache \emph{versehentlich} oder durch \emph{Unvermögen} herbeigeführt hat.
Das letzte Kriterium bezieht sich darauf, ob die Fehlerursache \emph{permanent} oder \emph{temporär} ist. Eine Störung des WLAN-Netzes kann sich zum Beispiel nach einer kurzen Zeit ohne Einflussnahme durch das System auflösen, während fehlerhafte Hardware sich im Normalfall nicht repariert.
Diese Kriterien kann man nun zu 256 Fehlerursachenklassen kombinieren. In \autoref{fault_classes} werden 31 Kombinationen gezeigt, die im Gegensatz zum Rest wahrscheinlich auftreten, sowie Beispiele für die Fehlerursache, sowie ob die Ursache \emph{während der Entwicklung}, \emph{auf Hardwareebene} oder \emph{während der Interaktion}, also im externen System, auftreten.\cite[S. 6ff.]{avizienis_dependability_2004}

\begin{figure}
	\centering
	\includegraphics[max width=0.75\linewidth]{figures/faults}
	\caption{Kriterien zur Einordnung der Fehlerursachen\cite[S. 8]{avizienis_dependability_2004}}
	\label{faults}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[max width=0.75\linewidth]{figures/fault_classes}
	\caption{Fehlerursachenklassen, die wahrscheinlich auftreten\cite[S. 9]{avizienis_dependability_2004}}
	\label{fault_classes}
\end{figure}

\subsection{Ausfall}
\label{subsection:systemausfall}

Ausfälle kann man in drei übergeordnete Klassen einstufen: \enquote{Dienstsystemausfälle}, die immer dann auftreten wenn der gelieferte Dienst des Systems vom korrekten Dienst abweicht, \enquote{Entwicklungsausfälle}. die auftreten, wenn der Entwicklungsprozess vorzeitig abgebrochen wird, z.B. wegen einer Überschreitung des Budgets oder drastisch geänderten Anforderungen, die ein neues System nötig machen, sowie \enquote{Zuverlässigkeitsausfälle}, die auftreten, wenn ein System von den vorher festgelegten Zuverlässigkeitsanforderungen abweicht. Während man als Entwickler Entwicklungssystemausfälle nur bedingt beeinflussen kann, werden Zuverlässigkeitsausfälle durch das Auftreten von Dienstsystemausfällen bedingt. Daher konzentriere ich mich auf Dienstsystemausfälle (im Folgenden nur Systemausfälle).\cite[S. 14ff]{avizienis_dependability_2004}
\cite[S. 324f.]{tanenbaum_distributed_2007} nimmt folgende Einteilung möglicher Systemausfallklasse vor:

\begin{description}
	\item[Zusammenbruch] beschreibt einen Systemausfall, bei dem das System anhält, bis dahin allerdings korrekt arbeitet.
	
	\item[Auslassung] beschreibt einen Systemausfall, bei dem das System nicht mehr auf eingehende Anfragen antwortet. Es gibt dabei zwei Unterklassen: Zum einen \emph{Empfangsauslassung}, bei welcher das System die Anfrage nicht erhält, z.B. wegen eines Übertragungsfehlers, und zum anderen \emph{Versandauslassung}, bei welcher das System die Anfrage zwar erhält, aber keine Antwort sendet.
	
	\item[Timing-Fehler] beschreibt einen Systemausfall, bei dem die Antwort außerhalb des vorher spezifizierten Zeitfensters liegt. Dabei kann die Antwort entweder zu früh erfolgen, was eventuell Probleme im Eingabepuffer des Empfängers verursachen kann, aber auch zu spät, wodurch der Empfänger vermuten könnte, dass das System abgestürzt ist.
	
	\item[Antwort-Fehler] beschreibt einen Systemausfall, bei der die Antwort eines Systems falsch ist. Diese Fehlerklasse kann man auch wieder in zwei Unterklassen aufteilen: zum einen den \emph{Wertfehler}, bei dem das System einen falschen Wert zurückgibt, und zum anderen den \emph{Zustandsübergangsfehler}, bei dem das System vom korrekten Verhalten für eingehende Nachrichten abweicht, weil es diese z.B. nicht lesen kann.
	
	\item[Willkürlicher Fehler], oft auch \enquote{byzantinischer Fehler} beschreibt einen Systemausfall, der sich beliebig  verhalten kann. Es ist egal, ob der Server keine Antwort gibt, absichtlich eine falsche Antwort gibt, oder einfach ausfällt. Allgemein kann man über diese Klasse von Systemausfällen keine näheren Annahmen treffen, wie sich das System verhält.
\end{description}

Tritt ein Systemausfall auf, so kann er, unabhängig von der Systemausfallklasse, in unterschiedlicher \emph{Schwere}  auftreten. Diese reichen von \emph{geringfügige Systemausfällen}, die den Nutzer dabei nur minimal beeinträchtigen, bis zu \emph{katastrophalen Systemausfällen}, welche einen immensen Schaden anrichten und teilweise auch Menschenleben in Gefahr bringen können.
Weiterhin kann man nun verschiedene Systeme benennen, die nur ein bestimmtes Fehlerverhalten aufweisen. Diese Systeme nennt man dann \enquote{Ausfall-kontrollierte Systeme}. In einem \emph{Ausfall-Zusammenbruch-System} treten nur Zusammenbrüche als Systemausfälle zu, ein \emph{ausfallstummes System} gibt keine fehlerhaften Antworten zurück und in einem \emph{ausfallsicheren System} sind die auftretenden Ausfälle nur geringfügig schwer, oder können vom Nutzer als Ausfall erkannt werden.\cite[S. 17]{avizienis_dependability_2004}

\subsection{Zustandsfehler}
Ein Zustandsfehler ist ein fehlerhafter Systemzustand, der zu einem Ausfall führen kann. Dieser Fehler ist \emph{erkannt}, wenn es ein Fehlersignal oder eine Fehlernachricht gibt, die auf den Fehler hinweist. Noch nicht entdeckte Fehler sind \emph{verborgen}. Eine einfache Klassifizierung der Zustandsfehler kann man über den Systemausfall vornehmen, der aus dem Zustandsfehler folgt. Ob ein Zustandsfehler dabei immer einen Systemausfall nach sich zieht, hängt jedoch hauptsächlich von zwei Sachen ab: der \emph{Struktur} des Systems, also sind Maßnahmen getroffen worden, um das Entstehen von Ausfällen zu vermeiden, wie z.B. Redundanz (siehe \ref{subsection:redundanz}), und dem \emph{Verhalten} des Systems. So kann ein Zustandsfehler in einem Teil des Systems passieren, der eventuell für den angeforderten Dienst gar nicht gebraucht wird. Allgemein gilt, dass ein Zustandsfehler erst dann zum Systemausfall wird, sobald er extern sichtbar wird.

\section{Fehlertoleranz}
Fehlertoleranz bezieht sich im Normalfall darauf, Fehlerursachen zu verstecken, sodass diese nicht zu einem Ausfall führen. Dazu wird vor allem Redundanz angewandt, um Ausfälle eines Teilsystems im Gesamtsystem zu verstecken, sodass daraus kein nach außen hin sichtbarer Ausfall entsteht. Dabei wird im Normalfall eine bestimmte Menge an Fehlerursachen vorher bestimmt, welche das System tolerieren soll. Dadurch, dass ein Teil des Systems wegfällt, wird man jedoch mit großer Wahrscheinlichkeit trotzdem eine gesenkte Leistungsfähigkeit von außen beobachten können.\cite[S. 7f.]{jalote_fault_1994}

\subsection{Fehlermaskierung durch Redundanz}
\label{subsection:redundanz}
\cite[S. 326f.]{tanenbaum_distributed_2007} unterscheidet zwischen drei Formen der Redundanz:

\begin{description}
	\item[Informationsredundanz] ist eine Form der Redundanz, bei der Daten und Datenstrukturen durch zusätzliche Informationen erweitert werden, um falsche Informationen zu erkennen und teilweise auch zu reparieren. Ein Beispiel dafür ist der sogenannte Hamming-Code, der mittels mehrerer Paritätsbits sowohl Fehlererkennung, als auch Reparatur des fehlerhaften Codes möglich macht.\cite{hanmer_patterns_2013} Eine Alternative zur Fehlererkennung sind sogenannte \enquote{Cyclic Redundancy Codes} (CRC)\cite{koopman_cyclic_2004}, welche ebenfalls zur Fehlererkennung dienen.
	
	\item[Zeitredundanz] ermöglicht, dass eine Aktion falls nötig mehrmals ausgeführt wird. Zum Beispiel kann ein TCP-Paket erneut versendet werden, sollte dieses beim ersten Senden nicht ankommen.
	
	\item[Physikalische Redundanz] besagt, dass zum einen Hardware-Komponenten mehrfach zur Verfügung stehen, wie z.B. bei der sogenannten \enquote{Triple Modular Redundancy} (TMR). Bei diesem Verfahren, dass u.A. in modernen Prozessoren angewandt wird, liegt jede Hardware-Komponente in dreifacher Ausführung vor. Wird nun ein Ergebnis der entsprechenden Komponente angefordert, so rechnen alle drei Komponenten unabhängig voneinander und danach wird per Mehrheitsentscheid (zwei von drei) das richtige Ergebnis gewählt.\cite{lyons_use_1962} Physikalische Redundanz kann sich auch auf mehrere gleichartige Prozesse beziehen, sodass bei Ausfall einiger der Prozesse das System immer noch richtig funktioniert.
\end{description}

Redundante Prozesse werden in \emph{Gruppen} zusammengefasst. Eine Gruppe kann entweder \emph{hierarchisch} oder \emph{flach} aufgebaut sein. Hierarchische Gruppen bestimmen einen \emph{Koordinatorprozess}, der eingehende Anfragen auf verfügbare \emph{Arbeiterprozesse} aufteilt. Dadurch, dass der Koordinator die Entscheidungsgewalt inne hat, kann er schnell Entscheidungen treffen und Ergebnisse liefern. Fällt er jedoch aus, fällt die gesamte Gruppe aus, bis ein neuer Koordinatorprozess gewählt wird. Dem entgegen sind in einer flachen Gruppe alle Prozesse gleichberechtigt. Jeder Prozess bearbeitet jede Anfrage, woraufhin ein Konsensverfahren folgt, um das abschließende Ergebnis zu bestimmen. Ein Algorithmus zur Wahl des Koordinators ist z.B. der Bully-Algorithmus\cite{garcia-molina_elections_1982}. Diese Gruppenform hat den Vorteil, dass es keinen alleinigen Punkt des Scheiterns gibt. Fällt ein einzelner Prozess oder nur einige wenige aus, so arbeitet die Gruppe immer noch korrekt. Das \emph{Byzantine Generals Problem}\cite{lamport_byzantine_1982} und der \emph{Paxos-Algorithmus}\cite{lamport_paxos_2001} beschäftigen sich mit Konsensfindung. Durch das Konsensverfahren hat man jedoch einen langwierigeren Prozess der Ergebnisfindung als in einer hierarchischen Gruppe.
Diese Gruppen brauchen dann festgelegte Regeln, wie ein Prozess in eine Gruppe ein- und austreten kann. Tritt ein neuer Prozess ein, so muss er in die Kommunikation der Gruppe aufgenommen werden und jede Nachricht innerhalb der Gruppe erhalten, nach dem Austritt aus der Gruppe sollte er keine Nachrichten dieser Gruppe mehr erhalten. Weiterhin sollte es Protokolle geben, mit denen bei einem Ausfall die Gruppe neu strukturiert werden kann (z.B. durch Wahl eines neuen Koordinators), oder die Gruppe aufzulösen, sollte sie nicht mehr funktionsfähig sein. Außerdem sollte es möglich sein, neue Gruppen zu erstellen.\cite[S. 327ff.]{tanenbaum_distributed_2007}

\subsection{Distributed Commit}
Ein Problem in verteilten Systemen ist das Problem des \enquote{distributed commit}. Dabei muss ein hierarchisches System entscheiden, ob es sich einer Aktion verpflichtet, oder die Aktion abbricht. Sollte jedoch während des Entscheidungsprozesses der Koordinator ausfallen, so müssen alle Teilnehmer die gleiche Entscheidung treffen, ob sie sich der Aktion verpflichten oder die Aktion abbrechen, um einen konsistenten Zustand in der Gruppe zu wahren. Bei dem sogenannten \enquote{one-phase commit} (1PC) benachrichtigt der Koordinator die beteiligten Prozesse nur darüber, dass sie die Aktion ausführen sollen. Ist ein Prozess jedoch nicht in der Lage, die Aktion auszuführen, so führt das zu einem inkonsistenten Gruppenzustand, da der entsprechende Prozess dem Koordinator nicht mitteilen kann, dass er die Aktion nicht ausführen kann. Alle anderen Prozesse führen jedoch die Aktion aus.\cite[S. 355]{tanenbaum_distributed_2007}
Daher wurde der sogenannte \enquote{two-phase commit} (2PC) entwickelt. Der Koordinator fragt mit einer \emph{VOTE\_REQUEST}-Nachricht zuerst alle Prozesse, ob sie die Aktion ausführen können. Diese schicken dann eine \emph{VOTE\_COMMIT}-Nachricht, wenn sie dazu in der Lage sind, oder eine \emph{VOTE\_ABORT}-Nachricht, wenn sie nicht dazu bereit sind, die Aktion auszuführen. Erhält der Koordinator wenigstens eine VOTE\_ABORT-Nachricht, so sendet er eine \emph{GLOBAL\_ABORT}-Nachricht an alle Prozesse, welche darauf hin die Aktion abbrechen. Erhält er jedoch von jedem Prozess eine VOTE\_COMMIT-Nachricht, so sendet er eine \emph{GLOBAL\_COMMIT}-Nachricht, woraufhin die Prozesse die Aktion ausführen. In \autoref{2pc} sieht man die Zustandsübergänge der einzelnen Prozesse in (a) Koordinatorrolle und in (b) Teilnehmerrolle. Dieses Protokoll bietet nun schon die Möglichkeit, dass Teilnehmer dem Koordinator mitteilen können, dass sie nicht bereit sind, die Aktion auszuführen. Der Ausfall des Koordinators kann jedoch immer noch dazu führen, dass ein inkonsistenter Systemzustand auftritt. Fällt der Koordinator aus, so versuchen die Prozesse durch Kommunikation untereinander zu ermitteln, wie sie handeln sollen. Fällt der Koordinator aus, während die Teilnehmer noch im \emph{INIT}-Zustand sind, so gehen diese nach einem festgelegten Time-Out in den einzig erreichbaren Endzustand -- \emph{ABORT} Ist nun einer der Teilnehmer im \emph{ABORT}- oder \emph{COMMIT}-Zustand, so kann man sicher darauf schließen, dass der Koordinator entweder ein \emph{GLOBAL\_ABORT}-Nachricht im ersten Fall, oder ein \emph{GLOBAL\_COMMIT}-Nachricht im zweiten Fall gesendet hat. Alle anderen Prozesse müssen daher die gleiche Aktion ausführen wie dieser Prozess. Fällt nun jedoch der Koordinator aus, bevor er eine der Nachrichten sendet, aber jeder Teilnehmer schon eine VOTE\_COMMIT-Nachricht gesendet hat, so befindet sich jeder Zustand im \emph{READY}-Zustand. Allein von dieser Information kann jedoch kein Zustand entscheiden, ob die Aktion ausgeführt oder abgebrochen werden soll, da das System konsistent bleiben soll. Erholt sich der Koordinator nicht, so ist die Gruppe in diesem Zustand gefangen und kann nicht weiter arbeiten.\cite[S. 355ff.]{tanenbaum_distributed_2007}

\begin{figure}
	\centering
	\includegraphics[max width=0.75\linewidth]{figures/2pc}
	\caption{Zustandsmaschine für 2PC\cite[S. 356]{tanenbaum_distributed_2007}}
	\label{2pc}
\end{figure}

Um dieses Problem anzugehen, wurde der sogenannte \enquote{three-phase commit} (3PC) entwickelt. Wie in \autoref{3pc} zu erkennen ist, wird eine neue Nachricht und damit auch ein neuer Zustand hinzugefügt: die \emph{PREPARE\_COMMIT}-Nachricht und der \emph{PRECOMMIT}-Zustand, die als Zwischenschritt zwischen dem \emph{READY}- und dem \emph{COMMIT}-Zustand dienen. Da es nun keinen Zustand gibt, von dem man sowohl in den \emph{ABORT}- als auch in den \emph{COMMIT}-Zustand kommt, wird im \emph{READY}-Zustand nach einem Time-Out dem obigen Prinzip folgend in den \emph{ABORT}-Zustand gewechselt, sollte keine \emph{PREPARE\_COMMIT}-Nachricht vom Koordinator kommen. Hat wenigstens ein Prozess eine solche Nachricht erhalten, so können alle Prozesse in den \emph{PRECOMMIT}-Zustand und danach in den \emph{COMMIT}-Zustand wechseln und es kommt auch beim Ausfall des Koordinators nie zu einem endlosen Wartezustand.\cite[S. 361ff.]{tanenbaum_distributed_2007}

\begin{figure}
	\centering
	\includegraphics[max width=0.75\linewidth]{figures/3pc}
	\caption{Zustandsmaschine für 3PC\cite[S. 361]{tanenbaum_distributed_2007}}
	\label{3pc}
\end{figure}

\section{Behandlung von Fehlern}
Um langfristig korrektes Systemverhalten sicherstellen zu können, müssen zuerst die Fehlerursachen erkannt, der angerichtete Schaden begrenzt und danach die Fehlerursachen behoben werden. Diesen Prozess kann man allerdings nur ausgehend von erkannten Zustandsfehlern beginnen, da man Systemausfälle und Fehlerursachen nicht direkt beobachtet werden können.\cite[S. 8f.]{jalote_fault_1994}

\subsection{Erkennen und Eingrenzen des Fehlers}
Da man die Fehlerursache nicht direkt erkennen kann, muss man zuerst den entsprechenden Zustandsfehler erkennen. Von diesem Zustandsfehler aus kann man dann Rückschlüsse auf mögliche Systemausfälle und Fehlerursachen schließen. Ein Mechanismus zum Erkennen des Zustandsfehlers sollte im Idealfall nur von den funktionalen Anforderungen abhängen und nicht von den Systemdetails. Das System wird daher als \enquote{Black Box} betrachtet. Weiterhin sollte ein idealer Mechanismus komplett und korrekt sein, also jeder Zustandsfehler wird gefunden, und, wenn der Mechanismus keinen Zustandsfehler entdeckt, dann liegt auch keiner vor. Der Mechanismus sollte außerdem nicht vom korrekten Funktionieren des Systems abhängen. Ein Test, der falsch ist, sobald ein Zustandsfehler im System auftritt, bringt keinen Mehrwert für das System. Alle diese gewünschten Eigenschaften zu erfüllen ist im Normalfall nicht machbar, da entweder die Mechanismen so komplex sein müssten, dass sie selbst wieder fehleranfällig sind, oder zu viele Ressourcen benötigt würden. Daher benutzt man, wie bei den Anforderungen an die Zuverlässigkeit, auch wieder akzeptable Mechanismen, die die Eigenschaften zu einem gewissen Maß erfüllen. Im folgenden sind einige generelle Mechanismen-Arten aufgeführt, die häufig benutzt werden:\cite[S. 9ff.]{jalote_fault_1994}

\begin{description}
	\item[Replikationsüberprüfungen] beschreiben Verfahren, bei dem Teile der internen Struktur repliziert werden. Sollten die replizierten Teile unterschiedliche Ergebnisse liefern, so kann man davon ausgehen, dass einer der gleichen Systemteile fehlerhaft ist. TMR ist ein Beispiel für die praktische Anwendung.
	
	\item[Timing-Überprüfungen] beschreiben Verfahren, bei dem Systemkomponenten bezüglich der vorher festgelegten zeitlichen Begrenzungen geprüft werden. Antwortet die Systemkomponente nicht im erwarteten Zeitintervall, so ist sie fehlerhaft.
	
	\item[Strukturelle und Code-Überprüfungen] beschreiben Verfahren, die die Daten und Datenstrukturen überprüfen. Als Beispiel hierfür können CRC-Überprüfungen genannt werden.
	
	\item[Diagnostische Überprüfungen] beschreiben Verfahren, die ein System ausführt, um die korrekte Arbeitsweise zu überprüfen. Ein Beispiel hierfür sind klassische Modultests.
	
	\item[Überprüfungen der Vernünftigkeit] beschreiben Verfahren, die prüfen, ob ein Ergebnis vernünftig ist. Würde für eine Uhrzeit z.B. 25:67 als Ergebnis geliefert werden, so ist das Ergebnis wahrscheinlich falsch.
\end{description}

Hat man einen Zustandsfehler erkannt, so sollte man überprüfen, wie weit sich der Fehler im System ausgebreitet hat. Das kann dadurch erreicht werden, indem vom Auftreten des Fehlers ausgehende Kommunikation überprüft wird, bis man entweder auf die Außengrenzen trifft, oder keine Kommunikation mit anderen Systemteilen stattgefunden hat. Im besten Fall kann man so die Ausbreitung innerhalb des Systems auf einen Teil des Systems beschränken.\cite[S. 14f.]{jalote_fault_1994}

\subsection{Korrigieren von Zustandsfehlern}
Hat man einen Zustandsfehler erkannt, so muss der Zustand des Systems korrigiert werden, sodass es seine Dienste wieder korrekt anbietet. Im Allgemeinen unterscheidet man dabei zwischen zwei Methoden: \enquote{rückwärtsgerichtete Korrektur} und \enquote{vorwärtsgerichtete Korrektur}. Bei der rückwärtsgerichteten Korrektur speichert das System regelmäßig sogenannte \enquote{Kontrollpunkte} in einem fehlerfreien Zustand. Wird nun ein fehlerhafter Zustand erkannt, so wird das System auf den letzten fehlerfreien Kontrollpunkt zurückgesetzt. Um danach einen korrekten Zustand gewährleisten zu können, braucht man einen \emph{stabilen Speicher}, der auch bei einem Zustandsfehler die alten Daten korrekt speichert und nicht verfälscht. Zudem kann man mit \enquote{Nachrichtenprotokollierung} die Nachrichten speichern, die während des fehlerhaften Systemzustands eingetroffen sind, und diese nach Wiederherstellen des korrekten Systemzustands erneut in das System einspielen -- solange diese nicht die Fehlerursache waren. Bei der vorwärtsgerichteten Korrektur wird versucht, das System durch entsprechende Handlungen wieder in einen fehlerfreien Zustand zu bringen. Dazu braucht man eine genaue Klassifizierung des Zustandsfehlers, um diesen korrekt behandeln zu können. Eine Beispiel dafür ist Ausnahmebehandlung.

\section{Zusammenfassung}
Zuverlässigkeit und Fehlertoleranz sind wichtige Eigenschaften, um die korrekte Funktionsweise eines Systems näher zu beschreiben. Dabei gibt es eine Vielzahl unterschiedlicher Fehlerklassen, die auftreten können, weshalb es meist nicht vernünftig ist, komplette Fehlertoleranz oder komplette Zuverlässigkeit zu fordern, sondern eine akzeptable Abweichung davon festzulegen. Zudem gibt es verschiedene Methoden, um Fehler zu erkennen und zu behandeln, die jeweils ihre eigenen Vor- und Nachteile haben. Daher muss bei jedem System neu abgewägt werden, welche Fehlerklassen behandelt werden sollen und welche Mittel dazu benutzt werden. 