\author{Nele Noack}
\email{nele.noack@student.hpi.uni-potsdam.de}
\group{BP2017P1 -- \enquote{Wolke sucht Herzschlag}}
\organisation{FG Betriebssysteme \& Middleware}
\title{Feedforward-Netze}
\event{Bachelorprojekt-Vorbereitungsseminar}

\maketitle

\begin{abstract}
Von der Definition künstlicher Neuronen über die Zusammensetzung zu einschichtigen Perzeptronen, die in ihrer Problemlösung auf linear separierbare Aufgabenstellungen beschränkt sind, über die umfassenderen mehrschichtigen Perzeptronen zur performanteren Klassifizierung von Bildern und zeitabhängigen Daten durch Convolutional Neural Networks wird in die Domain der Feedforward-Netze eingeführt.
Online und Batch Training mittels Fehlerrückführung werden gegenübergestellt, wovon ersteres zeitintensiver, aber meist genauer ist. Bei anschließender Evaluierung reicht die Richtigkeit des Netzwerkes allein nicht aus, die Präzision, die Sensitivität und weiteren Metriken sollten betrachtet werden.
\end{abstract}

\section{Einleitung}
Klassischerweise löst ein Computer ein Problem, indem ihm die einzelnen Ausführungsschritte in Form eines Programms gegeben werden. Doch es gibt auch Probleme, die sich nicht als Algorithmus ausdrücken lassen, welche für das menschliche Gehirn aber kein Problem darstellen. Entscheidet ist dabei die Fähigkeit des Gehirns zu lernen: Für das Erkennen, auch Klassifizieren, von Objekten oder Strukturen lernen wir Muster, die wir wiedererkennen können. Künstliche neuronale Netze dienen dazu, Computer ein solches Verhalten simulieren, sie lernen zu lassen. Das Neuronennetz des Gehirns dient dabei als Inspiration. Die folgende Arbeit befasst sich mit verschiedenen Feedforward-Netzen, in denen alle Verbindungen vorwärtsgerichtet sind. Zunächst wird der Grundbaustein künstlicher neuronaler Netze vorgestellt, das künstliche Neuron. Danach werden verschiedene darauf aufbauende Netze betrachtet: die vollständig vernetzen einlagigen und mehrlagigen Perzeptronen sowie das komplexere Convolutional Neural Network, welches auf Bilderkennung und der Klassifizierung zeitunabhängiger Daten spezialisiert ist. Des Weiteren wird in die Terminologie des Trainings eingeführt, wobei es sich um überwachtes Lernen handelt (Dabei erhält das Netz zu den gegebenen Daten die erwarteten Ausgabewerte). Zuletzt werden verschiedene Metriken zur Evaluierung der Netze vorgestellt, wobei vor allem die Frage interessant ist, wann das Netz den Problembereich ausreichend klassifiziert.


\section{Der Aufbau verschiedener Feedforward-Netze}
\subsection{Künstliches Neuron}
Die Basiseinheit künstlicher neuronaler Netzwerke lehnt sich an seine biologische Version an: Die Stärke der synaptischen Verbindungen wirkt sich auf die eingehenden Erregungssignale aus. Über den Zellkörper wird die Erregung summiert und das Neuron feuert, leitet also einen elektrischen Impuls weiter, wenn das Schwellenpotential erreicht wurde. 
\cite{hanser:2005:lexikon}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/ArtificialNeuronModel_deutsch.png}
	\caption{Aufbau eines künstlichen Neurons \newline
	Aus den Eingaben wird eine gewichtete Summe gebildet. Daraus berechnet die Aktivierungsfunktion, verschoben durch den Schwellenwert, die Ausgabe des Neurons. \cite{artificialneuron2005Chrislb}}
	\label{fig:Neuron}
\end{figure}

Daraus ergeben sich die in Abbildung 1 dargestellten Bestandteile eines künstlichen Neurons. Als erster Verarbeitungsschritt wird die gewichtete Summe der Eingaben gebildet, was als \emph{Eingabefunktion} bezeichnet wird. Die \emph{Aktivierungs}- oder auch \emph{Transferfunktion} berechnet aus der gewichteten Summe, verschoben durch den \emph{Schwellenwert}, auch Bias genannt, die Ausgabe des künstlichen Neurons. \cite{artificial-neuron}
\textbf{Im Folgenden sei mit \textit{Neuron} das künstliche Neuron gemeint.}

\emph{Im Gegensatz zum Neuron, dessen Aktivierungsfunktion variabel ist, steht das einfache Perzeptron:} Eingeführt wurde es von Rosenblatt, der dessen Parameter auf messbaren physikalischen Größen aufbaute \cite{Rosenblatt58theperceptron:}. 
Das Feuern eines biologischen Neurons ist allerdings eine boolesche Funktion, weshalb das einfache Perzeptron als Aktivierungsfunktion die Heaviside-Funktion nutzt. Dabei wird 1 ausgegeben, wenn die gewichtete Summe größer 0 (bzw. größer als der Schwellenwert) ist, sonst 0.

Aufgrund der nahezu übereinstimmenden Definition werden die Begriffe \emph{künstliches Neuron} und \emph{Perzeptron} heutzutage meist synonym verwendet. Daraus ergeben sich auch die Begriffe des \emph{einlagigen} und des \emph{mehrlagigen Perzeptrons}, deren Bestandteile Neuronen sind und nicht, wie der Name vermuten lässt, einfache Perzeptronen.

\subsection{Einlagiges Perzeptron}

Künstliche neuronale Netze bestehen aus einer Vielzahl an Neuronen, welche in verschiedenen Schichten angeordnet sind. Zusätzlich gibt es eine Schicht von Eingabeknoten, welche die jeweilige Eingabe durchreichen. Die einfachste Form einen neuronalen Netzes ist das einlagige Perzeptron, dargestellt in \autoref{fig:singleLayerPerceptron}]. Perzeptronen sind \emph{vollständig vernetzte Feedforward-Netze}, das heißt jeder Knoten einer Schicht ist mit jedem der Nachfolgeschicht verbunden. Feedforward-Netze kennzeichnet, dass keine Rückkopplung stattfindet, es bestehen also keine Verbindungen zwischen Knoten einer Schicht mit Knoten der Vorgängerschichten. Da die Knoten der Eingabeschicht keine trainierbaren Gewichte besitzen, werden sie beim Zählen der Schichten nicht betrachtet. \cite[76, 87]{Kriesel2007NeuralNetworks}
Einlagige Perzeptronen können allerdings nur linear separierbare Probleme lösen, wie 1969 von \textcite{minsky2017perceptrons} gezeigt wurde. Binäre Funktionen sind separierbar, wenn man ihren Argumentraum mit einer einzigen Geraden (bzw. Ebene) in wahre und falsche Ergebnisse teilen kann. Das XOR-Problem (Entweder-Oder-Problem) ist beispielsweise nicht linear separierbar. Um es trotzdem lösen bzw. erlernen zu können, müssen sogenannte versteckte Schichten zum Perzeptron hinzugefügt werden. 
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/singleLayerPerceptron.png}
	\caption{Aufbau eines einlagigen Perzeptrons \newline
	Die Eingabeschicht wird gebildet von den Eingabeknoten, welche jede Eingaben an jedes Neuronen der Ausgabeschicht weiterleiten. Diese verarbeiten dann die Eingaben mithilfe der Eingangs- und Aktivierungsfunktionen.}
	\label{fig:singleLayerPerceptron}
\end{figure}


\subsection{Mehrlagiges Perzeptron}

Mehrlagige Perzeptronen sind nicht auf die Lösung linear separierbarer Probleme limitiert. Sie dienen weiterhin der Approximation unbekannter Abbildungsvorschriften, zur Regression und Klassifizierung. \autoref{fig:doubleLayerPerceptron} stellt ein zweilagiges Perzeptron dar. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/multiLayerPerceptron.png}
	\caption{Ein zweilagiges Perzeptron mit einer versteckten Schicht \newline
	Die drei Eingabeknoten leiten ihre Eingabe an jedes der vier Neuronen der versteckten Schicht weiter. Deren Ausgaben werden an die beiden Neuronen der Ausgabeschicht weitergegeben.}
	\label{fig:doubleLayerPerceptron}
\end{figure}


Es gibt noch kein Theorem, das die optimale Anzahl der versteckten Schichten oder die Anzahl ihrer Neuronen festlegt, der es für ein bestimmtes Problem bedarf. Beim Experimentieren mit verschiedenen Parametern können folgende Hinweise, sinngemäß nach \textcite[S.~21~f.]{stark2001wechselkursprognose}, helfen:
\begin{enumerate}
	\item
	Um Overfitting (\enquote{Überanpassung}, \enquote{auswendig lernen}) zu vermeiden und die Generalisierung zu fördern, werden maximal so viele versteckte Neuronen pro Schicht erstellt, wie es Eingaben gibt. Verallgemeinert das Netz weiterhin nicht, der Fehler im Trainingsdatensatz ist also klein, während der Fehler im Testdatensatz groß ist, gibt es zu viele Neuronen in den versteckten Schichten.
	\item 
	Sind nicht genügend Informationen vorhanden, führen zu wenige versteckte Neuron, die bei Vorhandensein der Informationen ausreichen würden, zu Annahmen über den fehlenden Informationen, die möglicherweise nicht zutreffen. Große Fehlerwerte in den Trainingsepochen sprechen also eventuell für zu wenige versteckte Neuronen.
	\item
	Befinden sich alle Gewichte in der gleichen Größenordnung, deutet das ebenfalls auf zu wenige versteckte Neuronen hin.
	

\end{enumerate}

Mehrlagige Perzeptronen dienen der Klassifizierung, also beispielsweise auch der Bilderkennung. Bei einem Bild sind die Eingabevariablen gleichartig, \emph{\#FFFFFF} steht in jeder Eingabevariable für ein weißes Pixel. Mehrlagige Perzeptronen jedoch gehen davon aus, die Werte verschiedener Attribute eines Samples vorliegen zu haben. Dadurch erschweren sich mehrlagige Perzeptronen die Erkennung von Objekten, der Fokus liegt zu sehr auf dem Ort statt auf der bloßen Existenz eines Merkmals.

Als Begründer des an die Bilderkennung des Menschen angelehnten \emph{Convolutional Neural Networks} gilt der Franzose Yann LeCun.



\subsection{Convolutional Neural Network}

Für die Bilderkennung sowie für die Klassifizierung zeitabhängiger Daten ist das Convolutional Neural Network, auf deutsch \enquote{faltendes neuronales Netz}, am geläufigsten. Sein Aufbau ist ebenfalls von der Biologie inspiriert: 1959 zeigten David Hubel und Torsten Wiesel, dass bestimmte Neuronen im visuellen Kortex nur bei gesehenen Linien, Balken oder Kanten feuern und dass die Stärke von der jeweiligen Orientierung des Reizes abhängt. Zellen mit derselben bevorzugten Orientierung sind horizontal gruppiert, während Zellen mit ähnlicher Präferenz sich daneben gruppieren. \cite[30]{hueter2005lehrbuch}

Wie schon erwähnt berücksichtigen mehrschichtige Perzeptronen die räumliche Struktur von Bildern nicht. Außerdem werden hilfreiche Aspekte des Aufbaues eines Bildes nicht genutzt: Eng beieinander liegende Pixel, die beispielsweise beide Teil einer Linie sein könnten, haben im Perzeptronennetz die gleiche Beziehung wie weit auseinander liegende Pixel. \cite[Kapitel 6]{deeplearning2017nielsen}

\subsubsection{Aufbau}

Um den Aufbau eines Convolutional Neural Networks besser visualisieren zu können, hilft es, sich die Eingabeneuronen nicht in einer Reihe vorzustellen, sondern angeordnet in einer 3D-Matrix. Zeilen- und Spaltenanzahl entspricht dabei der Pixelanzahl in Höhe und Breite, während es drei Seiten für die Farbe als RGB-Wert gibt. In jedem Eingabeknoten steht ein Wert zwischen 0 und 255. In \autoref{fig:Inputmatrix} sind die drei Seiten auf eine abstrahiert.

\begin{figure}[!ht]
\centering 
\includegraphics[width=30mm, height=30mm]{figures/achtMalAchtInput.png}
\caption{\newline Eine 8x8x3 Eingabematrix, auf eine Seite abstrahiert}
\label{fig:Inputmatrix}
\end{figure}


Auf die Eingabeschicht folgt eine \emph{faltende Schicht}: Hier werden nicht alle Neuronen mit allen Knoten der vorherigen Schicht verbunden, sondern jedes nur mit Knoten einer bestimmten Region. Eine solche Region des Eingangsbildes wird als \emph{lokales rezeptives Feld} eines versteckten Neurons bezeichnet. Jedes Neuron der ersten versteckten Schicht ist also nur für eine begrenzte Menge an Pixeln zuständig, welche alle beieinander liegen. Dies ist in \autoref{fig:ConvLayer1} und \autoref{fig:ConvLayer2} exemplarisch für ein rezeptives Feld der Größe 5x5 dargestellt, wobei das lokale rezeptive Feld immer um ein Pixel verschoben wird. Dadurch ergeben sich in diesem Beispiel bei 28x28 Eingabeknoten 25x25 Knoten in der ersten versteckten Schicht. Die Anzahl der Pixel, um die verschoben wird, wird als \emph{Schrittlänge} bezeichnet und kann variieren. Für die Behandlung der Randregionen existieren verschiedene Herangehensweisen, in diesem Beispiel werden sie der Einfachheit halber vernachlässigt. Auch die Größe des rezeptiven Feldes kann variiert werden, also bei größeren Bildern ein entsprechend größeres Feld. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/convLayer1.png}
	\caption{Die Eingabeschicht (links) und die erste versteckte Schicht (rechts) \newline
	Ein Neuron der ersten versteckten Schicht ist hier für die Verarbeitung der Werte eines 5x5-Bereiches des Eingangsbildes, sein lokale rezeptive Feld, zuständig. \cite{deeplearning2017nielsen}}
	\label{fig:ConvLayer1}
\end{figure}
\newpage
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/convLayer2.png}
	\caption{Das lokale rezeptive Feld wird immer um ein Pixel verschoben, vergleiche \autoref{fig:ConvLayer1} \cite{deeplearning2017nielsen}}
	\label{fig:ConvLayer2}
\end{figure}

Neuronen bilden die gewichtete Summe der Eingabe und wenden darauf, eventuell durch einen Schwellenwert verschoben, die Aktivierungsfunktion an. Besonders an den Neuronen in der Faltenden Schicht ist allerdings, dass sich alle den gleichen Schwellenwert und die gleichen Gewichte teilen. Im bekannten Beispiel gäbe es also eine 5x5-Matrix an Gewichten, die jedes Neuron auf sein lokales rezeptives Feld anwendet. Diese Matrix an Gewichten wird auch als \emph{Faltungsmatrix} oder Filterkern bezeichnet. 
Diese gemeinsame Nutzung der Gewichte und des Schwellenwerts führen dazu, dass alle Neuronen der versteckten Schicht auf dasselbe Merkmal bzw. Feature reagieren, jeweils in ihrem Bereich des Bildes. Darum wird das Ergebnis auch als \emph{Feature Map} bezeichnet.
Nun müssen für die Bildererkennung aber deutlich mehr Features identifiziert werden können. Eine komplette faltende Schicht besteht also nicht nur aus einer Feature Map, sondern aus mehreren verschiedenen, entstanden durch unterschiedliche Faltungsmatrizen. Beispielsweise kann das in \autoref{fig:CompleteConvLayer} dargestellte Netz 	drei verschiedene Features erkennen, und zwar im gesamten Bild verteilt und mehrfach auftretend.
Ein entscheidender Vorteil dieses Aufbaus gegenüber vollständig vernetzten Netzen ist die stark reduzierte Parameteranzahl, was zu schnellerem Training führt \cite[Kapitel 6]{deeplearning2017nielsen}.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/completeConvLayer.png}
	\caption{Die Eingabeschicht wird auf mehrere Feature Maps abgebildet, \newline welche zusammen die erste faltende Schicht bilden. Jede Feature Map hat ihre eigene Faltungsmatrix, wodurch sie unterschiedliche Features lernen. \cite{deeplearning2017nielsen}}
	\label{fig:CompleteConvLayer}
\end{figure}

Zur Vereinfachung kann man sagen, dass mehrere faltende Schichten hintereinander immer größere Objekte erkennen, indem zuvor erkannte, kleinere sozusagen zusammengesetzt werden. Nun ist die exakte Position eines Features für das Zusammensetzen uninteressant, die ungefähre Position in Relation zu anderen genügt. Um also die Informationsmenge zu verringern, wird hinter jede faltende Schicht eine \emph{Pooling-Schicht} gelegt. Diese fasst einen Ausschnitt von Werten einer Feature Map zu einem einzigen Wert zusammen, wie \autoref{fig:pooling} zeigt. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/pooling.png}
	\caption{Ein 2x2-Bereich einer Feature Map wird von einem Neuron einer Pooling Schicht auf einen Wert abstrahiert \cite{deeplearning2017nielsen}}
	\label{fig:pooling}
\end{figure}

Üblicherweise wird als Aktivierungsfunktion die Max-Pooling-Funktion verwendet, welche einfach aus den Eingaben den höchsten Wert auswählt. Besteht die faltende Schicht aus mehreren Feature Maps, ergibt das genauso viele Schichten in der Pooling-Ebene. Nach einer Reihe von faltenden Schichten und Pooling-Schichten folgt als letztes eine vollständig vernetzte Schicht. Jedes Neuron der Ausgabeschicht entspricht einer Klasse entspricht. In Abbildung \autoref{fig:CompletePooling} ist der komplette Aufbau eines Convolutional Neural Networks vereinfacht dargestellt.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/complete.png}
	\caption{Ein 2x2 Bereich einer Feature Map wird von einem Neuron einer Pooling Schicht auf einen Wert abstrahiert \cite{deeplearning2017nielsen}}
	\label{fig:CompletePooling}
\end{figure}

\section{Training von Feedforward-Netzen}

Das Ziel des Trainings ist es, die Gewichte und Schwellenwerte derart zu setzen, dass der gesamte Fehler des Netzes minimal ist. Dieser Fehler wird mit einer sogenannten Verlust- oder Kostenfunktion angegeben, beispielsweise der mittleren quadratischen Abweichung. Abstrahiert wird das Netz also durch eine Funktion mit äußerst vielen Variablen dargestellt, welche es zu minimieren gilt.

Die Minimierung des Fehlers wird mithilfe eines Gradientenverfahrens erreicht, bei dem man sich dem Minimum der Funktion über eine Folge kleiner werdender Funktionswerte nähert. Üblicherweise wird das stochastische Gradientenverfahren, kurz SGD, verwendet, bei dem der Gradient approximiert wird. \cite[Kapitel 1]{deeplearning2017nielsen}

Oft ist es aufgrund der Ressourcen nicht möglich, auf dem gesamten Trainingsdatensatz zu trainieren, also erst nach Sichtung aller Trainingsdaten Gewichte und Schwellenwert anzupassen.
Intuitiv erscheint es am leichtesten, nach jedem Eintrag des Trainingsdatensatzes die Gewichte zu aktualisieren. Dieser Verfahren wird als \emph{Online Learning} bezeichnet. Doch das Anpassen der Gewichte ist zeitintensiv und es ist nicht selten gar nicht notwendig, nach jedem einzelnen Eintrag die Gewichte anzupassen. Schneller ist da das \emph{Batch Learning}, bei dem der Trainingsdatensatz in Stapel, sogenannte \emph{Batches}, aufgeteilt und erst nach Sichtung eines gesamten Batches die Anpassung vorgenommen wird. Sind alle Einträge gesichtet worden, ist eine \emph{Trainingsepoche} abgeschlossen, welche so viele \emph{Iterationen} durchläuft, wie es Batches gibt.
Die Gewichtsanpassung findet mithilfe des \emph{Backpropagation of Error}, zu deutsch Fehlerrückführung, statt. Dabei wird, nachdem die Trainingsdaten das Netz durchlaufen haben und die Ergebnisse mit den gewünschten Werten verglichen wurden, der ermittelte Fehler von der Ausgabeschicht zur Eingabeschicht zurückpropagiert. Abhängig von ihrem Einfluss auf den Fehler werden dabei die Gewichte angepasst, was einiges an Rechenaufwand kostet. Aus diesem Grund ist ein Training mit großer Batch-Größe schneller. Die optimale Batch-Größe und auch die Anzahl der benötigten Epochen hängt von dem verwendeten Datensatz ab.

Verschiedene Metriken, die die Güte des neuronalen Netzes nach erfolgtem Training repräsentieren, werden im folgenden Kapitel vorgestellt.

\section{Evaluierung eines neuronalen Netzes}

An erster Stelle steht die \emph{Richtigkeit} oder Accuracy des Netzwerkes: Wie viele unbekannte Daten werden nach dem Training richtig klassifiziert? Erst wenn die Klassifizierungsrichtigkeit ausreichend erscheint, wird das Training beendet. Diese wird üblicherweise in Prozent angegeben, also durch $\frac{\text{Anzahl korrekter Klassifizierungen}}{\text{Gesamtzahl an Klassifizierungen}}$ berechnet. Jedoch reicht dieser Wert allein nicht aus, um zu entscheiden, ob das Modell gut genug ist, das gegebene Problem zu lösen. 

Die \emph{Wahrheitsmatrix}, auch Confusion Matrix, dient dazu, einen ersten Überblick zu bekommen: Die Anzahl der falsch positiven und negativen Ergebnisse werden den korrekten positiven und negativen Vorhersagen gegenübergestellt.

Der Richtigkeit gegenüber steht die \emph{Präzision}, auch positiver Vorhersagewert genannt. Sie wird durch $\frac{\text{Anzahl richtig positiver Vorhersagen}}{\text{Gesamtanzahl positiver Vorhersagen}}$ gebildet. Die Präzision ist also ein Maß für die Genauigkeit des Klassifizierers. Eine geringe Präzision kann bei hoher Richtigkeit vorliegen, wenn die Anzahl der falsch positiven Vorhersagen hoch ist. 
Anzumerken ist, dass sich die Definition und Verwendung von Richtigkeit und Präzision in der Informationsgewinnung von denen anderer Wissenschaftszweige und der Statistik unterscheidet.

Des Weiteren gibt eine richtig positiv Rate, \emph{Sensitivität} oder Recall genannt. Für diesen Wert wird $\frac{\text{Gesamtzahl positiver Klassifizierungen}}{\text{Anzahl richtig positiver Vorhersagen}}$ berechnet. Die Sensitivität kann auch als Vollständigkeit des Modell angesehen werden. Wenn der Wert niedrig ist, liegen viele falsch negative Vorhersagen vor.

Die Metrik, die Präzision und Sensitivität mittels harmonischem Mittel kombiniert, wird \emph{F1-Maß} genannt: $2 * \frac{\text{Präzision * Sensitivität}}{\text{Präzision + Sensitivität}}$. Ein niedriger Wert stellt eine gute Balance zwischen Präzision und Sensitivität dar. \cite{evaluation2014brownlee}

\section{Zusammenfassung und Ausblick}

Künstliche neuronale Netze bieten die Möglichkeit, dem Computer keinen Algorithmus vorgebenzumüssen, sondern ihn ihn erlernen zu lassen. Mehrschichtige Perzeptronen stellen dabei mit vergleichsweise einfachem Aufbau eine gute erste Wahl zur Klassifizierung eines Problemraums dar, während sich Convolutional Neural Networks bestens zur Bilderkennung eignen. Es wurden einige Tipps gegeben, wie man eine gute Anzahl an versteckten Schichten und versteckten Neuronen in mehrschichtigen Perzeptronen ermitteln kann. Um das Overfitting noch radikaler zu verhindern, können \emph{Dropout Schichten} eingefügt werden, die zufällig ausgewählte Neuronen abschalten. Im Trend ist außerdem die künstliche Erzeugung von Daten, um einen Mangel an Beispieldaten zu beheben. Bei großen Datensätzen eignet sich das Batch Training gegenüber dem Online Training besser aufgrund der Geschwindigkeit. Bei anschließender Evaluierung reicht die Richtigkeit des Netzwerkes allein nicht aus, die Präzision, die Sensitivität und weiteren Metriken sollten betrachtet werden, wobei es noch weitaus mehr Metriken gibt, auf die in dieser Arbeit nicht eingegangen wurde.