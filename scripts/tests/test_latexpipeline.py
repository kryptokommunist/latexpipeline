# -*- coding: utf-8 -*-
"""
Testing module for the corresponding latexpipeline.py module.

Usage:

pytest

or

pytest --cov-report html --cov scripts

for coverage reports. Pytest should be the python3 version.
"""

from unittest.mock import MagicMock
import os
import subprocess
import pytest

from pipeline import latexpipeline

TEST_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
TEST_ROOT_DIR_PATH = os.path.join(TEST_DIR_PATH, "test_root_folder")
SCRIPT_TEST_ROOT_DIR_PATH = os.path.join(TEST_DIR_PATH, "test_script_root_folder")
#path relative to SCRIPT_TEST_ROOT_DIR_PATH/TEST_ROOT_DIR_PATH
RELATIVE_SCRIPT_PATH = "../../pipeline_script.py"

TEST_PROJECT_URL = "test"
TEST_USER = "Max Mustermann"
TEST_USER_MAIL = "marcus.ding@student.hpi.de"
TEST_JOB_ID = "4223"
TEST_COMMIT_MSG = ("Testrun of pipeline. --meta --final '{trd}/sample' "\
    "--comment Is this a test? Yes!").format(trd=TEST_ROOT_DIR_PATH)

pipeline_path = TEST_ROOT_DIR_PATH

def get_pipeline(commit_msg=TEST_COMMIT_MSG):
    """Return a new pipeline instance"""
    p = latexpipeline.LateXPipeline(TEST_USER, TEST_JOB_ID, commit_msg, TEST_USER_MAIL, TEST_PROJECT_URL, TEST_USER_MAIL)
    p.set_relative_path(pipeline_path)
    return p

class TestFunctions(object):
    """Test functions in isolation"""
    def test_sanitize_string(self):
        """Check the function"""
        assert latexpipeline.sanitize_string("test\033[91mtest\033[94mtest\033[0m💋") == "testtesttest💋"

    def test_get_files_folders(self, tmpdir):
        """Check the function"""

        assert latexpipeline.get_files_folders(tmpdir)["folders"] == []
        assert latexpipeline.get_files_folders(tmpdir)["files"] == []

        tmpdir.mkdir("test")
        tmpdir.join("test.txt").write("")
        assert latexpipeline.get_files_folders(tmpdir)["folders"] == ["test"]
        assert latexpipeline.get_files_folders(tmpdir)["files"] == ["test.txt"]

    def test_send_mail(self, mocker):
        """Check the function"""
        mock_smtp = mocker.patch('pipeline.latexpipeline.smtplib.SMTP')
        mock_smtp.return_value = MagicMock()
        from email.mime.text import MIMEText

        class MailConfig(object):
            pass

        config = MailConfig()
        config.sender_mail = "test@test.com"
        config.sender_name = "test"
        config.host = "test.com"
        config.recipient_mail = "test@recpient.com"
        config.subject = "subject"
        config.msg = "msg"
        config.user_mail = "test@user.com"

        msg = MIMEText(config.msg)
        msg["Subject"] = config.subject
        msg["From"] = '"{sn}" <{sa}>'.format(sn=config.sender_name, sa=config.sender_mail)
        msg["To"] = config.recipient_mail
        msg["Reply-To"] = config.user_mail

        latexpipeline.send_mail(config)
        mock_smtp.assert_called_once_with(config.host)
        mock_smtp.return_value.sendmail.assert_called_with(config.sender_mail, [config.recipient_mail, config.user_mail], msg.as_string())
        mock_smtp.return_value.quit.assert_called_once_with()
        mock_smtp.reset()

    def test_send_success_email(self, mocker):
        """Check the function"""
        mock_smtp = mocker.patch('pipeline.latexpipeline.smtplib.SMTP')
        mock_smtp.return_value = MagicMock()
        from email.mime.text import MIMEText

        class MailConfig(object):
            pass

        usr = "Rolf Test"
        artifact_name = "test artifact name"
        artifact_link = "test.com/test.pdf"
        config = MailConfig()
        config.sender_mail = latexpipeline.MAIL_SENDER
        config.sender_name = latexpipeline.MAIL_SENDER_NAME
        config.host = latexpipeline.MAIL_HOST
        config.recipient_mail = TEST_USER_MAIL
        config.subject = "Finale Abgabe von {u}: {a}".format(u=usr, a=artifact_name)
        config.msg = "\n".join((
            "Hallo,",
            "",
            "hier die finale Ausarbeitung von {u}:".format(u=usr),
            "",
            "  {a}".format(a=artifact_link),
            "",
            " Diese Nachricht wurde automatisch erstellt.",
        ))
        config.user_mail = TEST_USER_MAIL

        msg = MIMEText(config.msg)
        msg["Subject"] = config.subject
        msg["From"] = '"{sn}" <{sa}>'.format(sn=config.sender_name, sa=config.sender_mail)
        msg["To"] = config.recipient_mail
        msg["Reply-To"] = config.user_mail

        p = get_pipeline()
        p.send_success_email(usr, artifact_name, artifact_link)
        mock_smtp.assert_called_once_with(config.host)
        mock_smtp.return_value.sendmail.assert_called_with(config.sender_mail, [config.recipient_mail, config.user_mail], msg.as_string())
        mock_smtp.return_value.quit.assert_called_once_with()
        mock_smtp.reset()

    def test_send_telegram_msg(self, mocker):
        """Check the function"""
        mock_post = mocker.patch('pipeline.latexpipeline.requests.post')
        p = get_pipeline()
        p.notifications_enabled = True
        msg = "test"
        payload = {'value1': msg}
        url = 'https://maker.ifttt.com/trigger/{e}/with/key/{k}/'\
            .format(e=latexpipeline.IFTTT_TELEGRAM_EVENT, k=latexpipeline.IFTTT_API_KEY)
        p.send_telegram_msg(msg)
        mock_post.assert_called_once_with(url, data=payload)

        mock_post.reset_mock()
        p.notifications_enabled = False
        p.send_telegram_msg(msg)
        assert not mock_post.called

    def test_error_notify_and_exit(self, mocker):
        """Check the function"""
        mock_post = mocker.patch('pipeline.latexpipeline.requests.post')
        p = get_pipeline()
        with pytest.raises(SystemExit) as excinfo:
            p.error_notify_and_exit("test")
        assert "test" in str(excinfo.value)

class TestFunctionality(object):
    """Test pipeline with context using entire LateXPipeline object"""
    def run_fresh_pipeline_get_excinfo(self, commit_msg=TEST_COMMIT_MSG, expect_sysexit=True):
        """create a new pipeline instance and run it"""
        p = get_pipeline(commit_msg)
        if expect_sysexit:
            with pytest.raises(SystemExit) as excinfo:
                p.run()
            return excinfo
        p.run()
        return p

    def assert_missing_file_compliance(self, filename, mandatory_files, mocks, directory=False):
        """check that the pipeline will detect missing mandatory files"""
        if directory:
            directory.join(filename).write("")
        excinfo = self.run_fresh_pipeline_get_excinfo()
        mocks.mock_call.assert_not_called()
        mocks.mock_send_success_email.assert_not_called()
        mocks.mock_post.assert_not_called()
        mocks.mocker.resetall()
        assert "misses" in excinfo.value.code
        assert not filename in excinfo.value.code
        for mandatory_file in mandatory_files:
            assert mandatory_file in excinfo.value.code

    def test_compliance_enforcement(self, mocker, tmpdir):
        """check that pipeline will recognize missing not allowed files and throw errors if that's detected"""
        mock_send_success_email = mocker.patch('pipeline.latexpipeline.LateXPipeline.send_success_email')
        mock_call = mocker.patch('pipeline.latexpipeline.call')
        mock_post = mocker.patch('pipeline.latexpipeline.requests.post')
        mock_call.return_value = 0

        global pipeline_path
        pipeline_path = TEST_ROOT_DIR_PATH

        self.run_fresh_pipeline_get_excinfo(expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_success_email.assert_called_once()
        mock_post.assert_not_called()
        mocker.resetall()
        pipeline_path = tmpdir

        p = self.run_fresh_pipeline_get_excinfo(expect_sysexit=False)
        assert p.submitted_final == "flaggedfinal"

        tmpdir.mkdir("sample")
        excinfo = self.run_fresh_pipeline_get_excinfo()
        mock_call.assert_not_called()
        mock_send_success_email.assert_not_called()
        mock_post.assert_not_called()
        mocker.resetall()
        assert "misses" in excinfo.value.code
        assert "sample.tex" in excinfo.value.code
        for mandatory_file in latexpipeline.MANDATORY_FILES:
            assert mandatory_file in excinfo.value.code

        mandatory_files = list(latexpipeline.MANDATORY_FILES)
        sample_tmpdir = tmpdir.join("sample")
        class Mocks(object):
            pass
        mocks = Mocks()
        mocks.mocker = mocker
        mocks.mock_call = mock_call
        mocks.mock_send_success_email = mock_send_success_email
        mocks.mock_post = mock_post

        self.assert_missing_file_compliance("sample.tex", mandatory_files, mocks, sample_tmpdir)

        mandatory_files.remove("preamble.tex")
        self.assert_missing_file_compliance("preamble.tex", mandatory_files, mocks, sample_tmpdir)

        mandatory_files.remove("references.bib")
        self.assert_missing_file_compliance("references.bib", mandatory_files, mocks, sample_tmpdir)

        sample_tmpdir.join("content.tex").write("")
        sample_tmpdir.mkdir("figures").join("test.png").write("")
        excinfo = self.run_fresh_pipeline_get_excinfo(expect_sysexit=False)

        sample_tmpdir.join("forbidden.tex").write("")
        excinfo = self.run_fresh_pipeline_get_excinfo()
        assert not "sample.tex" in excinfo.value.code
        assert "must not contain" in excinfo.value.code
        assert "forbidden.tex" in excinfo.value.code

        sample_tmpdir.join("forbidden.tex").remove()
        sample_tmpdir.mkdir("forbidden_folder")
        excinfo = self.run_fresh_pipeline_get_excinfo()
        assert not "sample.tex" in excinfo.value.code
        assert "figures" in excinfo.value.code
        assert "forbidden_folder" in excinfo.value.code

        sample_tmpdir.join("forbidden_folder").remove()
        tmpdir.mkdir("forbidden_root_folder")
        excinfo = self.run_fresh_pipeline_get_excinfo()
        assert not "forbidden_folder" in excinfo.value.code
        assert "forbidden_root_folder" in excinfo.value.code
        assert "misses" in excinfo.value.code
        assert "forbidden_root_folder.tex" in excinfo.value.code

    def test_latex_call_error(self, mocker):
        """Check that an error will be sent when the LateX compilation fails"""
        mock_send_success_email = mocker.patch('pipeline.latexpipeline.LateXPipeline.send_success_email')
        mock_call = mocker.patch('pipeline.latexpipeline.call')
        mocker.patch('pipeline.latexpipeline.requests.post')
        mock_error = mocker.patch('pipeline.latexpipeline.LateXPipeline.error_notify_and_exit')
        mock_call.return_value = 1
        global pipeline_path
        pipeline_path = TEST_ROOT_DIR_PATH

        self.run_fresh_pipeline_get_excinfo("Testrun of pipeline without flags", expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_success_email.assert_not_called()
        mock_error.assert_called_once()

    def test_console_help(self):
        """Check that the console displays help to the command line user"""

        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH, shell=True, cwd=TEST_ROOT_DIR_PATH,\
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Help" in ret.stderr
        assert ret.stdout == b""

        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + " wtf ", shell=True, cwd=TEST_ROOT_DIR_PATH,\
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Help" in ret.stderr
        assert ret.stdout == b""

        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + " run", shell=True, cwd=TEST_ROOT_DIR_PATH,\
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Help" in ret.stderr
        assert ret.stdout == b""

        ret = subprocess.run("python3 " + "test_root_folder/" + RELATIVE_SCRIPT_PATH + " test", shell=True, cwd=TEST_DIR_PATH,\
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"test_folder_one\x1b[91m doesn't comply" in ret.stderr or b"test_folder_two\x1b[91m doesn't comply" in ret.stderr
        assert b"not contain: ['hpitr.cls']" in ret.stderr

    def test_console_test_usage(self):
        """Check that Latexmk actually works and the module can be invoked as script from command line"""
        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + ' test test@test.de "Test --meta lol"',\
            shell=True, cwd=SCRIPT_TEST_ROOT_DIR_PATH, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        print(ret.stderr)
        assert b"YAY" in ret.stdout
        assert b"Latexmk" in ret.stderr

        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + ' test test@test.de "Test --final lol --meta --comment lol"',\
            shell=True, cwd=SCRIPT_TEST_ROOT_DIR_PATH, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Flag" in ret.stderr
        assert b"--final $FOLDERPATH" in ret.stderr

        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + ' test test@test.de "Test --final lol --meta --comment lol" additional arg',\
            shell=True, cwd=SCRIPT_TEST_ROOT_DIR_PATH, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Allowed arguments" in ret.stderr
        assert b"additional" in ret.stderr
        assert b"arg" in ret.stderr

        samplepdf_path = os.path.join(SCRIPT_TEST_ROOT_DIR_PATH, "test_folder_one", "latex_output", "sample.pdf")
        samplepdf_path_after_one = os.path.join(SCRIPT_TEST_ROOT_DIR_PATH, "test_folder_one", "sample.pdf")
        samplepdf_path_after_two = os.path.join(SCRIPT_TEST_ROOT_DIR_PATH, "test_folder_two", "sample.pdf")
        pdfs = [samplepdf_path, samplepdf_path_after_one, samplepdf_path_after_two]
        for pdf in pdfs:
            if os.path.isfile(pdf):
                os.remove(pdf)
        ret = subprocess.run("python3 " + RELATIVE_SCRIPT_PATH + " run 1 2 '--meta' 4 5", shell=True, cwd=SCRIPT_TEST_ROOT_DIR_PATH,\
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        assert b"Pipeline succeeded" in ret.stdout
        for pdf in pdfs:
            assert os.path.isfile(pdf)
            os.remove(pdf)

    def test_pipeline_non_submit_run(self, mocker):
        """Check that a run that isn't a submission also works"""
        mock_send_success_email = mocker.patch('pipeline.latexpipeline.LateXPipeline.send_success_email')
        mock_call = mocker.patch('pipeline.latexpipeline.call')
        mock_post = mocker.patch('pipeline.latexpipeline.requests.post')
        mock_call.return_value = 0
        global pipeline_path
        pipeline_path = TEST_ROOT_DIR_PATH

        self.run_fresh_pipeline_get_excinfo("Testrun of pipeline without flags", expect_sysexit=False)
        mock_call.assert_called_once()
        mock_post.assert_called_once()
        mock_send_success_email.assert_not_called()

    def test_submit_flag(self, mocker):
        """Check behaviour of submit flag"""
        mock_send_mail = mocker.patch('pipeline.latexpipeline.send_mail')
        mock_call = mocker.patch('pipeline.latexpipeline.call')
        mocker.patch('pipeline.latexpipeline.requests.post')
        mock_call.return_value = 0
        global pipeline_path
        pipeline_path = TEST_ROOT_DIR_PATH

        excinfo = self.run_fresh_pipeline_get_excinfo("Testrun wrong submit --final {trd}/lol".format(trd=TEST_ROOT_DIR_PATH))
        assert "flag" in excinfo.value.code
        assert "doesn't match" in excinfo.value.code
        assert "lol" in excinfo.value.code
        mock_call.assert_called_once()
        mock_send_mail.assert_not_called()
        mocker.resetall()

        excinfo = self.run_fresh_pipeline_get_excinfo("Testrun wrong submit --final ")
        assert "Flag" in excinfo.value.code
        assert "--final $FOLDEPATH" in excinfo.value.code
        assert "Supplied" in excinfo.value.code
        mock_call.assert_not_called()
        mock_send_mail.assert_not_called()
        mocker.resetall()

        self.run_fresh_pipeline_get_excinfo("Testrun --final {trd}/sample --comment lol".format(trd=TEST_ROOT_DIR_PATH), expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_mail.assert_called_once()
        mocker.resetall()

        self.run_fresh_pipeline_get_excinfo("Testrun --final {trd}/sample --comment".format(trd=TEST_ROOT_DIR_PATH), expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_mail.assert_called_once()
        mocker.resetall()

    def test_meta_flag(self, mocker):
        """Check behaviour of meta flag"""
        mock_send_mail = mocker.patch('pipeline.latexpipeline.send_mail')
        mock_call = mocker.patch('pipeline.latexpipeline.call')
        mock_post = mocker.patch('pipeline.latexpipeline.requests.post')
        mock_call.return_value = 0
        global pipeline_path
        pipeline_path = TEST_ROOT_DIR_PATH

        self.run_fresh_pipeline_get_excinfo("Testrun no notification --meta --final {trd}/sample".format(trd=TEST_ROOT_DIR_PATH), expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_mail.assert_called_once()
        mock_post.assert_not_called()
        mocker.resetall()

        self.run_fresh_pipeline_get_excinfo("Testrun no notification --final {trd}/sample".format(trd=TEST_ROOT_DIR_PATH), expect_sysexit=False)
        mock_call.assert_called_once()
        mock_send_mail.assert_called_once()
        mock_post.assert_called_once()
        mocker.resetall()
