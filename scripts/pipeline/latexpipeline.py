# -*- coding: utf-8 -*-
r"""
This module compiles LaTeX files in all subfolders of LateXPipeline.relative_path relative to the
current working directory from where the script is invoked using latexmk in a
GitLab CI environment. All files are written to 'LateXPipeline.relative_path/latex_output'.
Further it checks the compliancy of all files/folders in these subfolders. It can notify
about errors and successes via Email and Telegram.

To use minted package import with "...usepackage[outputdir=../latex_output]{minted}"
Setting the outputdir prevents issues when writing everything to latex_output.

Execute this pipeline with:

 from pipeline import latexpipeline
 latexpipeline.LateXPipeline(USER, JOB_ID, COMMIT_MSG, USER_MAIL, PROJECT_URL, MAIL_RECIPIENT).run()

The script will sys.exit(ERROR_MESSAGE) if there are any errors otherwise it won't. Depending on
settings of LateXPipeline.notifications_enabled the pipeline will also send messages to IFTT.
"""

from os import walk
from os.path import join, normpath
from subprocess import call
from shutil import copyfile
import shlex
import smtplib
from email.mime.text import MIMEText
import requests

#ifttt.com credentials
IFTTT_API_KEY = ""
IFTTT_TELEGRAM_EVENT = "ci_built_succeeded"

MAIL_SENDER_NAME = "GitLab CI"
MAIL_SENDER = "noreply@gitlab.hpi.de"
MAIL_HOST = "mailout.hpi.uni-potsdam.de"

#relative path of directories with LaTeX files
RELATIVE_PATH = '.'
#colors used in messages to be sanitized for non terminal use
TERMINAL_COLORS = ["\033[91m", "\033[94m", "\033[0m"]
#files mandatory in each LaTeX folder
MANDATORY_FILES = ["preamble.tex", "references.bib", "content.tex"]
#directories in LateXPipeline.relative_path the pipeline will ignore
EXEMPT_DIRS = ["scripts", "latex_output", "BP_COMBINED", "__pycache__"]
#directories starting with given string in LateXPipeline.relative_path the pipeline will ignore
EXEMPT_DIRS_START_WITH = ["."]
#folders allowed but not mandatory in subdirectories
ALLOWED_SUBDIRS = ["figures"]
#allowed folder structure
FOLDER_STRUCTURE = {"exempt_dirs": EXEMPT_DIRS, "exempt_dirs_start_with": EXEMPT_DIRS_START_WITH,\
    "mandatory_files": MANDATORY_FILES, "allowed_subdirs": ALLOWED_SUBDIRS}

def sanitize_string(msg):
    """remove terminal color control characters from string"""
    for col in TERMINAL_COLORS:
        msg = msg.replace(col, "")
    return msg

def get_files_folders(path):
    """get files and directories in repository path as dict"""
    for (dirpath, dirnames, filenames) in walk(path):
        return  {"files": filenames, "folders": dirnames}


def send_mail(config):
    """Send an email with given arguments enclosed in object via a non secured smtp server"""
    msg = MIMEText(config.msg)
    msg["Subject"] = config.subject
    msg["From"] = '"{sn}" <{sa}>'.format(sn=config.sender_name, sa=config.sender_mail)
    msg["To"] = config.recipient_mail
    msg["Reply-To"] = config.user_mail
    smtp = smtplib.SMTP(config.host)
    smtp.sendmail(config.sender_mail, [config.recipient_mail, config.user_mail],\
        msg.as_string())
    smtp.quit()

class LateXPipeline(object):
    """Class contains functions for compiling LaTeX documents, checking for correct folder
       structure and sending notifications via IFTT.com. Just use LateXPipeline().run()
    """
    def __init__(self, user_str, job_id_str, commit_msg_str, user_mail_str, project_url_str,\
        recipient_mail_str):
        #enable email/telegram
        self.notifications_enabled = True
        self.set_relative_path(RELATIVE_PATH)
        self.folder_structure = FOLDER_STRUCTURE
        #if self.commit_msg supplied with --final flag, variable contains directory to be emailed
        self.final_submit_dir = False
        self.submit_comment = False
        self.job_id = job_id_str
        self.user = user_str
        self.submitted_final = False
        self.user_mail = user_mail_str
        self.commit_msg = commit_msg_str
        self.project_url = project_url_str
        self.recipient_mail = recipient_mail_str
        #headers for telegram messages
        self.error_header = ('<b>Gitlab Job FAILED</b><br><br>'\
            '<a href="{url}/-/jobs/{id}">'\
            'See job</a><br> Started by {u}<br><br>Commit:<br>{cm}<br><br>')\
            .format(url=self.project_url, id=self.job_id, u=self.user, cm=self.commit_msg)
        self.success_header = ('<b>Gitlab Job SUCCESS</b><br><br>'\
            '<a href="{url}/-/jobs/{id}">'\
            'See job</a><br> Started by {u}<br><br>Commit:<br>{cm}<br><br>')\
            .format(url=self.project_url, id=self.job_id, u=self.user, cm=self.commit_msg)
        self.final_header = ('<b>Gitlab Final SUBMIT</b><br><br>'\
            '<a href="{url}/-/jobs/{id}">'\
            'See job</a><br> Started by {u}<br><br>Commit:<br>{cm}<br><br>')\
            .format(url=self.project_url, id=self.job_id, u=self.user, cm=self.commit_msg)

    def set_relative_path(self, rel_path):
        self.relative_path = rel_path
        self.first_level_dirs = get_files_folders(self.relative_path)["folders"]
        self.final_submit_dir = False

    def run(self):
        """execute entire Latex pipeline"""
        self.process_flags()
        self.compile_pdfs()
        if self.final_submit_dir:
            self.send_final_submit(self.final_submit_dir)
            return
        self.send_telegram_msg(self.success_header)
        print("\033[92m Latex Pipeline succeeded")

    def send_telegram_msg(self, msg):
        """send a message via Telegram"""
        if self.notifications_enabled:
            url = 'https://maker.ifttt.com/trigger/{e}/with/key/{k}/'\
                .format(e=IFTTT_TELEGRAM_EVENT, k=IFTTT_API_KEY)
            payload = {'value1': sanitize_string(msg)}
            requests.post(url, data=payload)

    def send_success_email(self, usr, artifact_name, artifact_link):
        """send a message containing artifact link"""
        comment = ""
        if self.submit_comment:
            comment = "Abgabekommentar: {c}\n\n".format(c=self.submit_comment)
        msg = "\n".join((
            "Hallo,",
            "",
            "hier die finale Ausarbeitung von {u}:".format(u=usr),
            "",
            "  {a}".format(a=artifact_link),
            "",
            "{c} Diese Nachricht wurde automatisch erstellt.".format(c=comment),
        ))
        subject = "Finale Abgabe von {u}: {a}".format(u=usr, a=artifact_name)
        msg = sanitize_string(msg)

        class MailConfig(object):
            pass

        config = MailConfig()
        config.sender_mail = MAIL_SENDER
        config.sender_name = MAIL_SENDER_NAME
        config.host = MAIL_HOST
        config.recipient_mail = self.recipient_mail
        config.subject = subject
        config.msg = msg
        config.user_mail = self.user_mail

        send_mail(config)

    def error_notify_and_exit(self, msg):
        """send error message and exit script"""
        self.send_telegram_msg(self.error_header + msg)
        exit(msg)

    def enforce_compliance(self, dir_name, files_folders):
        """enforce constraints on folder structure and exit with error for non compliance"""
        files = files_folders["files"]
        dirs = files_folders["folders"]
        self.enforce_file_compliance(dir_name, files)
        self.enforce_subfolder_compliance(dir_name, dirs)
        print("\033[92mDirectory \033[94m{d}\033[92m is compliant.\033[0m"\
            .format(d=dir_name))

    def enforce_file_compliance(self, dir_name, files_in_folder):
        """enforce compliance of files in folder"""
        main_filename = dir_name + '.tex'
        compliant_files = [main_filename]
        compliant_files.extend(self.folder_structure["mandatory_files"])
        for filename in list(compliant_files):
            if filename in files_in_folder:
                files_in_folder.remove(filename)
                compliant_files.remove(filename)
        errmsg = ""
        if len(compliant_files) > 0:
            errmsg += ("\033[91mThe folder \033[94m{d}\033[91m doesn't comply"\
                " with formal requirements. It misses: {cf}\n")\
                .format(d=dir_name, cf=str(compliant_files))
        if len(files_in_folder) > 0:
            errmsg += ("\033[91mThe folder \033[94m{d}\033[91m doesn't comply "\
                "with formal requirements. It must not contain: {fs}")\
                .format(d=dir_name, fs=str(files_in_folder))
        if errmsg:
            self.error_notify_and_exit(errmsg)

    def enforce_subfolder_compliance(self, dir_name, subfolders):
        """enforce compliance of subfolders in folder"""
        for foldername in list(subfolders):
            if foldername in self.folder_structure["allowed_subdirs"]:
                subfolders.remove(foldername)
        if len(subfolders) > 0:
            errmsg = ("\033[91mThe folder \033[94m{d}\033[91m contains: {ds}."\
                " But it should only contain {af} folder!\033[0m")\
                .format(d=dir_name, ds=str(subfolders), af=self.folder_structure["allowed_subdirs"])
            self.error_notify_and_exit(errmsg)

    def remove_exempt_folders(self, folders):
        """remove folders defined as exempt in self.folder_structure from given list"""
        for exempt_folder in self.folder_structure["exempt_dirs"]:
            if exempt_folder in folders:
                folders.remove(exempt_folder)

        for folder in list(folders):
            for exempt_dir_start in self.folder_structure["exempt_dirs_start_with"]:
                if folder.startswith(exempt_dir_start):
                    folders.remove(folder)

        return folders

    def compile_pdfs(self):
        """compile every TeX file in each repository subfolder"""
        second_level_items = {}
        self.first_level_dirs = self.remove_exempt_folders(self.first_level_dirs)

        for first_level_dir in self.first_level_dirs:
            dir_path = join(self.relative_path, first_level_dir)
            files_folders = get_files_folders(dir_path)
            second_level_items[first_level_dir] = files_folders

        for dir_name, data in second_level_items.items():
            self.enforce_compliance(dir_name, data)

        for dir_name, data in second_level_items.items():
            main_filename = dir_name + ".tex"
            folderpath = join(self.relative_path, dir_name)
            cmd = ('cd "{fp}" && latexmk -output-directory=../latex_output -pdf "{f}"'\
                " -e '$pdflatex=q/pdflatex %O -shell-escape %S/'")\
                .format(fp=folderpath, f=main_filename)
            if call(cmd, shell=True) == 0:
                sccmsg = "\033[92mCompiled \033[94m{d}.tex\033[92m successfully\033[0m"\
                    .format(d=dir_name)
                print(sccmsg)
            else:
                errmsg = "\033[91mError with compiling \033[94m{d}.tex\033[0m\ncmd: {c}"\
                    .format(d=dir_name, c=cmd)
                self.error_notify_and_exit(errmsg)

    def process_flags(self):
        """check if commit contains "--final" or "--meta" flag"""
        commit_msg_split = shlex.split(self.commit_msg)
        if "--meta" in commit_msg_split:
            self.notifications_enabled = False
            print("\033[92mDisabled notifications")
        if "--final" in commit_msg_split:
            pos = commit_msg_split.index("--final")
            if len(commit_msg_split) >= pos+2:
                submit_path = commit_msg_split[pos+1]
                submit_path = submit_path.split("/")
                rel_path = "/".join(submit_path[:-1])
                if not self.submitted_final == "submitted":
                    self.submitted_final = "flaggedfinal"
                if self.relative_path == normpath(rel_path):
                    self.final_submit_dir = submit_path[-1]
            else:
                errmsg = "\033[91mFlag should be used this way: --final $FOLDEPATH. Supplied: {cm}"\
                    .format(cm=str(commit_msg_split))
                self.error_notify_and_exit(errmsg)
        if "--comment" in commit_msg_split:
            pos = commit_msg_split.index("--comment")
            if len(commit_msg_split) >= pos+2:
                self.submit_comment = " ".join(commit_msg_split[pos+1:])

    def send_final_submit(self, dir_name):
        """send final submission for given directory"""
        filename = dir_name + ".pdf"
        if not dir_name in self.first_level_dirs:
            errmsg = "\033[91mFinal flag path \033[94m{d}\033[91m doesn't match any directory! {ds}"\
                .format(d=dir_name, ds=self.first_level_dirs)
            self.error_notify_and_exit(errmsg)
        link = "/-/jobs/{id}/artifacts/raw/latex_output/{f}"\
            .format(id=self.job_id, f=filename)
        link = self.project_url + requests.utils.quote(link)
        self.send_success_email(self.user, filename, link)
        self.send_telegram_msg(self.final_header + link)
        self.submitted_final = "submitted"
        print("\033[92mSuccefully submitted \033[94m{d}".format(d=dir_name))

    def move_pdfs(self, new_folder_path=False):
        """move the pdfs out of the latex_output folder to given path"""
        if not new_folder_path:
            new_folder_path = self.relative_path
        for folder in self.first_level_dirs:
            pdf_name = folder + ".pdf"
            old_pdf_path = join(self.relative_path, "latex_output", pdf_name)
            new_pdf_path = join(new_folder_path, pdf_name)
            copyfile(old_pdf_path, new_pdf_path)
