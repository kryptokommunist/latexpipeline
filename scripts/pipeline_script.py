r"""
This script compiles LaTeX files in all subfolders of RELATIVE_PATH relative to the
current working directory from where the script is invoked using latexmk in a
GitLab CI environment. All files are written to RELATIVE_PATH/latex_output .
Further it checks the compliancy of all files/folders in these subfolders. It can notify
about errors and successes via Email and Telegram.

To use minted package import with "...usepackage[outputdir=../latex_output]{minted}"
Setting the outputdir prevents issues when writing everything to latex_output.

For CI environment run:
python3 pipeline_script.py run $USER $JOB_ID $COMMIT_MSG $USER_MAIL $PROJECT_URL

For test environment run:
python3 pipeline_script.py test [$USER_MAIL $COMMIT_MSG].
(arguments in brackets are optional)

Test mode executes the pipeline using the test parameters predefined below.
"""

import sys
from pipeline import latexpipeline

TEST_PROJECT_URL = "https://gitlab.com/kryptokommunist/latexpipeline"
TEST_USER = "Max Mustermann"
TEST_USER_MAIL = "marcus.ding@student.hpi.de"
TEST_JOB_ID = "4223"
TEST_COMMIT_MSG = "Testrun of pipeline. --meta --final 'letters/McFit Kündigung' "\
    "--comment Is this a test? Yes!"

MAIL_RECIPIENT = "BP2017P-Betreuer@hpi.de"

#ifttt.com credentials
latexpipeline.IFTTT_API_KEY = ""
latexpipeline.IFTTT_TELEGRAM_EVENT = "ci_built_succeeded"

latexpipeline.MAIL_SENDER_NAME = "GitLab CI"
latexpipeline.MAIL_SENDER = "noreply@gitlab.hpi.de"
latexpipeline.MAIL_HOST = "mailout.hpi.uni-potsdam.de"

#root level directories we won't process
EXEMPT_DIRS = ["scripts", "__pycache__"]
#dirs starting with given string in LateXPipeline.relative_path will be ignored
#also applied to root level dirs
EXEMPT_DIRS_START_WITH = ["."]
#files mandatory in each LaTeX folder
MANDATORY_FILES_ASSIGNMENT = ["preamble.tex", "references.bib", "content.tex"]
#directories in LateXPipeline.relative_path the pipeline will ignore
EXEMPT_DIRS_ASSIGNMENT = ["latex_output", "BP_COMBINED", "__pycache__"]
#folders allowed but not mandatory in subdirectories
ALLOWED_SUBDIRS = ["figures"]
#allowed folder structure for folder assignment_papers
FOLDER_STRUCTURE_ASSIGNMENT = {"exempt_dirs": EXEMPT_DIRS_ASSIGNMENT,\
    "exempt_dirs_start_with": EXEMPT_DIRS_START_WITH,\
    "mandatory_files": MANDATORY_FILES_ASSIGNMENT,\
    "allowed_subdirs": ALLOWED_SUBDIRS}
#allowed folder structure for folder letters
FOLDER_STRUCTURE_LETTERS = dict(FOLDER_STRUCTURE_ASSIGNMENT)
FOLDER_STRUCTURE_LETTERS["mandatory_files"] = []
#different folder structures for each root level dir
#test_root_folder(_one/two) is necessary for pytest testing
FOLDER_STRUCTURES = {"test_root_folder": FOLDER_STRUCTURE_ASSIGNMENT,\
    "test_script_root_folder": FOLDER_STRUCTURE_ASSIGNMENT,\
    "test_folder_one": FOLDER_STRUCTURE_ASSIGNMENT, "test_folder_two": FOLDER_STRUCTURE_ASSIGNMENT,\
    "assignment_papers": FOLDER_STRUCTURE_ASSIGNMENT, "letters": FOLDER_STRUCTURE_LETTERS}

#displayed on command line when arguments incorrect
HELP_MESSAGE = "\n".join((
    "Help:",
    "This script outputs all files to './latex_output'",
    "\033[94mFor CI environment run:\033[0m",
    "python3 {fn} run $USER $JOB_ID $COMMIT_MSG $USER_MAIL $PROJECT_URL"\
        .format(fn=sys.argv[0]),
    "",
    "\033[94mFor test environment run:\033[0m",
    "python3 {fn} test [$USER_MAIL $COMMIT_MSG]".format(fn=sys.argv[0]),
    "(arguments in brackets are optional)",
    "",
    '\033[94mYou supplied: \033[0m"{a}"'.format(a=" ".join(sys.argv[1:]))
))

is_production = True

if __name__ == "__main__":
    USER = TEST_USER
    JOB_ID = TEST_JOB_ID
    COMMIT_MSG = TEST_COMMIT_MSG
    USER_MAIL = TEST_USER_MAIL
    PROJECT_URL = TEST_PROJECT_URL
    if len(sys.argv) >= 2 and sys.argv[1] == "test":
        is_production = False
        MAIL_RECIPIENT = TEST_USER_MAIL
        if len(sys.argv) >= 3:
            USER_MAIL = sys.argv[2]
        if len(sys.argv) == 4:
            COMMIT_MSG = sys.argv[3]
        if len(sys.argv) > 4:
            exit(('\033[91mAllowed arguments for test: $USER_MAIL $COMMIT_MS ' \
                'You supplied: "{a}"').format(a=" ".join(sys.argv)))
    elif len(sys.argv) == 7 and sys.argv[1] == "run":
        USER = sys.argv[2]
        JOB_ID = sys.argv[3]
        COMMIT_MSG = sys.argv[4]
        USER_MAIL = sys.argv[5]
        PROJECT_URL = sys.argv[6]
    else:
        exit(HELP_MESSAGE)
    PIPELINE = latexpipeline.LateXPipeline(USER, JOB_ID, COMMIT_MSG, USER_MAIL,\
        PROJECT_URL, MAIL_RECIPIENT)
    FILES_FOLDERS = latexpipeline.get_files_folders(".")
    PIPELINE.folder_structure["exempt_dirs"] = EXEMPT_DIRS
    PIPELINE.folder_structure["exempt_dirs_start_with"] = EXEMPT_DIRS_START_WITH
    ROOT_FOLDERS = PIPELINE.remove_exempt_folders(FILES_FOLDERS["folders"])
    for folder in ROOT_FOLDERS:
        PIPELINE.set_relative_path(folder)
        PIPELINE.folder_structure = FOLDER_STRUCTURES[folder]
        PIPELINE.run()
        if is_production:
            PIPELINE.move_pdfs()
    if PIPELINE.submitted_final == "flaggedfinal":
        PIPELINE.error_notify_and_exit("\033[91mFlag should be used this way: --final $FOLDERPATH")
    print("\033[92mYAY :)")
