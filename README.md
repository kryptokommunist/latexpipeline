# A Gitlab CI based LaTeX pipeline [![build status](https://gitlab.com/kryptokommunist/latexpipeline/badges/master/build.svg)](https://gitlab.com/kryptokommunist/latexpipeline/pipelines)

 - Job: compile_pdf_combined
   - [Direct Download Link PDF1](https://gitlab.com/kryptokommunist/latexpipeline/-/jobs/artifacts/master/raw/assignment_papers/latex_output/BP2017P1.pdf?job=compile_pdf_combined "Wolke sucht Herzschlag")
   - [Direct Download Link PDF2](https://gitlab.com/kryptokommunist/latexpipeline/-/jobs/artifacts/master/raw/assignment_papers/latex_output/BP2017P2.pdf?job=compile_pdf_combined "IoT & Blockchain - Rail2X SmartServices")
 - Job: compile_pdf
   - [Browse all PDFs](https://gitlab.com/kryptokommunist/latexpipeline/-/jobs/artifacts/master/browse/?job=compile_pdf "All PDFs")


## Scripts [![coverage report](https://gitlab.com/kryptokommunist/latexpipeline/badges/master/coverage.svg)](https://gitlab.com/kryptokommunist/commits/master)

 - Job: tests
   - [Download coverage report](https://gitlab.com/kryptokommunist/latexpipeline/-/jobs/artifacts/master/browse/?job=tests "Coverage Report")

The files are located in [scripts](/scripts).

## Scripts (DE)

Zu finden unter [scripts](/scripts). Das Script ist in Python3 geschrieben. Das Skript prüft die Ordnerstrukturen auf Korrektheit und versendet zudem eine E-Mail an die Betreuer mit einem Link auf die in der Commit-Message angegebene Ausarbeitung (Nachträgliche Änderungen ändern nichts an der verlinkten Datei!). Mit dem Flag `--final $ORDNER_NAME` wird die Funktion getriggert. 
Mit dem `--comment Hier könnt ihr einen Kommentar schreiben` wird der E-Mail der Kommentar beigefügt. Der gesamte Inhalt der Commit-Message nach dem `--comment`-Flag wir übernommen. Es dürfen also keine Flags folgen. Z.B. würde folgende Commit-Message die CI-Pipeline veranlassen die Abgabe des Ordners `mmustermensch_mein_titel` inklusive Kommentar an die Betreuer schicken:

        Abgabe meiner Ausarbeitung --final assignment_papers/mmustermensch_mein_titel --comment Hier könnt ihr einen Kommentar schreiben
        
Die Funktion Notifications über [IFTTT](https://ifttt.com) kann, wenn sie eingerichtet wurde, über das Flag `--meta` unterdrückt werden.

        Abgabe meiner Ausarbeitung --final assignment_papers/mmustermensch_mein_titel --meta --comment Hier könnt ihr einen Kommentar schreiben
        
Obige Commit-Message würde die CI-Pipeline veranlassen die Abgabe des Ordners `assignment_papers/mmustermensch_mein` inklusive Kommentar an die Betreuer schicken und keine Benachrichtigung über IFTTT.com zu versenden.

### Lokal kompilieren und ausführen

Alle Outputfiles werden in einen einzelnen Ordner `/$SUBFOLDER/latex_output` geschrieben, jeder kann die Kompilation vor einem Commit lokal sorgenfrei testen indem er/sie

 `python3 scripts/pipeline_script.py test marcus.ding@student.hpi.de`
 
 oder mit einer Test-Commit-Message
 
 `python3 scripts/pipeline_script.py test marcus.ding@student.hpi.de "Test. --final assignment_papers/mmustermensch_mein_titel"` 

ausführt. Die Email ist entsprechend anzupassen.

### Lokales Testing mit Pytest

Zu finden unter [scripts/tests](/scripts/tests). Für Testing je nach Detailgrad

`pytest` oder `pytest --cov scripts` oder `pytest --cov-report html --cov scripts` ausführen.

# Wie richte ich die GitLab-CI für ein neues Repo ein?

 - forke dieses Repo
 - ersetze alle Pfadpräfixe in dieser `README.md`, um die Links auf das neue Repo anzupassen
 - setze einen [Gitlab-Runner](https://docs.gitlab.com/runner/) auf
 - füge ihn dem Repo hinzu
 - aktiviere das Erfassen der codecoverage in der CI
 - passe das [.gitlab-ci.yml](/.gitlab-ci.yml) an
 - passe das [script](/scripts/pipeline-script.py) an
 - relax 🙇